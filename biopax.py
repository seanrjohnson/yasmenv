import rdflib
#import reaction
from rdflib import URIRef
#import json
import pprint


def read_rdf(filename):
    g = rdflib.Graph()
    with open(filename) as infile:
        result = g.parse(infile)
        #print("parsed!")
        #print("graph has %s statements." % len(g))
        #f = open('test2.txt', 'w')
        reactions = g.subjects(URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#biochemicalReaction'))
        #metabolites = g.subjects(predicate='http://www.w3.org/1999/02/22-rdf-syntax-ns#type', object='http://www.biopax.org/release/biopax-level2.owl#biochemicalReaction')
        #print(reactions)
        # for subj, pred, obj in g:
            # f.write(subj.encode('utf-8') + " " + pred.encode('utf-8') + " " + obj.encode('utf-8'))
            # f.write(unicode("\n"))
        
        for rxn in reactions:
            #print(rxn)
            #comment = g.objects(rxn, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#COMMENT'))
            comment = g.predicate_objects(rxn)
            #for cmt in comment:
                #print cmt[0] + "\t" + cmt[1]
            
        

def read_biopax_level2(filename):
    g = rdflib.Graph()
    
    out_reactions = []
    out_metabolites = {}
    out_references = {}

    with open(filename) as infile:
        result = g.parse(infile)
        reactions = g.subjects(URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#biochemicalReaction'))

        i = -1
        for reaction in reactions:
            i += 1
            reaction_name = reaction.toPython()
            out_reactions.append({})
            out_reactions[i][u'rdf_name'] = reaction_name
            out_reactions[i][u'left'] = {}
            out_reactions[i][u'right'] = {}
            out_reactions[i][u'comment'] = set()
            out_reactions[i][u'xref'] = []
            out_reactions[i][u'EC'] = []
            for side in ((u'LEFT', u'left'), (u'RIGHT', u'right')):
                for reactant in g.objects(reaction, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#'+side[0])):
                    metabolite_name = ""
                    for _name in g.objects(reactant,URIRef(u'http://www.biopax.org/release/biopax-level2.owl#PHYSICAL-ENTITY')):
                        metabolite_name = _name.toPython()
                        #print metabolite_name.toPython()
                        #print str(metabolite_name)
                        #print repr(metabolite_name)
                    for coefficient in g.objects(reactant,URIRef(u'http://www.biopax.org/release/biopax-level2.owl#STOICHIOMETRIC-COEFFICIENT')):
                        #pprint.pprint(out_reactions)
                        out_reactions[i][side[1]][metabolite_name] = float(coefficient)
            for comment in g.objects(reaction, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#COMMENT')):
                out_reactions[i][u'comment'].add(unicode(comment))
            for ref in g.objects(reaction, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#XREF')):
                out_reactions[i][u'xref'].append(ref.toPython())
            for name in g.objects(reaction, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#NAME')):
                out_reactions[i][u'name'] = unicode(name)
            for EC_num in g.objects(reaction, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#EC-NUMBER')):
                out_reactions[i][u'EC'].append(unicode(EC_num))
        
        #metabolites = g.subjects(URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#smallMolecule'))
        metabolites = g.subjects(URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#physicalEntity'))
        
        for metabolite in metabolites:
            metabolite_name = metabolite.toPython()
            out_metabolites[metabolite_name] = {}
            out_metabolites[metabolite_name][u'xref'] = []
            for name in g.objects(metabolite, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#NAME')):
                out_metabolites[metabolite_name][u'name'] = unicode(name)
            for datasource in g.objects(metabolite, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#DATA-SOURCE')):
                out_metabolites[metabolite_name][u'data_source'] = datasource.toPython()
            for formula in g.objects(metabolite, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#CHEMICAL-FORMULA')):
                out_metabolites[metabolite_name][u'formula'] = unicode(formula)
            for ref in g.objects(metabolite, URIRef(u'http://www.biopax.org/release/biopax-level2.owl#XREF')):
                out_metabolites[metabolite_name][u'xref'].append(ref.toPython())
        
        
        for reftype in (URIRef(u'http://www.biopax.org/release/biopax-level2.owl#unificationXref'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#relationshipXref'), URIRef(u'http://www.biopax.org/release/biopax-level2.owl#publicationXref')):
            references = g.subjects(URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), reftype)
            for reference in references:
                reference_name = reference.toPython()
                out_references[reference_name] = {}
                for id in g.objects(reference,URIRef(u'http://www.biopax.org/release/biopax-level2.owl#ID')):
                    out_references[reference_name][u'id'] = unicode(id)
                for db in g.objects(reference,URIRef(u'http://www.biopax.org/release/biopax-level2.owl#DB')):
                    out_references[reference_name][u'db'] = unicode(db)
                for type in g.objects(reference,URIRef(u'http://www.biopax.org/release/biopax-level2.owl#RELATIONSHIP-TYPE')):
                    out_references[reference_name][u'type'] = unicode(type)
        pp = pprint.PrettyPrinter(depth=6)
        #pp.pprint(out_reactions)
        #pp.pprint(out_metabolites)
        #pp.pprint(out_references)
        return (out_reactions, out_metabolites, out_references)


# def reactions_from_rhea_biopax2(reactions, metabolites, references): #data structures are the ones returned from read_biopax_level2()
    # out_reactions = {}
    # pp = pprint.PrettyPrinter(depth=6)
    # pp.pprint(reactions)
    # pp.pprint(metabolites)
    # pp.pprint(references)

    # for reaction in reactions:
        # name = reaction[u'name']
