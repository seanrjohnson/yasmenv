import rdflib
from rdflib import URIRef

def subset_owl(infilename, outfilename, subset_size=10, predicate_anchor=u'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', object_anchor=u'http://www.biopax.org/release/biopax-level2.owl#biochemicalReaction'):
    """
        takes the path of a biopax level 2 owl file, and generates a subset of it with all of the records needed to describe a certain number of records (subset_size).
        By default, the types of records it tries to describe fully are reactions, but changing the 'predicate_anchor' or 'object_anchor' parameters will 
        result in an output that is centered around some other kind of object
        
        Algorithm:
            1. find all subject URIs with predicate predicate_anchor, and object object_anchor
            2. add the subjects to a queue
            3. for every URI in the queue, find all triples for which it is the subject
                3a. add the triple to the output
                3b. if the object of the triple has never been in the queue, add it to the queue
                3c. remove the URI from the queue
                3d. repeat step 3 until the queue is empty
            
            4. print the output as an xml file
    """
    
    g = rdflib.Graph()
    outgraph = rdflib.Graph()
    with open(infilename) as infile:
        result = g.parse(infile)
        reactions = g.subjects(URIRef(predicate_anchor), URIRef(object_anchor))
        queue = set()
       
        i = 0
        for reaction in reactions:
            if (i < subset_size):
                queue.add(reaction)
                i += 1
            else:
                break
        #print(queue)
        seen = set()
        #triples = []
        while (len(queue) > 0):    # queue size > 0 add children of queue elements to queue, add queue elements to ("seen")
            subject = queue.pop()
            seen.add(subject)
            children = g.predicate_objects(subject)
            
            for child in children:
                outgraph.add((subject,child[0],child[1]))
                if child not in seen: 
                    queue.add(child[1])
    
    with open(outfilename, 'w') as outfile:
        outfile.write(outgraph.serialize())

if __name__ == '__main__':
    #subset_owl("E:\\databases\\Rhea\\rhea-biopax_lite_truncated.owl","rhea_bipoax2_test.owl")
    #subset_owl("E:\\databases\\Rhea\\rhea-biopax.owl","test_out4.txt",5)
    subset_owl("C:\\Users\\LangeLab\\Documents\\sean\\scripts\\python\\modeler\\test\\data\\rhea_bipoax2_lite_test.owl","test_out3.txt",1)