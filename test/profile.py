import sys
sys.path.append("../")
from unittest import TestCase
import unittest
import biopax
from sequence import Sequence
from reaction import Reaction
import model
from StringIO import StringIO
import test_data
import pprint
import pandas as pd
import modelreaction
from modelreaction import ModelReaction
import util
### adapted from cobrapy test\__init__.py
import os.path
import biocyc as bc


data_directory = os.path.join(os.path.split(os.path.abspath(__file__))[0], "data")
if not data_directory.endswith(os.path.sep):
    data_directory += os.path.sep
### /

inf = float('inf')
ninf = -inf

pp = pprint.pprint


def test_tiny():
    import gurobi_solver as gs
    solve = gs.Solver()
    
    m = model.from_tsv(data_directory+'tiny_model_expression.tsv', expression_col='Expression')
    #sol = solve.frank(m)
    #pp(sol)
    
    sol = solve.frank(m, linear_objective=False)
    pp(sol)
    
test_tiny()