NAME fba
* Max problem is converted into Min one
ROWS
 N  OBJ
 E  PC2     
 E  PC1     
 E  TAG     
 E  glycerol
 E  DAG2    
 E  DAG3    
 E  DAG1    
COLUMNS
    <=> glycerol  glycerol  1
    glycerol <=> DAG1  glycerol  -1
    glycerol <=> DAG1  DAG1      1
    DAG1 <==> PC1  PC1       1
    DAG1 <==> PC1  DAG1      -1
    PC1 <=> PC2  PC2       1
    PC1 <=> PC2  PC1       -1
    PC1 <=> DAG2  PC1       -1
    PC1 <=> DAG2  DAG2      1
    DAG2 <=> DAG3  DAG2      -1
    DAG2 <=> DAG3  DAG3      1
    DAG2 <=> TAG  TAG       1
    DAG2 <=> TAG  DAG2      -1
    PC2 <=> DAG2  PC2       -1
    PC2 <=> DAG2  DAG2      1
    DAG1 <=> TAG  TAG       1
    DAG1 <=> TAG  DAG1      -1
    PC2 <=> DAG1  PC2       -1
    PC2 <=> DAG1  DAG1      1
    TAG <=>   OBJ       -1
    TAG <=>   TAG       -1
RHS
BOUNDS
 UP BND1      <=> glycerol  1000
 LO BND1      glycerol <=> DAG1  -1000
 UP BND1      glycerol <=> DAG1  1000
 LO BND1      DAG1 <==> PC1  -1000
 UP BND1      DAG1 <==> PC1  1000
 LO BND1      PC1 <=> PC2  -1000
 UP BND1      PC1 <=> PC2  1000
 LO BND1      PC1 <=> DAG2  -1000
 UP BND1      PC1 <=> DAG2  1000
 LO BND1      DAG2 <=> DAG3  -1000
 UP BND1      DAG2 <=> DAG3  1000
 LO BND1      DAG2 <=> TAG  -1000
 UP BND1      DAG2 <=> TAG  1000
 LO BND1      PC2 <=> DAG2  -1000
 UP BND1      PC2 <=> DAG2  1000
 LO BND1      DAG1 <=> TAG  -1000
 UP BND1      DAG1 <=> TAG  1000
 LO BND1      PC2 <=> DAG1  -1000
 UP BND1      PC2 <=> DAG1  1000
 UP BND1      TAG <=>   1000
ENDATA
