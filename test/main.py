import sys
sys.path.append("../")
from unittest import TestCase
import unittest
import biopax
from sequence import Sequence
from reaction import Reaction
import model
from StringIO import StringIO
import test_data
import pprint
import pandas as pd
import modelreaction
from modelreaction import ModelReaction
import util
### adapted from cobrapy test\__init__.py
import os.path
import biocyc as bc


data_directory = os.path.join(os.path.split(os.path.abspath(__file__))[0], "data")
if not data_directory.endswith(os.path.sep):
    data_directory += os.path.sep
### /

inf = float('inf')
ninf = -inf

pp = pprint.pprint

# class reactions_test(TestCase):
    # def setUp(self):
        # self.react_dict = Reaction.from_rhea_biopax2(*biopax.read_biopax_level2(data_directory+'rhea_bipoax2_test.owl'))
        
    # def test_identity(self):
        #import rdflib
        # out_dict = dict()
        # for react in self.react_dict.iterkeys():
            # name = self.react_dict[react].notes['RheaID']
            
            # out_dict[name] = {}
            # out_dict[name]['formula'] = self.react_dict[react].name
            # out_dict[name]['direction'] = self.react_dict[react].direction
            # out_dict[name]['master'] = self.react_dict[react].notes['master']
            # for annotation_type in self.react_dict[react].annotations:
                # out_dict[name][annotation_type] = " ".join(self.react_dict[react].annotations[annotation_type])
        # df = pd.DataFrame.from_dict(out_dict, orient='index')
        
        # df.to_csv('test_out5.csv')
        
        # #pp = pprint.PrettyPrinter(depth=6)
        # #pp.pprint(self.react_dict)
        # #print dir(rdflib.term.Literal)
        # #print dir(rdflib.term.URIRef)
        # pass



class sequence_test(TestCase):
    def setUp(self):
        #runs before each test case
        self.seq_dict = Sequence.from_fasta(StringIO(test_data.fastaData), '^[a-z]*\|([0-9]+)\|.+$')
        Sequence.annotations_from_tsv(self.seq_dict, StringIO(test_data.sequenceAnnotationData), 'genes')

    def test_read_fasta(self):
        self.assertEqual(len(self.seq_dict), 3)
        self.assertTrue(u'7245280' in self.seq_dict)
    
    def test_read_annotation(self):
        #pprint.pprint(self.seq_dict, depth=5)
        self.assertEqual(self.seq_dict[u'7245280'].annotation['genes'], set((u'ATH1','ATH12','ATH7','ATH9')))
        self.assertNotEqual(self.seq_dict[u'7245280'].annotation['genes'], self.seq_dict['7245279'].annotation['genes'])
        self.assertTrue(u'ATH8' in self.seq_dict[u'7245279'].annotation['genes'])
        self.assertEqual(self.seq_dict[u'7245278'].annotation['genes'], set())
        self.assertEqual(u'=', '=')

class modelreaction_test(TestCase):
    def test_net(self):
        t = list() #test
        a = list() #answer
        t.append(ModelReaction('',{'a':1.,'b':1.},{'c':1.,'d':1.},0,0,0))
        a.append(ModelReaction('',{'a':1.,'b':1.},{'c':1.,'d':1.},0,0,0))
        
        t.append(ModelReaction('',{'a':-1.,'b':1.},{'c':-1.,'d':1.},0,0,0))
        a.append(ModelReaction('',{'c':1.,'b':1.},{'a':1.,'d':1.},0,0,0))
        
        t.append(ModelReaction('',{'a':0.,'b':1},{'c':-1,'d':0},0,0,0))
        a.append(ModelReaction('',{'c':1,'b':1},{},0,0,0))
        
        t.append(ModelReaction('',{'a':1,'b':1},{'a':3,'d':1},0,0,0))
        a.append(ModelReaction('',{'b':1},{'a':2,'d':1},0,0,0))
        
        t.append(ModelReaction('',{'a':1,'b':1},{'a':-3,'d':1},0,0,0))
        a.append(ModelReaction('',{'a':4,'b':1},{'d':1},0,0,0))
        
        t.append(ModelReaction('',{'a':-3,'b':-2},{'a':-3,'b':-2},0,0,0))
        a.append(ModelReaction('',{},{},0,0,0))
        
        for i in range(len(t)):
            self.assertEqual(t[i].net().left, a[i].left)
            self.assertEqual(t[i].net().right, a[i].right)
    
    def test_convert_bounds_to_finite(self):
        t = list()
        a = list()
        
        t.append(([ninf, 5], [-5, inf])) #((lbs),(ubs))
        a.append((-5,5,)) #(lb < x),(ub > x)
        
        for i in range(len(t)):
            self.assertEqual(modelreaction.convert_bounds_to_infinite(*modelreaction.convert_bounds_to_finite(t[i][0],t[i][1])), t[i])
            
            (lower, upper, low_inf, high_inf) = modelreaction.convert_bounds_to_finite(*t[i])
            #print (lower, upper, low_inf, high_inf)
            for x in range(len(lower)):
                if t[i][0][x] == -inf:
                    self.assertLess(lower[x], a[i][0])
                else:
                    self.assertEqual(t[i][0][x], lower[x])
            for x in range(len(upper)):
                if t[i][1][x] == inf:
                    self.assertGreater(upper[x], a[i][1])
                else:
                    self.assertEqual(t[i][1][x], upper[x])
                
            self.assertTrue(low_inf < a[i][0])
            self.assertTrue(high_inf > a[i][1])
            
            
    
    def test_overlap(self):
        t = list()
        a = list()
        
        t.append((0,0,0,0))
        a.append('contained')
        
        t.append((-10,0,0,10))
        a.append('overlap')
        
        t.append((ninf,inf,ninf,inf))
        a.append('contained')
        
        t.append((ninf,-5,5,inf))
        a.append('separate')
        
        t.append((ninf,-5,ninf,inf))
        a.append('contained')
        
        t.append((0,10,8,10))
        a.append('contained')
        
        t.append((0,10,8,11))
        a.append('overlap')
        
        t.append((0,10,10,11))
        a.append('overlap')
        
        for i in range(len(t)):
            self.assertEqual(modelreaction.overlap(*t[i]), a[i])
    
    def test_split_bounds(self):
        t = list() #test
        a = list() #answer
        t.append(ModelReaction('',{},{},ninf,0,0))
        a.append([(0,0),(0,inf)])
        
        t.append(ModelReaction('',{},{},0,inf,0))
        a.append([(0,inf),(0,0)])
        
        t.append(ModelReaction('',{},{},ninf,inf,0))
        a.append([(0,inf),(0,inf)])
        
        t.append(ModelReaction('',{},{},50,100,0))
        a.append([(50,100),(0,0)])
        
        t.append(ModelReaction('',{},{},-100,-50,0))
        a.append([(0,0),(50,100)])
        
        t.append(ModelReaction('',{},{},-100,50,0))
        a.append([(0,50),(0,100)])
        
        for i in range(len(t)):
            self.assertEqual(t[i].split_bounds(), a[i])

class model_test(TestCase):
    def setUp(self):
        self.tiny = model.from_tsv(data_directory+'tiny_model.tsv')
        self.tiny_inf = model.from_tsv(data_directory+'tiny_model_inf.tsv')

    def test_read_model(self):
        self.assertEqual(self.tiny['glycerol <=> DAG1'].name, 'glycerol <=> DAG1')
        self.assertEqual(self.tiny['PC1 <=> DAG2'].name, 'PC1 <=> DAG2')
        
        self.assertEqual(self.tiny['<=> glycerol'].lb, 0)
        self.assertEqual(self.tiny['PC1 <=> DAG2'].lb, -1000)
        self.assertEqual(self.tiny['<=> glycerol'].ub, 1000)
        self.assertEqual(self.tiny['TAG <=>'].ub, 1000)
        self.assertEqual(self.tiny['TAG <=>'].objective, 1)
        self.assertEqual(self.tiny['<=> glycerol'].objective, 0)
        self.assertEqual(len(self.tiny.reactions_list()), 11)
        
        self.assertEqual(self.tiny_inf['glycerol <=> DAG1'].name, 'glycerol <=> DAG1')
        self.assertEqual(self.tiny_inf['PC1 <=> DAG2'].name, 'PC1 <=> DAG2')
        self.assertEqual(self.tiny_inf['<=> glycerol'].lb, 0)
        self.assertEqual(self.tiny_inf['PC1 <=> DAG2'].lb, ninf)
        self.assertEqual(self.tiny_inf['<=> glycerol'].ub, inf)
        self.assertEqual(self.tiny_inf['TAG <=>'].ub, 1000)
        self.assertEqual(len(self.tiny_inf.reactions_list()), 11)
    
    def test_add_reaction(self):
        orig_mets_len = self.tiny_inf.len_metabolites()
        self.assertEqual(len(self.tiny_inf.reactions_list()), 11)
        self.tiny_inf.add_reaction(ModelReaction('new',{'a':2,'b':1},{'c':1},ninf,inf,1))
        self.assertEqual(len(self.tiny_inf.reactions_list()), 12)
        self.assertEqual(self.tiny_inf.reactions_list()[-1].name, 'new')
        self.assertEqual(orig_mets_len + 3, self.tiny_inf.len_metabolites())
    
    def test_metabolite_connectivity(self):
        con = self.tiny_inf.metabolite_connectivity()
        std_con = {'glycerol': 2, 'DAG1': 4, 'PC1': 3, 'PC2': 3, 'DAG2': 4, 'DAG3': 1, 'TAG': 3}
        self.assertEqual(len(std_con), len(con))
        for (k,v) in std_con.iteritems():
           self.assertEqual((k,v), (k, con[k]))
        
        self.tiny_inf.add_reaction(ModelReaction('new',{'PC2':3},{'TAG':1},ninf,inf,1))
        self.tiny_inf.add_reaction(ModelReaction('new2',{'a':1},{'b':7},ninf,inf,1))
        std_con = {'glycerol': 2, 'DAG1': 4, 'PC1': 3, 'PC2': 4, 'DAG2': 4, 'DAG3': 1, 'TAG': 4, 'a': 1, 'b': 1}
        con = self.tiny_inf.metabolite_connectivity()
        self.assertEqual(len(std_con), len(con))
        for (k,v) in std_con.iteritems():
           self.assertEqual((k,v), (k, con[k]))

    def test_sparse_matrix(self):
        sparse = [(0, 0, 1.0), (0, 1, -1.0), (1, 1, 1.0), (1, 2, -1.0), (2, 2, 1.0), (2, 3, -1.0), (3, 3, 1.0), (2, 4, -1.0), (4, 4, 1.0), (4, 5, -1.0), (5, 5, 1.0), (4, 6, -1.0), (6, 6, 1.0), (3, 7, -1.0), (4, 7, 1.0), (1, 8, -1.0), (6, 8, 1.0), (3, 9, -1.0), (1, 9, 1.0), (6, 10, -1.0)]
        
        model_sparse = self.tiny_inf.sparse_matrix()
        
        #order is not important
        for tup in sparse:
            self.assertIn(tup, model_sparse)
        for tup in model_sparse:
            self.assertIn(tup, sparse)
    
    def test_find_singlets(self):
        #print self.tiny_inf.find_singlets()
        self.assertEqual(len(self.tiny_inf.find_singlets()), 1) #DAG3 is a singlet in the original model
        self.tiny_inf.add_reaction(ModelReaction('new',{'a':2,'b':1},{'c':1},ninf,inf,1))
        #all the meatabolites in the new reaction should be singlets
        self.assertEqual(len(self.tiny_inf.find_singlets()), 4)
        self.assertIn('a', self.tiny_inf.find_singlets())
        self.assertIn('b', self.tiny_inf.find_singlets())
        self.assertIn('c', self.tiny_inf.find_singlets())
    
    def test_make_irreversible(self):
        self.tiny_inf.add_reaction(ModelReaction('new',{'a':2,'b':1},{'c':1, 'd':5},ninf,inf,1))
        (irrev, irrev_map) = self.tiny_inf.make_irreversible()
        self.assertEqual(len(irrev.reactions_list()), (len(self.tiny_inf.reactions_list())*2))
        #irrev.to_tsv('test_model_out.tsv')

#class biocyc_test(TestCase):
    # def test_instantiate(self):
        # rxns = bc.Dat(data_directory+'progress.dat', rec_class=bc.Reaction)
        # cpds = bc.Dat(data_directory+'compounds.dat', rec_class=bc.Compound)
        # classes = bc.Dat(data_directory+'classes.dat', rec_class=bc.Class)
        # #self.assertIn('RXN-8258', rxns)
        # #instances = rxns['RXN-12939'].instances(cpds, classes)
        # #pp(instances)
        # #self.assertEqual(len(instances), len(set([str(x) for x in instances])))
        # #balanced = rxns['RXN-12939'].balanced_instances(cpds, classes)
        
        # for rxn_name in rxns.all_records().keys():
            # #(balanced_include, unbalanced, balanced_not_include)
            # (balanced_include, unbalanced, balanced_not_include) = rxns[rxn_name].processed_instances(cpds, classes)
            # if len(balanced_include) > 0:
                # for (num, rxn) in enumerate(balanced_include):
                    # print rxn_name + "\t" + str(num) + "\t" + bc.reaction_as_str(rxn['reaction'])
            # else:
                # if len(unbalanced) > 0:
                    # for (num, rxn) in enumerate(unbalanced):
                        # print rxn_name + "\t" + "NOT BALANCED" + "\t" + bc.reaction_as_str(rxn['reaction']) + "\t" + rxn['tag']
            # if len(balanced_not_include) > 0:
                # for (num, rxn) in enumerate(balanced_not_include):
                    # print rxn_name + "\t" + 'BALANCED NOT INCLUDE' + "\t" + bc.reaction_as_str(rxn['reaction'])
            
        
    # def test_instantiate2(self):
        # # CARBOXYLESTERASE-RXN
        # rxns = bc.Dat(data_directory+'reactions.dat', rec_class=bc.Reaction)
        # cpds = bc.Dat(data_directory+'compounds.dat', rec_class=bc.Compound)
        # classes = bc.Dat(data_directory+'classes.dat', rec_class=bc.Class)
        # pp(len(bc.compounds_with_class('Carboxylic-esters', cpds, classes)))
        # pp(len(bc.compounds_with_class('Alcohols', cpds, classes)))
        # pp(len(bc.compounds_with_class('Carboxylates', cpds, classes)))
        
    
    # def test_class_ancestors(self):
        # classes = bc.Dat(data_directory+'classes.dat', rec_class=bc.Class)
        # #pp(classes['Coniferyl-Esters'].ancestors(classes))
        # self.assertEqual(classes['Coniferyl-Esters'].ancestors(classes), set(['Chemicals', 'Compounds', 'Compounds-And-Elements', 'Coniferyl-Esters', 'Esters', 'FRAMES']))
    
    # def test_compounds_with_class(self):
        # cpds = bc.Dat(data_directory+'compounds.dat', rec_class=bc.Compound)
        # classes = bc.Dat(data_directory+'classes.dat', rec_class=bc.Class)
        # pp(bc.compounds_with_class('Coniferyl-Esters', cpds, classes))

# class glpk_test(TestCase):
    # def setUp(self):
        
        # #self.bna = model.from_tsv(data_directory+'bna572_mint_full.tsv')
        # #self.model = model.from_tsv(data_directory+'AraGEM.tsv')
        # self.tiny_model = model.from_tsv(data_directory+'tiny_model.tsv')
        # self.tiny_inf_model = model.from_tsv(data_directory+'tiny_model_inf.tsv')
        
    # def test_fba(self):
        # import glpk_solver as lp
        
        # # print lp.format_list_of_tuples(self.model.metabolite_connectivity().iteritems())
        # # print self.model.metabolite_connectivity()
        
        # solver = lp.Solver()
        # #print lp.format_fba(solver.fba(self.model))#, minimize_flux=True))
        # self.assertEqual(solver.fba(self.tiny_model, minimize_flux=True),solver.fba(self.tiny_inf_model, minimize_flux=True))
        
        # # print lp.format_fva(solver.fva(self.model))
        # # pprint.pprint(solver.fba(self.model))
        
    # # def test_double_ko(self):
        # # import glpk_solver as lp
        # # solver = lp.Solver()
        # # dko = solver.double_knockout(self.bna)
        # # #pprint.pprint(dko)
        # # print lp.format_list_of_tuples(dko['kos'])
        
        # # #sko = solver.single_knockout(self.bna)
        # # #print lp.format_list_of_tuples(sko['kos'])
        # # #pprint.pprint(sko)

# class gurobi_test(TestCase):
    # def setUp(self):
        
        # #self.bna = model.from_tsv(data_directory+'bna572_mint_full.tsv')
        # self.bna = model.from_tsv(data_directory+'bna572.tsv')
        # #self.model = model.from_tsv(data_directory+'AraGEM.tsv')
        # self.tiny_model = model.from_tsv(data_directory+'tiny_model.tsv')
        # self.tiny_inf_model = model.from_tsv(data_directory+'tiny_model_inf.tsv')
        
    # def test_fba(self):
        # import gurobi_solver as lp
        
        # # print lp.format_list_of_tuples(self.model.metabolite_connectivity().iteritems())
        # # print self.model.metabolite_connectivity()
        
        # solver = lp.Solver()
        # sol = solver.fba(self.bna)
        # #print util.format_fba()#, minimize_flux=True))
        # print sol['obj']
        # #print solver.fba(self.tiny_model, minimize_flux=True)
        # #self.assertEqual(solver.fba(self.tiny_model, minimize_flux=True),solver.fba(self.tiny_inf_model, minimize_flux=True))
        
        # # print lp.format_fva(solver.fva(self.model))
        # # pprint.pprint(solver.fba(self.model))
    # def test_moma(self):
         # pass
         # #pp(moma_test_gurobi('tinier_model.tsv'))
         # #pp(moma_test_gurobi('bna572_mint_full.tsv')['obj'])
    # # def test_double_ko(self):
        # # import glpk_solver as lp
        # # solver = lp.Solver()
        # # dko = solver.double_knockout(self.bna)
        # # #pprint.pprint(dko)
        # # print lp.format_list_of_tuples(dko['kos'])
        
        # # #sko = solver.single_knockout(self.bna)
        # # #print lp.format_list_of_tuples(sko['kos'])
        # # #pprint.pprint(sko)

class test_frank(TestCase):

        
    def test_tiny(self):
        import gurobi_solver as gs
        solve = gs.Solver()
        
        m = model.from_tsv(data_directory+'tiny_model_expression.tsv', expression_col='Expression')
        #sol = solve.frank(m)
        #pp(sol)
        
        sol = solve.frank(m, linear_objective=False)
        pp(sol)
    
    
def moma_test_gurobi(model_name):
    import gurobi_solver as gs

    
    solve = gs.Solver()

    m = model.from_tsv(data_directory+model_name)
    fba_solution = solve.fba(m, minimize_flux=False)
    pp(fba_solution['fluxes'])
    fba_solution = [x[1] for x in fba_solution['fluxes']]
    
    
    #print lp.format_fba(lpsolve.fba(tiny_model))#, minimize_flux=True))
    #pp(fba_solution)
    
    target = fba_solution
    #target = [1000.0, 100.0, 250.0, 620.0, 352.0, 0.0, 0.0, 0.0, 1000.0, 0.0, 1000.0]
    
    moma_solution = solve.moma(m, target, fba_optimize_constraint=False)
    #moma_solution = cpsolve.moma(m, target)
    #pp(moma_solution)
    return moma_solution

# class test_convex(TestCase):
    # def test_moma(self):
        # import convexprogramming as cp
        # import glpk_solver as lp
        
        # moma_test('tiny_model.tsv')
        # #moma_test('bna572_mint_full.tsv')
        
        # # lpsolve = lp.Solver()
        # # cpsolve = cp.ConvexSolver()
        
        # # tiny_model = model.from_tsv(data_directory+'tiny_model.tsv')
        # # fba_solution = lpsolve.fba(tiny_model, minimize_flux=True)
        # # fba_solution = [x[1] for x in fba_solution['fluxes']]
        
        # # #print lp.format_fba(lpsolve.fba(tiny_model))#, minimize_flux=True))
        # # pp(fba_solution)
        
        # # target = fba_solution
        # # #target = [1000.0, 100.0, 250.0, 620.0, 352.0, 0.0, 0.0, 0.0, 1000.0, 0.0, 1000.0]
        
        # # moma_solution = cpsolve.moma2(tiny_model, target)
        # # pp(moma_solution)
        
# def moma_test(model_name):
    # import convexprogramming as cp
    # import glpk_solver as lp
    
    # lpsolve = lp.Solver()
    # cpsolve = cp.ConvexSolver()
    
    # m = model.from_tsv(data_directory+model_name)
    # fba_solution = lpsolve.fba(m, minimize_flux=True)
    # fba_solution = [x[1] for x in fba_solution['fluxes']]
    
    # #print lp.format_fba(lpsolve.fba(tiny_model))#, minimize_flux=True))
    # #pp(fba_solution)
    
    # target = fba_solution
    # #target = [1000.0, 100.0, 250.0, 620.0, 352.0, 0.0, 0.0, 0.0, 1000.0, 0.0, 1000.0]
    
    # moma_solution = cpsolve.moma2(m, target)
    # #moma_solution = cpsolve.moma(m, target)
    # pp(moma_solution)
        
if __name__ == '__main__':
    unittest.main()
