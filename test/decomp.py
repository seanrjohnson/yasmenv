import sys
sys.path.append("../")
import model
import pprint
import pandas as pd
import modelreaction
from modelreaction import ModelReaction
#import glpk_solver as lp
import gurobi_solver as gs
import convexprogramming as cp
from util import *

### adapted from cobrapy test\__init__.py
import os.path

data_directory = os.path.join(os.path.split(os.path.abspath(__file__))[0], "data")
if not data_directory.endswith(os.path.sep):
    data_directory += os.path.sep
### /

inf = float('inf')
ninf = -inf
solver = gs.Solver()
pp = pprint.pprint

cpsolver = cp.ConvexSolver()

def main():
    
    full = model.from_tsv(data_directory+'tiny_model_comp.tsv')
    
    
    #fba_full = solver.fba(full, minimize_flux=False)
    #pp(fba_full)
    
    ## REMOVE EXTERNAL METABOLITES ##
    no_ext1 = full.remove_metabolites(['TAG[e]', 'glycerol[e]'])
    no_ext2 = full.remove_metabolites(full.metabolite_names(compartments=['e']))
    #no_ext2 = full.decompartmentalize(preserve=['c','p','lb']) #this should do the same as the previous
    
    fba_no_ext1 = solver.fba(no_ext1, minimize_flux=True)
    fba_no_ext2 = solver.fba(no_ext2, minimize_flux=True)
    #pp(no_ext1.metabolite_names())
    #pp(no_ext2.metabolite_names())
    #pp(fba_no_ext1)
    #pp(fba_no_ext2)
    #print (fba_no_ext1 == fba_no_ext2)
    
    
    ## DECOMPARTMENTALIZE THE REMAINING REACTIONS ##
    decomp1 = no_ext1.decompartmentalize()
    decomp2 = no_ext2.decompartmentalize()

    #pp(decomp1.metabolite_names())
    #pp(decomp2.metabolite_names())
    
    #for x in decomp1.reactions_list():
    #    print x.as_str()
    #print decomp1.sparse_matrix()
        
    fba_decomp1 = solver.fba(decomp1, minimize_flux=False)
    fba_decomp2 = solver.fba(decomp2, minimize_flux=False)
    pp(fba_decomp1)
    #pp(fba_decomp2)
    
    ## CLEAN THE MODELS ##
    (cleaned1, rmap1) = decomp1.clean(merge_args={'metabolites_merge':max})
    (cleaned2, rmap2) = decomp2.clean(merge_args={'metabolites_merge':max})
    #pp([x.name for x in decomp1.reactions_list()])
    #pp([x.name for x in cleaned1.reactions_list()])
    #for x in decomp1.reactions_list():
    #    print x.as_str()
    #print "cleaned"
    #for x in cleaned1.reactions_list():
    #    print x.as_str()
    pp(rmap1)
    
    fba_cleaned1 = solver.fba(cleaned1, minimize_flux=False)
    fba_cleaned2 = solver.fba(cleaned2, minimize_flux=False)
    pp(fba_cleaned1)
    #pp(fba_cleaned2)

    ##  COMPARE THE DECOMPARTMENTALIZED FLUXES TO THE COMPARTMENTALIZED FLUXES ##
    
    #
    # we want to compare the fluxes from the original (compartmentalized) model to the fluxes
    # from the decompartmentalized model.
    # Some reactions (mainly uniporter transport reactions) from the original model were removed in
    # the formation of the decompartmentalized model and should not have any weight in the
    # comparison.
    # Other reactions were combined (for example if they have the same stoichiometry,
    # but different compartments. The sum of the two reaction fluxes in the original model should
    # be compared to the single reaction flux of the decompartmentalized model.
    #
    # The relationship between reactions in the original model and the decompartmentalized model
    # is stored in the reaction maps (rmap1, and rmap2) produced by the "clean" function
    # Eliminated reactions are marked by None values in the reaction maps.
    # 

    # first we want to express reaction fluxes from the original model in terms of the equivalent reactions
    # in the decomp model
    orig = dict()
    for (rxn, flux) in fba_no_ext1['fluxes']:
        if rmap1[rxn] is not None:
            orig[rmap1[rxn]] = orig.get(rxn, 0.0) + flux
    
    #now lets compare them to the fluxes from the decompartmentalized model
    diff = list()
    relative_diff = list()
    for (rxn, flux) in fba_cleaned1['fluxes']:
        print ""
        print (rxn, flux, orig[rxn])
        print ""
        diff.append((rxn, flux - orig[rxn]))
        relative_diff.append((rxn, (flux - orig[rxn])/(max((abs(flux), abs(orig[rxn]))) or 1 ) ))
    
    pp(diff)
    pp(relative_diff)

def bna572_decompartmentalize():
    full = model.from_tsv(data_directory+'bna572_mint_full_maximize_biomass.tsv')
    fba_full = solver.fba(full, minimize_flux=True)
    #pp(fba_full)
    
    naive_decomp = full.decompartmentalize()
    (naive_clean, naive_map) = naive_decomp.clean(merge_args={'metabolites_merge':max})
    fba_naive = solver.fba(naive_clean, minimize_flux=True)
    # express reaction fluxes from the original model in terms of the equivalent reactions
    # in the decomp model
    orig_naive = dict()
    for (rxn, flux) in fba_full['fluxes']:
        if naive_map[rxn] is not None:
            orig_naive[naive_map[rxn]] = orig_naive.get(rxn, 0.0) + flux
    
    other_decomp = full.decompartmentalize(preserve=['is','lu','ap']) #the "not quite as naive" decompartmentalization
    (other_clean, other_map) = naive_decomp.clean(merge_args={'metabolites_merge':max})
    fba_other = solver.fba(other_clean, minimize_flux=True)
    
    # express reaction fluxes from the original model in terms of the equivalent reactions
    # in the decomp model
    orig_other = dict()
    for (rxn, flux) in fba_full['fluxes']:
        if other_map[rxn] is not None:
            orig_other[other_map[rxn]] = orig_other.get(rxn, 0.0) + flux
    
    diff = list()
    relative_diff = list()
    naive_moma_target = list()
    for (rxn, flux) in fba_naive['fluxes']:
        #print ""
        #print (rxn, flux, orig_naive[rxn])
        #print ""
        diff.append((rxn, flux - orig_naive[rxn]))
        naive_moma_target.append(orig_naive[rxn])
        relative_diff.append((rxn, (flux - orig_naive[rxn])/(max((abs(flux), abs(orig_naive[rxn]), 0.0000000001)) or 1 ) )) #the 0.0001 is just so tiny fluxes don't show up as huge relative differences, another way would be to threshold the fluxes
    #pp(diff)
    #pp(relative_diff)
    average_relative_naive = sum([abs(x[1]) for x in relative_diff if x[1] != 0.0])/len([abs(x[1]) for x in relative_diff if x[1] != 0.0])
    average_diff_naive = sum([abs(x[1]) for x in diff if x[1] != 0.0])/len([abs(x[1]) for x in diff if x[1] != 0.0])
    naive_diff = diff
    
    diff = list()
    relative_diff = list()
    other_moma_target = list()
    for (rxn, flux) in fba_other['fluxes']:
        #print ""
        #print (rxn, flux, orig_other[rxn])
        #print ""
        other_moma_target.append(orig_other[rxn])
        diff.append((rxn, flux - orig_other[rxn]))
        relative_diff.append((rxn, (flux - orig_other[rxn])/(max((abs(flux), abs(orig_other[rxn]), 0.0000000001)) or 1) ))
    #pp(diff)
    #pp(relative_diff)
    
    
    
    average_diff_other = sum([abs(x[1]) for x in diff if abs(x[1]) != 0.0])/len([abs(x[1]) for x in diff if abs(x[1]) != 0.0])
    average_relative_other = sum([abs(x[1]) for x in relative_diff if x[1] != 0.0])/len([abs(x[1]) for x in relative_diff if x[1] != 0.0])
    
    print "Avg diff naive: %f " % average_diff_naive
    print "Avg diff other: %f " % average_diff_other
    
    print "Avg relative diff naive: %f " % average_relative_naive
    print "Avg relative diff other: %f " % average_relative_other
    
    # arrange from most to least different, look for pattern
    # change objective to maximize biomass rather than minimize photon uptake
    # compare with FVA
    
    naive_moma = solver.moma(naive_clean, naive_moma_target)
    other_moma = solver.moma(other_clean, other_moma_target)
    
    # with open('bna_fba.tsv', "w") as outfile:
        # outfile.write(lp.format_list_of_tuples(fba_full['fluxes']))
    
    # with open("naive_diff.tsv","w") as outfile:
        # outfile.write(lp.format_list_of_tuples(naive_diff))
    # with open("naive_fba.tsv","w") as outfile:
        # outfile.write(lp.format_list_of_tuples(fba_naive['fluxes']))
    with open("naive_moma.tsv","w") as outfile:
        outfile.write(format_list_of_tuples(naive_moma['fluxes']))
    
    # with open("other_diff.tsv","w") as outfile:
        # outfile.write(lp.format_list_of_tuples(diff))
    # with open("other_fba.tsv","w") as outfile:
        # outfile.write(lp.format_list_of_tuples(fba_other['fluxes']))
    with open("other_moma.tsv","w") as outfile:
        outfile.write(format_list_of_tuples(other_moma['fluxes']))
    naive_clean.to_tsv("naive.tsv")
    other_clean.to_tsv("other.tsv")
    
    pp(naive_moma['obj'])
    pp(other_moma['obj'])

    
def analyze_decompartmentalization(full, decomp, rxn_map, out_prefix):
    fba_full = solver.fba(full, minimize_flux=True)
    
    # express reaction fluxes from the original model in terms of the equivalent reactions
    # in the decomp model
    full_to_decomp = dict() #keys are reaction names from decomp, values are sum of fluxes from the equivalent reactions in full
    for (rxn, flux) in fba_full['fluxes']:
        if rxn_map[rxn] is not None:
            full_to_decomp[rxn_map[rxn]['name']] = full_to_decomp.get(rxn_map[rxn]['name'], 0.0) + flux*rxn_map[rxn]['dir']
    
    moma_target = list()
    for rxn in decomp.reactions_list():
        moma_target.append(full_to_decomp[rxn.name])
    
    moma_sol = solver.moma(decomp, moma_target)
    
    ### write results to files ###
    decomp.to_tsv(out_prefix+"_decomp.tsv")
    full.to_tsv(out_prefix+"_full.tsv")
    decomp.to_tsv(out_prefix+"_model.tsv")
    full.to_sif(out_prefix+"_full.sif")
    with open(out_prefix+"_full_fluxes.tsv","w") as outfile:
        outfile.write(format_list_of_tuples(fba_full['fluxes']))
    # with open(out_prefix+"_full_fluxes.tsv","w") as outfile:
        # outfile.write(format_list_of_tuples())
    with open(out_prefix+"_moma.tsv","w") as outfile:
        outfile.write(format_list_of_tuples(moma_sol['fluxes']))
    with open(out_prefix+"_target.tsv","w") as outfile:
        target_out = list()
        for (i, rxn) in enumerate(decomp.reactions_list()):
            target_out.append((rxn.name, moma_target[i]))
        outfile.write(format_list_of_tuples(target_out))
    print ""
    print out_prefix + " full objective"
    pp(fba_full['obj'])
    print out_prefix + " fba objective"
    pp(moma_sol['fba_obj'])
    print out_prefix + " moma objective"
    pp(moma_sol['obj'])
    

def analyze_bna572_decompartmentalize():
    full = model.from_tsv(data_directory+'bna572.tsv')

    #naive_decomp = full.decompartmentalize()
    #(naive, naive_map) = full.decompartmentalize().remove_metabolites(['M_H','M_H2O','M_ATP']).clean(merge_args={'metabolites_merge':max})
    (naive, naive_map) = full.decompartmentalize().clean(merge_args={'metabolites_merge':max})
    
    
    #the "not quite as naive" decompartmentalization
    (other, other_map) = full.decompartmentalize(preserve=['is','lu','ap']).clean(merge_args={'metabolites_merge':max})
    
    analyze_decompartmentalization(full, naive, naive_map, "naive_bna_revert")
    analyze_decompartmentalization(full, other, other_map, "other_bna_revert")
    
    #analyze_decompartmentalization(full, naive, naive_map, "naive2")
    #analyze_decompartmentalization(full, other, other_map, "other2")
    
if __name__ == '__main__':
    #main()
    analyze_bna572_decompartmentalize()
    
    #analyze_bna572_decompartmentalize()
    #pp(other_maps[0])
    #pp(other_maps[1])
    # for (k,v) in other_maps[0].iteritems():
        # if not other_maps[1][k] == v:
            # print 
            # print k,v
            # print k, other_maps[1][k]
    #(other_maps[0] == other_maps[1])
    #print (naive_maps[0] == naive_maps[1])
    
    #bna572_decompartmentalize()
    #bna572_decompartmentalize()
    #print "x"
    #analyze_bna572_decompartmentalize()
