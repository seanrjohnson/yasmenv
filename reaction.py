import biopax
import re
import pprint

class Reaction(object):

    def __init__(self, name, associations, transport, annotations, left_side, right_side, direction, pair, spontaneous, notes, formula, balanced=True):
        self.name = name #string
        self.associations = associations #Key is type, value is set of strings. strings are boolean combinations of annotation names
        self.annotations = annotations #key is type, value is set of strings  #including organism
        self.left_side = left_side #dict keys are metabolite names, values are stoichiometry
        self.right_side = right_side #dict
        self.direction = direction #string can be 'left-to-right', 'right-to-left', 'reversible', 'unknown'
        self.pair = pair #string with a reaction name
        self.spontaneous = spontaneous #boolean
        self.notes = notes # a dict of strings
        self.transport = transport #boolean
        self.formula = formula #string
        self.balanced = balanced #bool
        
    def __repr__(self):
        out = "Reaction(\n"
        for field in vars(self):
            out += "\t" + field + ": " + repr(getattr(self, field)) + "\n"
        out += "\n)"
        
        return out
    def check_balanced(self):
        pass
    
    @classmethod
    def merge(cls, reacts1, reacts2):
        """
            Input: two dicts where values are Reaction objects
            Output: a dict that combines the two input dicts, and prints a warning for keys that show up in both
        """
        pass
    
    @classmethod
    def from_rhea_biopax2(cls, reactions, metabolites, references):
        out = {}
        
        #pprint.pprint(biopax_data)
        #
        for reaction in reactions:
            if len(reaction[u'rdf_name'].split('#')) > 1:
                name = reaction[u'rdf_name'].split('#')[1]
            else:
                name = reaction[u'rdf_name'].split('http://identifiers.org/rhea/')[1]
            formula = reaction[u'name']
            
            associations = {}
            annotations = {}
            
            left_side = {}
            right_side = {}
            direction = ''
            pair = '' # TODO: figure out how to make this work
            spontaneous = False
            notes = dict()
            transport = False
            balanced = True
            
            if (reaction[u'name'] in out):
                print "error multiple instances of reactions with name: " + reaction['name']
            
            notes['RheaID'] = name
            for comment in reaction[u'comment']:
                #RHEA comments look like: "RHEA:Class of reactions=false"
                #so take off the first 5 letters then split at the =
                if (comment.startswith('RHEA:') and '=' in comment):
                    (left,right) = comment[5:].split('=')
                    if (left == 'Direction'):
                        direction_dict = {'left to right':'left-to-right','right to left':'right-to-left','bidirectional':'reversible','undefined':'unknown'}
                        if (right in direction_dict):
                            direction = direction_dict[right]
                            
                            #if direction = "undefined", then it is its own master reaction, otherwise set master reaction below, where processing xrefs
                            if right == "undefined":
                                notes["master"] = notes['RheaID']
                        
                        else:
                            print 'Warning, direction ' + right + ' not recognized for ' + name
                    elif (left == 'Transport'):
                        if (right == 'true'):
                            transport = True
                    elif (left == 'Chemically balanced'):
                        if (right == 'false'):
                            print 'warning reaction not balanced ' + name
                            balanced = False
                    else:
                        notes[left] = right
                else:
                    #print "unrecognized comment: " + comment.encode('ascii', 'ignore')
                    if (not 'OTHER' in notes):
                        notes['OTHER'] = ""
                    notes['OTHER'] += "; " + comment
                
                
            for side in ('left','right'):
                for metabolite in reaction[side].keys():
                    metab_name = metabolite
                    metab_stoich = reaction[side][metabolite]
                    if side == 'left':
                        left_side[metab_name] = metab_stoich
                    elif side == 'right':
                        right_side[metab_name] = metab_stoich
            master = "none"
            if 'EC' in reaction:
                #print reaction[u'EC']
                annotations["EC"] = set()
                annotations["EC"] = annotations["EC"].union(set(reaction[u'EC']))
            
            for ref in reaction['xref']:
                db = references[ref]['db']
                id = references[ref]['id']
                if not (db in annotations):
                    annotations[db] = set()
                annotations[db].add(id)
                
                if ((db == "Rhea") and ('type' in references[ref]) and (references[ref]['type'] == "undefined")):
                    notes["master"] = references[ref]['id']
            if (not "master" in notes):
                    print 'Warning, no master reaction for ' + reaction[u'rdf_name']

            out[notes['RheaID']] = Reaction(name, associations, transport, annotations,left_side,right_side,direction, pair, spontaneous, notes, formula, balanced)
        
        #out = biopax_data[0]
        return out
    

    
    @classmethod
    def from_metacyc_biopax2(cls, filename):
        pass
    
    @classmethod
    def from_metacyc_biopax3(cls, filename):
        pass
