from cvxopt import matrix, spmatrix, solvers
import cvxpy as cv
import numpy as np
from scipy.sparse import lil_matrix
#import linearprogramming as lp

### NOTE: this module is for experimentation purposes only. I have found the cvxopt library too slow to be used for model simulations,
### although it is very possible that I'm just not using the library optimally. Also, don't trust these implementations...  SJ

class ConvexSolver(object):
    def __init__(self):
        pass
    
    def moma(self, model, target_flux, primary=None):
        '''
            The general QP problem is:
            
            minimize    (1/2)*x'*P*x + q'*x 
            subject to  G*x <= h
                        A*x = b
                        
            for MOMA:
                The objective is to minimize the sum of of the squares of the 
                distances between the solution vector and 'target_flux'.
                In other words, to minimizing the euclidean distance between the vectors.
                
                q is the negative of 'target_flux'
                P is the identity matrix
                A is the stoichiometric matrix
                b is zeros
                
                

        '''
        #S = 
        A = cv.spmatrix((model.len_metabolites(), model.len_reactions()))
        for (met, rxn, stoich) in model.sparse_matrix():
            A[met, rxn] = stoich
        A = matrix(A.todense())
        #spmatrix((model.len_metabolites(), model.len_reactions()))
        #A = spmatrix(zip(*model.sparse_matrix(style='SMR')),(model.len_metabolites(),model.len_reactions()))
        q = matrix(target_flux)
        P = matrix(np.identity(model.len_reactions()))
        b = matrix(np.zeros((model.len_metabolites(),1)))
        
        print b
        print A
        print q
        print P
        
        sol=solvers.qp(P, q, A=A, b=b)
        #sol=solvers.qp(P, q)
        print sol['x']
        
        return sol
        
        
    def moma2(self, model, target_flux):
        '''
            the MOMA problem is:
                The objective is to minimize the sum of of the squares of the 
                distances between the solution vector and 'target_flux'.
                In other words, to minimizing the euclidean distance between the vectors.
            
            The solution vector should conform to the same constraints as it would for a 
            flux balance analysis problem
            
            minimize    euclidean_norm(x-target_flux)
            subject to  x >= lb
                        x <= ub
                        A*x = 0
                
            A*x = b
        '''

        
        w = cv.matrix(target_flux).T #a column vector of target_flux
        x = cv.variable(model.len_reactions(),1) #a column vector for the solution
        
        for (i, val) in enumerate(target_flux):
            x[i,0].value = val
        
        #set the coefficient matrix
        A = cv.spmatrix((model.len_metabolites(), model.len_reactions()))
        for (met, rxn, stoich) in model.sparse_matrix():
            A[met, rxn] = stoich
        
        #set the bounds
        lb = cv.zeros(shape=(model.len_reactions(),1))
        ub = cv.zeros(shape=(model.len_reactions(),1))
        #inf = float('inf')
        #ninf = -inf
        for (i, rxn) in enumerate(model.reactions_list()):
            lb[i,0] = rxn.lb
            ub[i,0] = rxn.ub
            
        p =  cv.program(cv.minimize(x+w), [cv.geq(x,lb), cv.leq(x,ub), cv.eq(A*x,0)]) #x*(x-2*w)
        
        
        opt = p.solve()
        
        print opt
        print x.value
        
        return(opt, p)

            
        