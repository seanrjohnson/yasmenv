import numpy
import scipy
from scipy import linalg, matrix
from operator import itemgetter
#import unicodedata
import re

# def format_samples(sample_array,sep="\t"):
    # '''
      # takes the output from
    # '''
    # pass

def to_file(file_name, data, append = False):
    '''
        opens a file with the given name, and writes the data (presumed to be a string) to it.
        If append = True, appends the text to the file instead of overwriting it.
    '''
    
    filetype = "w"
    if append:
        filetype="a"
    
    with open(file_name, filetype) as outfile:
        outfile.write(data)
    
def format_fba(sols,sep='\t'):
    '''
        takes the output from solver.fba
        returns a string with 1 reaction per line
            reaction name and flux separated by sep
    '''
    out = ""
    for sol in sols['fluxes']:
        out += '%s%s%f\n' % (sol[0], sep, sol[1])
    return out

def format_fva(sols,sep='\t'):
    '''
        takes the output from solver.fva
        sep is the separator character
        returns a string with 1 reaction per line
            fields are "reaction name" "min flux" "max flux"
    '''
    out = ""
    #print sols['fluxes']
    for sol in sols['fluxes']:
        #out += str(sol[0]) + sep + "%.6f" % sol[1] + sep + "%.6f" % sol[2] +'\n'
        out += str(sol[0]) + sep + str(sol[1]) + sep + str(sol[2]) +'\n'
    return out

def format_frank(sol, sep='\t'):
    fields = ['expression', 'expression_rank', 'flux_rank', 'abs', 'fluxes']
    out = sep.join(['name']+fields) +'\n'
    sol_dict = dict()
    for field in fields:
        sol_dict[field] = {x[0]:str(x[1]) for x in sol[field]}
    for (r, rank) in sol['fluxes']:
        row = [r]
        for field in fields:
            if r in sol_dict[field]:
                row.append(sol_dict[field][r])
            else:
                row.append("")
        out += sep.join(row) + "\n"
    return out

def format_list_of_tuples(in_list, sep='\t'):
    '''
        generic method for printing lists of tuples
        
        does not escape any characters or add quotes, so
        precondition: the separator should be a character not found in the data
    '''
    out = ""
    for tup in in_list:
        l = [str(x) for x in tup]
        out += sep.join(l) + "\n"
    return out
    
def format_dict(in_dict, sep='\t'):
    '''
        generic method for printing lists of tuples
        
        does not escape any characters or add quotes, so
        precondition: the separator should be a character not found in the data
    '''
    
    return format_list_of_tuples(in_dict.items(), sep)

def sbmlify_id(string):
    '''
        remove all non-alphanumeric characters from a string in a reversible way
    '''
    out = ""
    for char in string:
        if char.isalnum():
            out += char
        else:
            #print char
            #print unicode(char)
            #print unicodedata.decimal(unicode(char))
            out += "_" + str(ord(char)) + "_"
    return out

def yasmenvify_id(string):
    '''
        the inverse of sbmlify_id
        basically just scan the string left to right and every time you hit
        _[0-9]+_
        then replace it with a character
        TODO: better test this...
    '''
    out = ""
    state = "none"
    buf = ""
    num_buf = ""
    for char in string:
        if char == "_":
            if state == "none" or state == "start":
                state = "start"
                out += buf
                buf = "_"
            elif state == "num":
                state == "none"
                buf = ""
                out += chr(int(num_buf))
        elif char.isdigit():
            if state == "start" or state == "num":
                num_buf += char
                buf += char
            else:
                state += buf
                buf = ""
                num_buf = ""
                state += char
        else:
            state += buf
            buf = ""
            num_buf = ""
            state += char
    return out

def molecular_formula_dict_to_string(dic, normalize_name=False):
    '''
        input: a dict where keys are chemical symbols, and values are integers of intramolecular stoichiometries
        
        output: a string of the molecular formula
        
        If normalize_name = True, then format all atom names to standard format: one or two letters where the first is capital and the second is lower case
    '''
    
    if dic is None:
        return ""
    
    terms = [[name, str(stoich)] for name, stoich in dic.items()]#convert dict to list of lists sorted alphabetically
    if normalize_name:
        for name in terms:
            name[0] = name[0].upper()
            if len(name[0]) > 2:
                name[0] = name[0][:2]
                name[0][1] = name[0][1].lower()
    terms = sorted(terms, key=itemgetter(0))
    
    out = "".join(["".join(term) for term in terms])
    
    return out
    
def molecular_formula_string_to_dict(string):
    pass #TODO

def underscore_compartment_to_bracket_compartment(name):
    '''
        given a name that ends with a scrumpy style compartment indicator (_x), convert it into a name
        with a yasmenv/COBRA style compartment indicator ([x])
    '''
    out = name
    match = re.match("^(.+)_([^_]+)$", name)
    if match:
        out = match.group(1) + "[" + match.group(2) + "]"
    return out

    
def null(A, eps=1e-15):
# from here: http://stackoverflow.com/questions/5889142/python-numpy-scipy-finding-the-null-space-of-a-matrix
    if A.shape[1] > A.shape[0]:
        #there need to be at least as many rows as columns.
        B = numpy.zeros(shape=(A.shape[1],A.shape[1]))
        B[0:A.shape[0],0:A.shape[1]] = A
        A = B
    u, s, vh = scipy.linalg.svd(A)
    null_mask = (s <= eps)
    null_space = scipy.compress(null_mask, vh, axis=0)
    #return null_space
    return scipy.transpose(null_space)

# def null_sparse(A, eps=1e-15):
# # from here: http://stackoverflow.com/questions/5889142/python-numpy-scipy-finding-the-null-space-of-a-matrix
    # if A.shape[1] > A.shape[0]:
        # B = numpy.zeros(shape=(A.shape[1],A.shape[1]))
        # B[0:A.shape[0],0:A.shape[1]] = A
        # A = B
    # u, s, vh = scipy.linalg.svd(A)
    # print s
    # null_mask = (s <= eps)
    # print null_mask
    # null_space = scipy.compress(null_mask, vh, axis=0)
    # #return null_space
    # return scipy.transpose(null_space)