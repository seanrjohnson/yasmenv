import re
from collections import OrderedDict
import copy
import constants
from pprint import pprint as pp

def parse_col(filename):
    '''
    read a .col file (as defined by BioCyc)
    into a 2D dict of lists, first key is the first column (UNIQUE-ID), second key is subsequent columns.
    some fields in the .col files are listed multiple times, so values for each column title are stored as a list, with indexes corresponding to the order in which the data appears in the data file


    Algorithm:
    Ignore lines starting with '#' (comment lines)
    Ignore empty lines
    Parse other lines by tabs
    Entries in the first non-comment line are taken to be headers, and used for keys in the second dimension of the hash
    (except the first column, because it is used as the first key)
    Subsequent lines are data lines
    
    #WARNING: NOT TESTED!
    '''

    out = {}
    with open(filename) as infile:
        state = u"begin"
        headings = []
        heading_count = {}
        line_num = 0
        
        for line in infile:
            line = line.rstrip('\n') #chomp
            line_num += 1
            if (line == "" or re.match('\s*#',line)):
                pass #do nothing because it's a comment line or a blank line
            elif (state == u'begin'):
                headings = line.split("\t")
                if (len(headings) < 1):
                    print "error reading " + filename + ", near line " + line_num +" no data in heading line\n"
                #count the number of instances of each unique heading
                for i in range(0,len(headings) - 1):
                    if (headings[i] in heading_count):
                        heading_count[heading] += 1
                    else:
                        heading_count[heading] = 1
                state = u'body'
            elif (state == u'body'):
                fields = line.split("\t")
                if ((len(fields) > 1) and (len(fields) <= len(headings))):
                    out[fields[0]] = {} #make it a 2D dict
                    for i in range(1,fields.len()-1):
                        if (headings[i] not in out[fields[0]]):
                            out[fields[0]][headings[i]] = [] # if the field doesn't exist yet, make it an empty list
                        out[fields[0]][headings[i]].append(fields[i])
                else:
                    print "error reading " + filename + ", near line " + line_num + " improperly formatted data line \n"
    return out



def parse_dat(filename):
    '''
        read a .dat (as used by BioCyc) file into a dict of dicts of lists of dicts, first key is the first field (UNIQUE-ID), second key is an attribute, the third key is arbitrary (dependent on the order the line appears in in the file), the fourth key is 'value', or some other descriptor.
        some fields in the .dat files are listed multiple times, so values for each row title are stored as a list, with indexes corresponding to the order in which the data appears in the data file.
        If a line is followed by a line beginning with a caret "^", the data in the caret line is assumed to be additional data about the most recent non-caret data line. Data in careted lines
        will be included in the dict for the most recent non-caret data line under a key corresponding to the field title.
        For example, the lines:
        UNIQUE-ID - RXN-13035
        LEFT - ALGINATE
        ^COMPARTMENT - CCO-OUT
        would result in a data record with the following structure:
        {'RXN-13035':{'UNIQUE-ID':[{'value': 'RXN-13035'}], 'LEFT':[{'value': 'ALGINATE', 'COMPARTMENT': 'CCO-OUT'}]}}
        
        Algorithm:
        Ignore lines starting with '#' (comment lines)
        Ignore empty lines
        '//' marks the end of a record, so reset the parser state to a new record when this line is encountered
        '/' indicates a continuation of the previous line (only used for comments as far as I know).  So when a line begins with '/', append it to the previous data line.
        parse other lines by looking for the pattern /^(\W+) - (.+)$/ (a string of non whitespace characters, followed by a space, a dash, another space, and an arbitrary string)
        The first line of a record should be 'UNIQUE-ID', if not, report an error.
        Subsequent lines are data lines
        Use UNIQUE-ID as the first key in the output object, and subsequent attribute names as the second key. 
    '''
    
    out = OrderedDict()
    with open(filename) as infile:
        state = 'new'
        unique_id = None
        attribute = None
        line_num = 0
        for line in infile:
            line = line.strip('\n') #chomp
            line_num += 1
            if (line == "" or re.match('\s*#', line)):
                pass #do nothing because it's a comment line or a blank line
            elif (state == 'new'):
                matches = re.match('^(\S+) - (.+)$', line)
                if (matches): 
                    if (matches.group(1) == 'UNIQUE-ID'):
                        unique_id = matches.group(2)
                        attribute = ''
                        out[unique_id] = OrderedDict()
                        out[unique_id]['UNIQUE-ID'] = [{'value': unique_id}]
                        state = 'body'
                    else:
                        print "warning, first line of record does not contain UNIQUE-ID, " + filename +" line " + line +"\n";
            elif (state == 'body'):
                matches = re.match('^(\S+) - (.*)$', line)# attribute line
                if (matches):
                    value = matches.group(2).strip().strip("|")
                    modifier_match = re.match('^\^(.+)',matches.group(1))
                    if (modifier_match):  #It's a modifier
                        out[unique_id][attribute][len(out[unique_id][attribute]) - 1][modifier_match.group(1)] = value
                        #assign the modifier to the most recently read attribute
                    else:
                        attribute = matches.group(1).strip()
                        if (attribute not in out[unique_id]):
                            out[unique_id][attribute] = []
                        out[unique_id][attribute].append({'value': value})
                        #out[unique_id][attribute].append(value)
                elif (re.match('^\/\/$', line)): #end of record symbol
                    state = 'new'
                    unique_id = None
                    attribute = None
                elif (re.match('^\/([^\/]*|([^\/].*))', line)): #comment continuation, lines starting with '/' not immediately followed by another '/'
                    out[unique_id][attribute][len(out[unique_id][attribute]) - 1]['value'] += "\n" + re.match('^\/([^\/]*|([^\/].*))', line).group(1)
                    #out[unique_id][attribute][len(out[unique_id][attribute]) - 1] += "\n" + re.match('^\/([^\/]*|([^\/].*))', line).group(1)
                else:
                    print "warning, line of record does not correctly parse, " + filename + " line " + line + "\n";
    return out

class Record(object):
    def __init__(self, data):
        '''
            data is a dict of lists of dicts, where the first key is the field name, the second key is arbitrary, the third key is the data description.
        '''
        self._data = data
    
    def __getitem__(self, field):
        '''
            returns a list of values associated with the input field.
            Does not include extra data (such as coefficients and compartments)
        '''
        out = list()
        if field in self._data:
            out = [x['value'] for x in self._data[field]]
        return out
    
    def __contains__(self, key):
        return key in self._data
    
    def values_and_modifiers(self, field):
        '''
            returns a list of data associated with the field, including all extra data.
            (such as coefficients and compartments)
        '''
        if field in self:
            return self._data[field]
        else:
            return list()
    
    def formatted_first(self, field):
        unformatted = self[field][0]
        unformatted = unformatted.trim()
    
    def first(self, field):
        if field in self:
            return self[field][0]
        else:
            return None
    
    def name(self):
        return self.first('UNIQUE-ID')
    
    def list(self, field):
        out = list()
        if field in self:
            out = self[field]
        return out
        
    def external_links(self, subset=None):
        '''
            returns a list of dicts of external links for the record
                keys are 'source', and 'accession'
            
            subset: an optional set of source names. If source for a record is not in that set, it will not be included in the output
        '''
        
        out = list()
        for link in self.list("DBLINKS"):
            fields = link.strip().strip('()').split()
            out_dict = {'source': fields[0], 'accession': fields[1].strip('"')}
            if (subset is None) or (out_dict['source'] in subset):
                out.append(out_dict)
        return out
    
    
class Dat(object):
    def __init__(self, input, type='file', rec_class=Record):
        data = None
        initialized = False
        if type == 'file':
            data = parse_dat(input)
            initialized = True
        elif type=='parsed':
            data = input
            initialized = True
        
        if initialized:
            self._records = OrderedDict()
            for (k, v) in data.iteritems():
                self._records[k] = rec_class(v)
        else:
            raise("Cannot initialize Dat object")

    def __getitem__(self, item):
        return self._records[item]
    
    def __contains__(self, key):
        return key in self._records
    
    def __iter__(self):
        return self._records.iterkeys()
    
    def all_records(self):
        return self._records
    
    # def find(self, field, value):
        # '''
            # retur
        # '''
        # out = OrderedDict()
        # for rec in self._records.itervalues():
class Enzrxn(Record):
    '''
        class for data from enzrxns.dat
        nothing special here so far, uses all default Record methods
    '''
    
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)


class Reaction(Record):
    '''
        class for data from reactions.dat
        
        TODO: one major missing feature here is the inability to deal with metabolite compartmentalization
    '''
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)
    
    def left(self):
        return self._side(self.values_and_modifiers('LEFT'))
        
    def right(self):
        return self._side(self.values_and_modifiers('RIGHT'))
    
    def compartments(self):
        '''
            returns a set of the compartments associated with the reaction
        '''
        compartments = set()
        #pp(self.values_and_modifiers('LEFT'))
        for met in self.values_and_modifiers('LEFT') + self.values_and_modifiers('RIGHT'):
            if 'COMPARTMENT' in met:
                compartments.add(met['COMPARTMENT'])
        return compartments
    
    def parent_reactions(self, reactions):
        '''
            return a set of the names of all reactions listed under the pathways field for this reaction
        '''
        r_pways = self['IN-PATHWAY']
        out = set()
        for pway in r_pways:
            if pway in reactions:
                out.add(pway)
        return out
    
    def ancestor_reactions(self, reactions):
        '''
            Recursively find all of the reactions listed in the pathways field of this reaction or a parent, grandparent, etc, reactions.
        '''
        seen = set()
        q = [self.parent_reactions(reactions)]
        while len(q) > 0:
            next = q.pop().difference(seen)
            seen = seen.union(next)
            for item in next:
                if item in reactions:
                    q.append(reactions[item].parent_reactions(reactions))
        return seen
        
    def formula(self):
        return {'left': self.left(), 'right': self.right()}
    
    def bounds(self):
        
        direction_dict={'REVERSIBLE': 'reversible', 
                    'PHYSIOL-LEFT-TO-RIGHT': 'forward',
                    'PHYSIOL-RIGHT-TO-LEFT': 'backward',
                    'IRREVERSIBLE-LEFT-TO-RIGHT': 'forward',
                    'IRREVERSIBLE-RIGHT-TO-LEFT': 'backward', 
                    'LEFT-TO-RIGHT': 'forward',
                    'RIGHT-TO-LEFT': 'backward',
                    'default': 'reversible'}
        bounds_dict={'reversible': ('(-)inf', '(+)inf'), #one wonders if these should be strings or floats
                    'forward': ('0', '(+)inf'),
                    'backward': ('(-)inf', '0')}
        
        direction = direction_dict['default']
        if 'REACTION-DIRECTION' in self:
            direction = direction_dict[self.first('REACTION-DIRECTION')]
        
        return bounds_dict[direction]
    
    def spontaneous(self):
        out = False
        if 'SPONTANEOUS?' in self and self.first('SPONTANEOUS?') == 'T':
            out = True
        return out
    
    def subsystems(self):
        return self.list('IN-PATHWAY')
    
    def description(self):
        out = ""
        if 'COMMON-NAME' in self:
            out =  self.first('COMMON-NAME')
        return out
    
    def uniprot(self, enzrxns, proteins):
        '''
            returns a set of uniprot ids associated with this reaction
        '''
        
        links = self.external_links(subset=set(['UNIPROT']))
        out = {x['accession'] for x in links} #set comprehension
        
        enzrxns_list = self.list('ENZYMATIC-REACTION')
        proteins_set = set()
        for enzrxn in enzrxns_list:
            if enzrxn in enzrxns:
                for protein in enzrxns[enzrxn].list('ENZYME'):
                    proteins_set.add(protein)
        
        for protein in proteins_set:
            out = out.union(proteins[protein].uniprot(proteins))
        
        return out
    
    def genes(self, enzrxns, proteins, genes):
        '''
            returns a set of genes associated with this reaction
        '''
        
        enzrxns_list = self.list('ENZYMATIC-REACTION')
        proteins_set = set()
        out = set()
        for enzrxn in enzrxns_list:
            if enzrxn in enzrxns:
                for protein in enzrxns[enzrxn].list('ENZYME'):
                    proteins_set.add(protein)
        
        for protein in proteins_set:
            out = out.union(proteins[protein].genes(proteins, genes))
        
        return out
    
    def unidentifiable_reactants(self, compounds, classes):
        '''
            return set of reactants that are neither compounds nor classes
        '''
        
        reactants = set(self.left().keys()).union(set(self.right().keys()))
        unknown = set()
        for reactant in reactants:
            if reactant not in compounds and reactant not in classes:
                unknown.add(reactant)
        return unknown
    
    def ecs(self):
        '''
            returns a list of ecs associated with the reaction
        '''
        
        #ec_re = re.compile('(([0-9]+|-)\.([0-9]+|-)\.([0-9]+|-)\.([0-9]+|-))')
        ec_re = re.compile('(([0-9]+)(\.([0-9]+|-))?(\.([0-9]+|-))?(\.([0-9]+|-|[a-z]+))?)')
        
        out = list()
        ecs = self.list('EC-NUMBER')
        for ec_str in ecs:
            m = ec_re.search(ec_str)
            if m:
                out.append(m.group(1))
            else:
                print "could not recognize EC " + ec_str
            
        return out
        
    
    def _side(self, metabs):
        '''
            metabs is a list of dicts with key 'value', and optional key 'coefficient'
            value is the name of a compound or class
            if coefficient is present that is the stoichiometry of the compound in the reaction
            else the stoichiometry is 1
        
            returns a dict where keys are compound/class names, and values are coefficients
        '''
        out = dict()
        for metab in metabs:
            coeff = metab.get('COEFFICIENT', 1)
            if coeff == 'n':
                coeff = 1
            elif coeff == "n-1":
                coeff = 2 #TODO: find a more graceful way to deal with this (luckily these are only in one reaction so it doesn't really matter at this point)
            elif coeff == "n-2":
                coeff = 1 #TODO: find a more graceful way to deal with this (luckily these are only in one reaction so it doesn't really matter at this point)
            elif coeff == "2n":
                coeff = 2 #TODO: find a more graceful way to deal with this
            elif coeff == "4n":
                coeff = 4 #TODO: find a more graceful way to deal with this
            elif coeff == "(n+1)":
                coeff = 2 #TODO: find a more graceful way to deal with this
            elif coeff == "n+1":
                coeff = 2 #TODO: find a more graceful way to deal with this
            elif coeff == "m+q":
                coeff = 2 #TODO: find a more graceful way to deal with this
            elif coeff == "N":
                coeff = 1 #TODO: find a more graceful way to deal with this
            elif coeff == "<i><b>n</i></b>":
                coeff = 1 #TODO: find a more graceful way to deal with this
            elif coeff == "m":
                coeff = 1 #TODO: find a more graceful way to deal with this
            coeff = float(coeff)
            out[metab['value']] = out.get(metab['value'], 0) + coeff
        
        return out
    
    def instances(self, compounds, classes):
        '''
            returns three lists of instantiations of the reaction:
                each instantiation is a dict with the following keys and values:
                reaction: a dict with keys 'left' and 'right', which are dicts with compound names as keys and stoichiometry as values
                compounds: a set of the names of the compounds that were added to the generic reaction to create the instance
                tag: a string indicating some information about this reaction
                    'instantiation' (not necessarily a balanced instantiation) #TODO: distinguish between balanced and not balanced instantiations
                    'non-generic'
                    'generic_no_balanced_instantiations' 
                the three lists are "balanced_include", "unbalanced", "balanced_not_include"
                balanced_include: instantiations that are balanced chemically, and where no two balanced instantiations contain the same metabolite. 
                
                unbalanced: 
                
                balanced_not_include: instantiations that are balanced chemically, but where one or more metabolite occurs in multiple balanced instantiations. In this case, it is likely that isomer pairs will get confused, so for example, a reaction like "a monosaccharide + ATP => a monosaccharide phosphate + ADP" could have a balanced instantiation "glucose... => glucose-6-phosphate" as well as an instantiation "glucose... => fructose-6-phosphate". Obviously one of those is a more plausible instantiation than the other.
            
            
            TODO: NOTE, this will fail if there are two generic compounds with the same name on the same side of an equation.
                YASMEnv currently adds the stoichiometries of identically named compounds into a single term.  This could
                eliminate some instantiations that should be considered.
                For example, the equation:
                    a monosaccharide + a monosaccharide <=> a disaccharide
                would be reduced to:
                    2 a monosaccharide <=> a disaccharide
                which would eliminate the possibility of instantiating to a heterodisaccharide.
                I don't know of any reactions where this is actually a problem, but there may be some.
                It could be dealt with by refactoring YASMEnv, but probably more easily, just by dealing with
                it as a special case within this function.
                
            7/23/13 removed the requirement for charge balance.  I think there are plenty of valid reactions that don't charge balance
        '''

        to_sort = list()
        
        balanced_include = list()
        unbalanced = list()
        balanced_not_include = list()
        
        
        left = self.left()
        right = self.right()
        left_instances = {x:set() for x in left if x not in compounds} #if it's not a compound, then presumably it's a class (although 
        right_instances = {x:set() for x in right if x not in compounds}
        

        total_possible_instantiations = 1
        for class_ in left_instances.iterkeys():
            left_instances[class_] = left_instances[class_].union(compounds_with_class(class_, compounds, classes))
            total_possible_instantiations *= len(left_instances[class_])

        for class_ in right_instances.iterkeys():
            right_instances[class_] = right_instances[class_].union(compounds_with_class(class_, compounds, classes))
            total_possible_instantiations *= len(right_instances[class_])

        #print self.name() + "\t" + str(total_possible_instantiations)

        tag = ""
        if (len(left_instances) + len(right_instances) == 0):
            #all of the reactants are compounds
            #if mass_balanced({'left': left, 'right': right}, compounds) and charge_balanced({'left': left, 'right': right}, compounds):
            if mass_balanced({'left': left, 'right': right}, compounds):
                to_sort.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'non-generic_balanced', 'compounds': set()})
            else:
                to_sort.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'non-generic-not_balanced', 'compounds': set()})
        else:
            #there are some generics

            #### subset the combinations to check for mass balance by using _enumerate_sums to find the combinations that have "atomic number balance" ####
            l_static = sum([compounds[x].atomic_number_sum(default=0) * left[x] for x in left if x in compounds]) #sum of atomic numbers of non-generic reactants
            r_static = sum([compounds[x].atomic_number_sum(default=0) * right[x] for x in right if x in compounds])
            
            left_sums = list() # a list of dicts, first key is the position(arbitrary) of the generic metabolite, the second key is the atomic_number_sum * stoichiometry, the value is a set of corresponding metabolites
            left_class_to_position = dict()
            for (class_name, met_set) in left_instances.iteritems():
                left_sums.append(dict())
                left_class_to_position[class_name] = len(left_sums) -1
                for met in met_set:
                    sum_ = compounds[met].atomic_number_sum(default=0) * left[class_name]
                    left_sums[-1][sum_] = left_sums[-1].get(sum_, set()).union(set([met]))
            
            right_sums = list() # a list of dicts, first key is the position(arbitrary) of the generic metabolite, the second key is the atomic_number_sum * stoichiometry, the value is a set of corresponding metabolites
            right_class_to_position = dict()
            for (class_name, met_set) in right_instances.iteritems():
                right_sums.append(dict())
                right_class_to_position[class_name] = len(right_sums) -1
                for met in met_set:
                    sum_ = compounds[met].atomic_number_sum(default=0) * right[class_name]
                    right_sums[-1][sum_] = right_sums[-1].get(sum_, set()).union(set([met]))
            
            l_lists = [sorted(x.keys()) for x in left_sums]
            r_lists = [sorted(x.keys()) for x in right_sums]
            
            (solutions, tag) = _enumerate_sums(l_lists, r_lists, l_static, r_static)
            
            #print "atomic_number_balanced" + "\t" + str(len(solutions))
            
            #### Now make a list of all possible atomic_number balanced instances####
            for (solution) in solutions:
                left_queue = left_instances.keys()
                right_queue = right_instances.keys()
            
                stack = [{'reaction': {'left': copy.deepcopy(left), 'right': copy.deepcopy(right)}, 'tag':"instantiation", 'compounds':set()}] 
                
                while len(left_queue) > 0:
                    new_stack = list()
                    to_replace = left_queue.pop()
                    while len(stack) > 0:
                        prev = stack.pop()
                        position = left_class_to_position[to_replace]
                        for met in left_sums[position][solution[position]]:
                            next = copy.deepcopy(prev)
                            next['compounds'].add(met)
                            stoich = next['reaction']['left'][to_replace]
                            del next['reaction']['left'][to_replace]
                            next['reaction']['left'][met] = stoich
                            new_stack.append(next)
                    stack = new_stack
                while len(right_queue) > 0:
                    new_stack = list()
                    to_replace = right_queue.pop()
                    while len(stack) > 0:
                        prev = stack.pop()
                        position = right_class_to_position[to_replace] 
                        for met in right_sums[position][solution[position + len(l_lists)]]:
                            next = copy.deepcopy(prev)
                            next['compounds'].add(met)
                            stoich = next['reaction']['right'][to_replace]
                            del next['reaction']['right'][to_replace]
                            next['reaction']['right'][met] = stoich
                            new_stack.append(next)
                    stack = new_stack
                for instance in stack:
                    #if mass_balanced(instance['reaction'], compounds) and charge_balanced(instance['reaction'], compounds):
                    if mass_balanced(instance['reaction'], compounds):
                        to_sort.append(instance)
            if len(to_sort) == 0:
                to_sort.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'generic_no_balanced_instantiations', 'compounds': set()})
        
        #### sort the instances into the three bins ###
        cpds_in_instances = dict() # a dict to count how many balanced subreactions an instantiated metabolite occurs in
        for instance in to_sort:
            if instance['tag'] == "instantiation":
                #if mass_balanced(instance['reaction'], compounds) and charge_balanced(instance['reaction'], compounds):
                if mass_balanced(instance['reaction'], compounds):
                    for cpd in instance['compounds']:
                        cpds_in_instances[cpd] = cpds_in_instances.get(cpd, 0) + 1
        
        
        for instance in to_sort:
            if instance['tag'] == 'generic_no_balanced_instantiations':
                unbalanced.append(instance)
            elif instance['tag'] == 'non-generic_balanced':
                balanced_include.append(instance)
            elif instance['tag'] == 'instantiation':
                multiples = False
                for cpd in instance['compounds']:
                    if cpds_in_instances[cpd] > 1:
                        multiples = True
                        break
                if multiples:
                    balanced_not_include.append(instance)
                else:
                    balanced_include.append(instance)
            else: #presumably the only things left are instances with tag == 'non-generic-not_balanced'
                unbalanced.append(instance)
        return balanced_include, unbalanced, balanced_not_include
    
    
    # def instances(self, compounds, classes):
        # '''
            # returns a list of instantiations of the reaction:
                # each instantiation is a dict with the following keys and values:
                # reaction: a dict with keys 'left' and 'right', which are dicts with compound names as keys and stoichiometry as values
                # compounds: a set of the names of the compounds that were added to the generic reaction to create the instance
                # tag: a string indicating some information about this reaction
                    # 'generic_too_many_instantiations'
                    # 'instantiation'
                    # 'non-generic'
                    # 'generic_no_balanced_instantiations'
            # TODO: NOTE, this will fail if there are two generic compounds with the same name on the same side of an equation.
                # YASMEnv currently adds the stoichiometries of identically named compounds into a single term.  This could
                # eliminate some instantiations that should be considered.
                # For example, the equation:
                    # a monosaccharide + a monosaccharide <=> a disaccharide
                # would be reduced to:
                    # 2 a monosaccharide <=> a disaccharide
                # which would eliminate the possibility of instantiating to a heterodisaccharide.
                # I don't know of any reactions where this is actually a problem, but there may be some.
                # It could be dealt with by refactoring YASMEnv, but probably more easily, just by dealing with
                # it as a special case within this function.
                
            ##### THIS IS THE ORIGINGAL, NOT VERY EFFICIENT VERSION #####
        # '''
        
        # out = list()
        
        # left = self.left()
        # right = self.right()
        # left_instances = {x:set() for x in left if x not in compounds} #if it's not a compound, then presumably it's a class
        # right_instances = {x:set() for x in right if x not in compounds}
        
        # total_possible_instantiations = 1
        # for class_ in left_instances.iterkeys():
            # left_instances[class_] = left_instances[class_].union(compounds_with_class(class_, compounds, classes))
            # total_possible_instantiations *= len(left_instances[class_])
            # #pp(mets)
        # for class_ in right_instances.iterkeys():
            # right_instances[class_] = right_instances[class_].union(compounds_with_class(class_, compounds, classes))
            # total_possible_instantiations *= len(right_instances[class_])
            # #pp(mets)
        
        # #pp(left_instances)
        # #pp(right_instances)
        # tag = ""
        # if (len(left_instances) + len(right_instances) == 0):
            # #all of the reactants are compounds
            # out.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'non-generic', 'compounds': set()})
        # elif total_possible_instantiations > MAXIMUM_INSTANCES_TO_CHECK:
            # out.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'generic_too_many_instantiations', 'compounds': set()})
        # else:
            # left_queue = left_instances.keys()
            # right_queue = right_instances.keys()
            # stack = [{'reaction': {'left': copy.deepcopy(left), 'right': copy.deepcopy(right)}, 'tag':"instantiation", 'compounds':set()}] 
            
            # while len(left_queue) > 0:
                # new_stack = list()
                # to_replace = left_queue.pop()
                # while len(stack) > 0:
                    # prev = stack.pop()
                    # for met in left_instances[to_replace]:
                        # next = copy.deepcopy(prev)
                        # next['compounds'].add(met)
                        # stoich = next['reaction']['left'][to_replace]
                        # del next['reaction']['left'][to_replace]
                        # next['reaction']['left'][met] = stoich
                        # new_stack.append(next)
                # stack = new_stack
            # while len(right_queue) > 0:
                # new_stack = list()
                # to_replace = right_queue.pop()
                # while len(stack) > 0:
                    # prev = stack.pop()
                    # for met in right_instances[to_replace]:
                        # next = copy.deepcopy(prev)
                        # next['compounds'].add(met)
                        # stoich = next['reaction']['right'][to_replace]
                        # del next['reaction']['right'][to_replace]
                        # next['reaction']['right'][met] = stoich
                        # new_stack.append(next)
                # stack = new_stack
            # out = stack
            # if len(out) == 0:
                # out.append({ 'reaction':{'left': left, 'right': right}, 'tag': 'generic_no_balanced_instantiations', 'compounds': set()})
        # return out
        
    def mass_balanced(self, compounds):
        return mass_balanced(self.formula(), compounds)
    
    def charge_balanced(self, compounds):
        return charge_balanced(self.formula(), compounds)
    
    # def processed_instances(self, compounds, classes):
        # '''
            # returns three lists of reactions derived from this reaction
                # balanced_include: balanced reactions where any concrete instances of generic compounds occur in only 
                # balanced_not_include: balanced reactions that are ambiguous.  If there are two or more balanced instances with the same instantiation for at least one of
                    # the generic compounds, all of those instances go here
                # unbalanced: The reaction is unbalanced or one of its compounds is missing formula annotation.  Also includes generic reactions that have no
                    # possible instantiations
        # '''
        
        # #instances = self.instances(compounds, classes)
        # instances = self.instances(compounds, classes)
        # balanced_include = list()
        # unbalanced = list()
        # balanced_not_include = list()
        
        # cpds_in_instances = dict() # a dict to count how many balanced subreactions an instantiated metabolite occurs in
        # for instance in instances:
            # if instance['tag'] == "instantiation":
                # if mass_balanced(instance['reaction'], compounds) and charge_balanced(instance['reaction'], compounds):
                    # for cpd in instance['compounds']:
                        # cpds_in_instances[cpd] = cpds_in_instances.get(cpd, 0) + 1
        
        
        # for instance in instances:
            # if instance['tag'] == 'generic_too_many_instantiations' or instance['tag'] == 'generic_no_balanced_instantiations':
                # unbalanced.append(instance)
            # elif mass_balanced(instance['reaction'], compounds) and charge_balanced(instance['reaction'], compounds):
                # multiples = False
                # for cpd in instance['compounds']:
                    # if cpds_in_instances[cpd] > 1:
                        # multiples = True
                        # break
                # if multiples:
                    # balanced_not_include.append(instance)
                # else:
                    # balanced_include.append(instance)
            # elif instance['tag'] == 'non-generic':
                # unbalanced.append(instance)
        # return balanced_include, unbalanced, balanced_not_include

def compounds_with_class(class_, compounds, classes):
    out = set()
    for (met_name, met) in compounds.all_records().iteritems():
        if class_ in met.types(classes):
            out.add(met_name)
    return out

def reaction_as_str(rxn_dict, equals_sign = "<=>"):
    '''
        returns a string representation of the reaction
    '''
    left = list()
    right = list()
    for (met, stoich) in rxn_dict['left'].iteritems():
        left.append(str(stoich) + " " + met)
    
    for (met, stoich) in rxn_dict['right'].iteritems():
        right.append(str(stoich) + " " + met)
    
    left = sorted(left) #make it deterministic
    right = sorted(right)
    
    eq = " " + equals_sign + " "
    return eq.join((" + ".join(left), " + ".join(right))).strip()

def mass_balanced(rxn_dict, compounds):
    '''
        rxn_dict should be a dict of dicts where rxn_dict[side][compound] = stoichiometry
        TODO: add ability to handle exceptions, such as sets of compounds that are known to mass balance even though they lack a specified formula. 
          For example, ferredoxins, or protein reactants.
    '''
    #out = True
    totals = dict() #dict of dicts
    last = None
    for (side, metabs) in rxn_dict.iteritems():
        totals[side] = dict()
        for (met, met_stoich) in metabs.iteritems():
            if (met in compounds) and (compounds[met].formula() is not None):
                for (elem, elem_stoich) in compounds[met].formula().iteritems():
                    totals[side][elem] = totals[side].get(elem, 0) + met_stoich*elem_stoich
            else:
                # it's not a compound, or it has no formula listed
                return False
        if last is None:
            last = totals[side]
        else:
            if last != totals[side]:
                #if its different from the previous one, its not balanced, quit now
                return False
    
    return True

def charge_balanced(rxn_dict, compounds):
    '''
        rxn_dict should be a dict of dicts where rxn_dict[side][compound] = stoichiometry
    '''
    #out = True
    totals = dict() #dict of floats
    last = None
    for (side, metabs) in rxn_dict.iteritems():
        totals[side] = 0
        for (met, met_stoich) in metabs.iteritems():
            if (met in compounds):
                totals[side] += met_stoich*compounds[met].charge()
        if last is None:
            last = totals[side]
        else:
            if last != totals[side]:
                #if its different from the previous one, its not balanced, quit now
                return False
    #pp(totals)
    return True


class Compound(Record):
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)
    
    def types(self, classes):
        if not hasattr(self, '_types_set'):
            # cache it so you don't have to waste time traversing the class tree every time
            self._types_set = set() # memoize!
            if 'TYPES' in self:
                for type in self['TYPES']:
                    if type in classes:
                        self._types_set = self._types_set.union(classes[type].ancestors(classes))
        return self._types_set
    
    #def has_formula(self):
    #    return 'CHEMICAL-FORMULA' in self

    #def types(self):
    #    return self.list('TYPES')
    
    #def synonyms(self):
    #    return self.list('SYNONYMS')
        
    def synonyms(self):
        return self.list('SYNONYMS')
        
    def common_name(self):
        out = ""
        if 'COMMON-NAME' in self:
            out =  self.first('COMMON-NAME')
        return out
        
    def smiles(self):
        out = ""
        if 'SMILES' in self:
            out =  self.first('SMILES')
        return out
    
    def inchi(self):
        out = ""
        if 'INCHI' in self:
            out =  self.first('INCHI')
        return out
        
    def molecular_weight(self):
        out = ""
        if 'MOLECULAR-WEIGHT' in self:
            out =  self.first('MOLECULAR-WEIGHT')
        return out
        
    def monoisotopic_weight(self):
        out = ""
        if 'MONOISOTOPIC-MW' in self:
            out =  self.first('MONOISOTOPIC-MW')
        return out
    
    def chebi(self):
        '''
          returns the first CHEBI ID associated with the compound
        '''
        links = self.external_links(subset=set(['CHEBI']))
        out = ""
        if len(links) > 0:
            out = links[0]['accession']

        return out
    
    def pubchem(self):
        '''
          returns the first PubChem ID associated with the compound
        '''
        links = self.external_links(subset=set(['PUBCHEM']))
        out = ""
        if len(links) > 0:
            out = links[0]['accession']

        return out
        
    def formula(self):
        '''
            returns a dict of the compound's formula
            or None if it has no formula
        '''
        if 'CHEMICAL-FORMULA' not in self:
            return None
        else:
            out = dict()
            forms = self.list('CHEMICAL-FORMULA')
            for form in forms:
                form = form.strip().strip('()')
                (elem, stoich) = form.split()
                out[elem] = out.get(elem, 0) + int(stoich)
        return out
    
    def atomic_number_sum(self, default=None):
        '''
            returns an integer of the total number of protons in the compound
            or default if it has no formula
        '''
        if 'CHEMICAL-FORMULA' not in self:
            return default
        else:
            out = 0
            forms = self.list('CHEMICAL-FORMULA')
            for form in forms:
                form = form.strip().strip('()')
                (elem, stoich) = form.split()
                out += int(stoich) * constants.atomic_number[elem]
        return out
    
    
    def charge(self):
        '''
            returns the net charge on the molecule
            if no charged atoms listed, returns 0
        '''
        total_charge = 0
        if 'ATOM-CHARGES' in self:
            forms = self.list('ATOM-CHARGES')
            for form in forms:
                form = form.strip().strip('()')
                (pos, charge) = form.split()
                total_charge += int(charge)
        return total_charge

class Class(Record):
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)

    def ancestors(self, classes, include_self=True):
        '''
            return a set of the names of the super-classes
        '''
        seen = set()
        q = [self.parents()]
        while len(q) > 0:
            next = q.pop().difference(seen)
            seen = seen.union(next)
            for item in next:
                if item in classes:
                    q.append(classes[item].parents())
        if include_self:
            seen.add(self.name())
        return seen

    def parents(self):
        return set(self['TYPES'])
        
class Protein(Record):
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)

    def ancestors(self, proteins, include_self=True):
        '''
            return a set of the names of the super-proteins
        '''
        seen = set()
        q = [self.parents()]
        while len(q) > 0:
            next = q.pop().difference(seen)
            seen = seen.union(next)
            for item in next:
                if item in proteins:
                    q.append(proteins[item].parents())
        if include_self:
            seen.add(self.name())
        return seen

    def parents(self):
        return set(self['COMPONENTS'])
    
    def uniprot(self, proteins):
        all_ancestors = self.ancestors(proteins)
        
        out = set()
        for ancestor in all_ancestors:
            if ancestor in proteins:
                out = out.union({x['accession'] for x in proteins[ancestor].external_links(subset=set(['UNIPROT']))})
            else:
                pass
                #print ancestor
                #in metacyc 17.1 this prints: RNPB-RNA
                # twice when going through all reactions
        
        return out
        
    def genes(self, proteins, genes):
        '''
            return a set of genes encoding components of this protein
        '''
        components = self.ancestors(proteins)
        component_genes = set()
        out = set()
        
        for component in components:
            component_genes |= set(proteins[component]['GENE'])
        
        #print("\n".join(genes._records.keys()))
        for gene in component_genes:
            out.add(genes[gene].gene())
        return out
    

class Gene(Record):
    def __init__(self, *args, **kwargs):
        Record.__init__(self, *args, **kwargs)
    
    def transcript(self):
        if 'ACCESSION-1' in self:
            return self.first('ACCESSION-1')
        else:
            return self.name()
    
    def gene(self):
        return self.transcript().split('.')[0]
        
def _enumerate_sums(l_lists, r_lists, l_sum=0, r_sum=0, solution_list=list()):
    '''
        runs a binary search through all combinations where one element is chosen from each of the input lists:
        finds all combinations such that (l_sum + sum(choices_from_l_lists)) = (r_sum + sum(choices_from_r_lists))
        
        l_lists: list of list of numbers. Each sublist represents the possible numbers for a given position
        on the left side of the equation.
        
        r_lists: list of lists of numbers. For the right side of the equation
        
        l_sum and r_sum: constants to be added to the left and right side (respectively) before comparison
        
        solution_list: a list of values that have been picked from members of l_lists and r_lists
            in the path leading up to this calling of enumerate_sums
        
        precondition: lists must be sorted! It also helps to remove duplicates, but that isn't absolutely essential
        (note that the algorithm can handle subtraction as well, just apply the minuses before sorting)
        
        Description:
            It's kind of a depth-first implicit enumeration algorithm and also resembles a binary search.
            
        Algorithm:
            starting with the first list in l_lists,
            pick the median and determine if there are any possible solutions
            starting with that number, or if all possible combinations starting
            with that number have the left_side larger than any possible right_side
            or vice versa.  If there are possible solutions, store those solutions,
            and ask the same question ("branch") about the numbers half-way higher and half-way
            lower in the list.  If it is a lower bound, only look higher, if it is an upper bound,
            only look lower.
            
            To know if a choice is an upper or lower bound, or not a bound, add the value of the choice
            to l_sum (or r_sum, if the current choice is from a right-hand list), and call enumerate_sums, 
            but with the list that the choice is from removed.  This chain will continue until there 
            are no more lists left in l_lists or r_lists (a depth-first traversal). Once there are no 
            possible choices, compare l_sum to r_sum and return a string indicating whether they are
            the same (and thus the path is a solution), or how they are different.
            
            
        Example:
            l = range(1,1000)
            l2 = range(1,100)
            (solution, tag) = enumerate_sums([l]*2, [l2]*1, 0, 0)
    '''
    queue = list()
    sols = list()
    
    top_list = None
    side = None
    if len(l_lists) > 0: 
        l_lists = copy.copy(l_lists)
        top_list = l_lists.pop(0) #return the first item from the list
        side = "l"
    elif len(r_lists) > 0:
        r_lists = copy.copy(r_lists)
        top_list = r_lists.pop(0) # return the first item from the list
        side = "r"
    else: #no more lists left, decide whether we are too high, too low, or just right
        if l_sum > r_sum:
            return (list(), "high") #l_sum is too high
        elif l_sum < r_sum:
            return (list(), "low") #r_sum is too low
        else:
            return ([solution_list], "no_bound")
    
    if len(top_list) == 0:
        return (list(), "error")
    
    queue.append(( 0, len(top_list)-1))
    low_present = False
    high_present = False
    no_bound_present = False
    while len(queue) > 0:
        (low, high) = queue.pop()
        mid = int((high+low)/2)
        value = top_list[mid]
        
        (new_sols, msg) = (None, None)
        if side == "l":
            new_solution_list = copy.copy(solution_list)
            new_solution_list.append(value)
            (new_sols, msg) = _enumerate_sums(l_lists, r_lists, l_sum+value, r_sum, new_solution_list)
        elif side == "r":
            new_solution_list = copy.copy(solution_list)
            new_solution_list.append(value)
            (new_sols, msg) = _enumerate_sums(l_lists, r_lists, l_sum, r_sum+value, new_solution_list)
        if msg == "error":
            return (set(), "error")
        elif msg == "high":
            high_present = True
            if side == 'l':
                if mid != low:
                    queue.append((low,mid-1))
            if side == 'r':
                if mid != high:
                    queue.append((mid+1,high))
            
        elif msg == "low":
            low_present = True
            if side == 'r':
                if mid != low:
                    queue.append((low,mid-1))
            if side == 'l':
                if mid != high:
                    queue.append((mid+1,high))

        elif msg == "no_bound":
            no_bound_present = True
            sols = sols + new_sols
            if mid != high:
                queue.append((mid+1,high))
            if mid != low:
                queue.append((low,mid-1))
    out_tag = "no_bound"
    if not no_bound_present:
        if (low_present and not high_present):
            out_tag = 'low'
        elif(high_present and not low_present):
            out_tag = 'high'
    return (sols, out_tag)