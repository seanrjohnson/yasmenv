import re

class Sequence(object):
    def __init__(self, name, sequence = ""):
        if (isinstance(name, unicode)):
            self.name = name
        else:
            self.name = name.decode('ascii')
        self.annotation = {} # keys are annotation type, values are sets of annotations (strings)
        if (isinstance(sequence, unicode)):
            self.sequence = sequence
        else:
            self.sequence = sequence.decode('ascii')
        self.localization = [] #list of localizations
        
    def __repr__(self):
        out = "Sequence(\n"
        out += repr(self.name) + "\n"
        out += repr(self.annotation) + "\n"
        out += repr(self.sequence) + "\n"
        out += repr(self.localization) + "\n"
        
        return out

    
    def add_annotation(self, annotation_name, annotation):
        pass
    
    
    def add_localization(self, localization):
        pass
    
    @classmethod
    def from_fasta(cls, infile, name_regex, include_sequence = False):
        
        '''
            In: infile is a file object of a fasta file, name_regex is a regular expression with which to parse the sequence names in the fasta file
                the first match group will be used as the sequence name, include sequence indicates whether a string of the sequence should be
                included.
            out: a dict where keys are sequence names and values are Sequence objects
        '''
        out = {}
        name_pattern = re.compile(name_regex)
        name = ""
        sequence = ""
        for line in infile:
            line = line.decode('ascii')
            line = line.strip()
            if (line.startswith('>')):
                #its a sequence name line
                #first add the previous sequence to the output
                if (name != ""):
                    cls._add_seq_to_dict(out, name, sequence, include_sequence)
                #next reset the sequence parameters
                name = ""
                sequence = ""
                
                #finally, read the name of the next sequence
                line = line[1:] #take off the starting '>' 
                name_match = name_pattern.match(line)
                name = name_match.group(1)
                if (name == ""):
                    print "error no name match for " + line + "using entire line"
                    name == line
            elif (line == ""):
                #ignore blank lines
                pass
            else:
                #other lines are probably sequence, so add them to the sequence variable
                sequence += line
        #pick up the last squence (where there is no next one to start with a '>'
        cls._add_seq_to_dict(out, name, sequence, include_sequence)
        return out
    
    @classmethod
    def _add_seq_to_dict(cls, collection, name, seq, include_sequence):
        name = name.decode('ascii')
        if name not in collection:
            if (include_sequence):
                new_sequence = Sequence(name,sequence)
            else:
                new_sequence = Sequence(name)
            collection[name] = new_sequence
        else:
            print "error multiple instances of sequence named " + name
    
    @classmethod
    def from_fasta_file(cls, filename, name_regex, include_sequence = False):
        
        '''
            In: filename is the name of a fasta file, name_regex is a regular expression with which to parse the sequence names in the fasta file
                the first match group will be used as the sequence name, include sequence indicates whether a string of the sequence should be
                included.
            out: a dict where keys are sequence names and values are Sequence objects
        '''
        out = {}
        with open(filename) as infile:
            out = cls.from_fasta(cls, infile, name_regex, include_sequence)
        return out
    
    @classmethod
    def annotations_from_tsv (cls, sequence_dict, infile, annotation_name):
        '''
            sequence_dict is a dictionary of sequence objects, 
            filename is a tsv file object
            the keys of the dict should correspond to the entries in the first column of the tsv file
            subsequent columns of the tsv file will be interpretted as annotations and added to
                the list at sequence_dict[first_column].annotation[annotation_name]
        '''
        for key in sequence_dict.iterkeys():
            if (annotation_name not in sequence_dict[key].annotation):
                sequence_dict[key].annotation[annotation_name] = set()
        for line in infile:
            line = line.strip()
            cols = line.split("\t")
            #print line
            if ((len(cols)  > 1) and (cols[0] in sequence_dict)):
                ## if there is no annotation or the sequence isn't in the list, don't bother continuing
               
                for annotation in cols[1:]:
                    if (annotation != ""):
                        sequence_dict[cols[0]].annotation[annotation_name].update(set([annotation]))

    @classmethod
    def annotations_from_tsv_file (cls, sequence_dict, filename, annotation_name):
        '''
            sequence_dict is a dictionary of sequence objects, 
            filename is a tsv file
            the keys of the dict should correspond to the entries in the first column of the tsv file
            subsequent columns of the tsv file will be interpretted as annotations and added to
                the list at sequence_dict[first_column].annotation[annotation_name]
        '''
        with open(filename) as infile:
            out = cls.annotations_from_tsv(sequence_dict, infile, annotation_name)
        return out