import re
import argparse
import pandas as pd

"""
    generates entries for the "Protein" column of a model file by correllating EC numbers to the ExPASy Enzyme database

    example: EC_to_uniprot.py main.tsv ec_uniprot.tsv bna572_main_protein.tsv
"""

def main(model_file, enzyme_file, output):
    
    enzyme = pd.read_table(enzyme_file)
    enzyme = enzyme.fillna(value={'uniprot':""})
    model = pd.read_table(model_file)
    out = dict() #a dict of dataframes where the key is from the "name" column of config.txt and the value is a dataframe derived from the derived from that row
    out_order = list()
    
    for (index, row) in model.iterrows():
        if (pd.notnull(row['EC'])):
            ECs = set(row['EC'].strip().split())
        else:
            ECs = set()
        out_order.append(row['Rxn name'])
        out[row['Rxn name']] = ""
        for EC in ECs:
            enz_row = enzyme[enzyme['EC'] == EC] #this will be a single row because EC is unique in enzyme
            if enz_row.shape[0] == 1:
                #print enz_row
                if enz_row['transferred'].iat[0] == True:
                    print "warning " + row['EC'] + " transferred reaction " + str(row['Rxn name'])
                out[row['Rxn name']] += enz_row['uniprot'].iat[0] + " "
            elif enz_row.shape[0] == 0:
                if row['EC'] != 'no ec':
                    print "warning, no Enzyme record for EC: " + EC
                out[row['Rxn name']] = ""
            else:
                #should be impossible
                print "warning, multiple Enzyme records for EC: " + EC
    with open(output, "w") as outfile:
        for name in out_order:
            outfile.write(name + "\t" +  " ".join(set(out[name].strip().split())) + "\n")
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("model_file")
    parser.add_argument("enzyme_file")
    parser.add_argument("output")
    args = parser.parse_args()
    
    main(args.model_file, args.enzyme_file, args.output)