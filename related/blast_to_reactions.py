import re
import argparse
import pandas as pd

"""
    reads a config file then based on the config file, reads tsv files containing sequence annotations and sequence to reaction associations
"""

def main(input, output, separate):
    config = pd.read_table(input)


    out = dict() #a dict of dataframes where the key is from the "name" column of config.txt and the value is a dataframe derived from the derived from that row
    #TODO: implement the "separate" parameter (right now it's being treated as always == True)
    
    for (config_row_num, config_row) in config.iterrows():
        config_row_out = dict()
        
        blast_table = pd.read_table(config_row['blast_file'])#, index_col=0)
        reaction_table = pd.read_table(config_row['reactions_file'])#, index_col=0)
        reaction_table = reaction_table.dropna(subset=[config_row['seq_column']]) #if it doesn't have anything in the seq_column, no reason to waste time iterating over it below
        
        reaction_columns = [config_row['name_column']]
        reaction_names = [config_row['name']]
        
        reaction_columns += config_row['reaction_columns'].split(' ')
        reaction_names += config_row['reaction_names'].split(' ')
        
        
        #take the length of the smallest so we can avoid out of bounds errors later
        reaction_names_len = min(len(reaction_names), len(reaction_columns))
        if (len(reaction_names) != len(reaction_columns)):
            print "error, not the same amount of reaction names and reaction columns for " + config_row['reactions_file']
        
        blast_table.fillna(value={'subject': '&&&WILL_NOT_MATCH_ANYTHING&&&'},inplace=True)#TODO: unclumsify this
        
        reactions_sets = [set(reactions_string.split(" ")) for reactions_string in reaction_table[config_row['seq_column']].fillna("")] #convert the space separated sequence names to sets of sequence names
        #TODO: I wonder if the set could be replaced with hierarchical columns to make the finding of entries faster
        
        for (blast_row_index_number, blast_row) in blast_table.iterrows():

            reaction_subset = reaction_table[[(blast_row['subject'] in tempset) for tempset in reactions_sets]]
            
            # if blast_row['subject'] == "":
                # #filter out the matches that matched the blank string            
                # reaction_subset = reaction_table[[False] * len(reaction_table.index)]
            
            if (not blast_row['query'] in config_row_out):
                config_row_out[blast_row['query']] = dict()
            
            for reaction_name_index in range(0, reaction_names_len):
                
                if (not reaction_names[reaction_name_index] in config_row_out[blast_row['query']]):
                    config_row_out[blast_row['query']][reaction_names[reaction_name_index]] = ""
                output_ids = reaction_subset[reaction_columns[reaction_name_index]].dropna().astype(str)
                config_row_out[blast_row['query']][reaction_names[reaction_name_index]] += "|||" + "|||".join(output_ids) #hopfully ||| is never in an input string...
    
        ### collapse multiples ####
        for query_dict in config_row_out.itervalues():
            for col_name in query_dict.iterkeys():
                query_dict[col_name] = " ".join(set(query_dict[col_name].split("|||"))) #split by space, convert to a set, and join by space to get rid of redundant words
        
        #there should be no more than 1 row in the config file for each "reactions_file","blast_file" combination, so this index should be unique to a row in the config file
        out[config_row['reactions_file'] + " " + config_row['blast_file']] = pd.DataFrame.from_dict(config_row_out, orient="index")# .to_csv(output)
        
        #TODO: should make a "verbose" flag
        print config_row['reactions_file'] + " " + config_row['blast_file']
    pd.concat([out[out_row] for out_row in sorted(out.keys())], keys=sorted(out.keys())).to_csv(output, sep="\t")
        
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", help="separate output. Causes the output to be separated by source file as well as sequence name")
    parser.add_argument("config_file")
    parser.add_argument("output")
    args = parser.parse_args()
    
    main(args.config_file, args.output, args.s)