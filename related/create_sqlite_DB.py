import sqlite3
import pandas as pd
import numpy as np
import re



def main():

    with sqlite3.connect('test.db') as con:
        cur = con.cursor()
        
        ### BLAST RESULTS TABLE ###
        # create_blast_table(cur)
        # add_tsv_to_blast_table(cur, 'contigs_to_tair_top5.tsv', 'TAIR', 'blastx_top5')
        # add_tsv_to_blast_table(cur, 'contigs_to_swissprot_top5.tsv', 'Swissprot', 'blastx_top5')
        # add_tsv_to_blast_table(cur, 'contigs_to_nr_top5.tsv', 'nr', 'blastx_top5')
        # add_tsv_to_blast_table(cur, 'contigs_to_experimental_peppermint_top5.tsv', 'curated_peppermint', 'blastx_top5')
        
        ### CONTIGS TABLE ###
        # create_contigs_table(cur)
        # add_fasta_to_contigs_table(cur, "pepAndParentsAndEst.fasta")
        
        ### EXPRESSION TABLE ###
        #create_expression_table(cur)
        #add_rsem_to_expression_table(cur, "rsem_pep_young.isoforms.results", "young")
        #add_rsem_to_expression_table(cur, "rsem_pep_mature.isoforms.results", "mature")
        
        ### REACTIONS TABLE ####
        create_reactions_table(cur)
        #create_aracyc_table(cur)
        add_tsv_to_reactions_table(cur, "Rhea", "rhea.tsv", "name", "formula", "direction")
        add_tsv_to_reactions_table(cur, "AraCyc", "aracyc_gpr.tsv", "Rxn name", "Formula", "reversible", direction_dict={0:'left-to-right',1:'reversible'})

        
        # ### REACTION ASSOCIATION TABLES ###
        # create_reaction_association(cur)
        # add_csv_to_reaction_association_table(cur, "rhea.csv", "rhea", reaction_name="name", type="EC", )
        # add_aracyc_to_reaction_association_table(cur, "aracyc_gpr.csv", "aracyc")
        
        # ### CONTIG ANNOTATION TABLES ###
        # create_automatic_annotation_table(cur)
        # create_manual_annotation_table(cur)
        
        
        ### reference annotations ###
        #TAIR
        #Swissprot
        #curated_peppermint
        
        
        
        
def add_tsv_to_reactions_table(cur, source, filename, id_col, reaction_col, direction_col, direction_dict={'left-to-right':'left-to-right','reversible':'reversible', "right-to-left":"right-to-left", "unknown": "reversible"}):
    
    
    subs = {"source": source}
    data = pd.read_table(filename)
    
    #substitute in standardized descriptions into direction column
    
    data.subs(to_replace={direction_col:direction_dict}, inplace = True)
    subs = {'id':id_col, 'forumla':reaction_col, 'direction':direction_col}
    
    tsv_into_table(cur, "reactions", data, canned, subs)

    # CREATE TABLE expression(
    # source TEXT NOT NULL,
    # id TEXT NOT NULL,
    # formula TEXT,
    # direction TEXT NOT NULL
    # PRIMARY KEY(source, id)
    
def add_rsem_to_expression_table(cur, rsem_file, condition):
    subs = {'contig_name': 'transcript_id'} #database_colname : datafile_colname
    canned = {'condition': condition} #data that is the same for all rows
    data = pd.read_table(rsem_file)
    
    tsv_into_table(cur, "expression", data, canned, subs)

def add_fasta_to_contigs_table(cur, fasta, type='nucleotide' , name_regex = "^(\S+)", match_group=1):
    data = parse_fasta(fasta, name_regex, match_group)
    
    statement = "INSERT INTO contigs (name, type, length, sequence) VALUES (?,?,?,?)"
    table_input = list()
    for (seq_name, seq) in data.iteritems():
        data_row = list()
        
        #name
        data_row.append(seq_name)
        
        #type
        data_row.append(type)
        
        #length
        data_row.append(len(seq))
        
        #sequence
        data_row.append(seq)
        
        table_input.append(data_row)
    cur.executemany(statement, table_input)


def parse_fasta(infile_name, name_regex, match_group = 1):
    """
        In: infile_name is the name of a fasta file, name_regex is a regular expression with which to parse the sequence names in the fasta file
            the first match group will be used as the sequence name by default, but others could be used
        out: a dict where keys are sequence names and values are sequences
    """
    with open(infile_name, 'rb') as infile:
        out = {}
        name_pattern = re.compile(name_regex)
        name = ""
        sequence = ""
        for line in infile:
            line = line.decode('ascii')
            line = line.strip()
            if (line.startswith('>')):
                #its a sequence name line
                #first add the previous sequence to the output
                if (name != ""):
                    out[name] = sequence
                #next reset the sequence parameters
                name = ""
                sequence = ""
                
                #finally, read the name of the next sequence
                line = line[1:] #take off the starting '>' 
                name_match = name_pattern.match(line)
                name = name_match.group(match_group)
                if (name == ""):
                    print "error no name match for " + line + "using entire line"
                    name == line
            elif (line == ""):
                #ignore blank lines
                pass
            else:
                #other lines are probably sequence, so add them to the sequence variable
                sequence += line
        #pick up the last squence (where there is no next one to start with a '>'
        out[name] = sequence
    return out
            

#TODO: maybe make another one to load from xml files
def add_tsv_to_blast_table(cur, hits_tsv, subject_db, experiment):
    subs = {'contig_name': 'query', 'hit_order': 'hit', 'bit_score': 'bits'} #database_colname : datafile_colname
    canned = {'database': subject_db, 'experiment': experiment} #data that is the same for all rows
    data = pd.read_table(hits_tsv)
    
    # if there weren't any hits, don't add it to the database
    data = data.dropna(subset=['subject'])
    tsv_into_table(cur, "blast", data, canned, subs)


def tsv_into_table(cur, db_table_name, pandas_table, canned, subs):
    """
        insert data from a tsv file into a database table
        cur: a cursor for the database
        db_table_name: the name of the database table
        pandas_table: a pandas data_frame
        canned: a dict of values to use in every newly inserted row.  keys are table column names, values are what to put in that column
        subs: a dict mapping db_table columns to pandas table columns
    """
    
    #get db column names
    data = pandas_table
    
    #get table column names
    cur.execute('select * from ' + db_table_name + ' LIMIT 1')
    db_cols = list(map(lambda x: x[0], cur.description))
    
    #get tsv column names
    data_cols = data.columns.tolist()
    
    statement = "INSERT INTO " + db_table_name +" ("+ ",".join(db_cols) +") VALUES (" + ','.join(['?']*len(db_cols)) +")"
    
    table_input = list()
    for (row_num, row) in data.iterrows():
        row_data = list()
        for tmp_colname in db_cols:
            colname = tmp_colname
            if colname in subs:
                colname = subs[colname]
            if colname in canned:
                row_data.append(canned[colname])
            elif colname in data_cols and not row[colname] is np.nan:
                row_data.append(row[colname])
            else:
                print colname
                print "none"
                row_data += [None]
                print row_data
        table_input.append(row_data)
    cur.executemany(statement, table_input)

def create_blast_table(cur):
    cur.execute("DROP TABLE IF EXISTS blast")
    query = """
        CREATE TABLE blast(
            database TEXT NOT NULL,
            experiment TEXT NOT NULL, 
            contig_name TEXT NOT NULL,
            hit_order INTEGER NOT NULL,
            subject TEXT NOT NULL,
            bit_score REAL NOT NULL,
            e_value REAL NOT NULL,
            identities INTEGER,
            query_coverage REAL,
            subject_coverage REAL,
            align_length INTEGER,
            query_start INGEGER,
            query_end INTEGER,
            subject_start INTEGER,
            subject_end INTEGER,
            PRIMARY KEY(database, experiment, contig_name, hit_order)
        );"""
    cur.execute(query)

def create_contigs_table(cur):
    cur.execute("DROP TABLE IF EXISTS contigs")
    query = """
        CREATE TABLE contigs(
            name TEXT NOT NULL,
            type TEXT NOT NULL,
            length INTEGER NOT NULL,
            sequence TEXT NOT NULL,
            PRIMARY KEY(name)
        );"""
    cur.execute(query)
    
def create_expression_table(cur):
    cur.execute("DROP TABLE IF EXISTS expression")
    #transcript_id	gene_id	length	effective_length	expected_count	TPM	FPKM	IsoPct
    query = """
        CREATE TABLE expression(
            contig_name TEXT NOT NULL,
            condition TEXT NOT NULL,
            gene_id TEXT,
            effective_length INTEGER NOT NULL,
            expected_count REAL NOT NULL,
            TPM REAL NOT NULL,
            FPKM REAL NOT NULL,
            IsoPct REAL NOT NULL,
            PRIMARY KEY(contig_name, condition)
        );"""
    cur.execute(query)

def create_reactions_table(cur):
    cur.execute("DROP TABLE IF EXISTS reactions")
    query = """
        CREATE TABLE expression(
            source TEXT NOT NULL,
            id TEXT NOT NULL,
            formula TEXT,
            direction TEXT NOT NULL,
            PRIMARY KEY(source, id)
        );"""
    cur.execute(query)

def create_reaction_annotation_table(cur):
    cur.execute("DROP TABLE IF EXISTS reaction_annotation")
    query = """
        CREATE TABLE expression(
            source TEXT NOT NULL,
            id TEXT NOT NULL,
            reaction_source TEXT NOT NULL,
            reaction_id TEXT NOT NULL,
            PRIMARY KEY(source, id, reaction_source, reaction_id)
        );"""
    cur.execute(query)
#def add_csv_to_reactions(cur):
    
# def create_automatic_annotation_table(cur):
    # cur.execute("DROP TABLE IF EXISTS automatic_annotation")
    # #transcript_id	gene_id	length	effective_length	expected_count	TPM	FPKM	IsoPct
    # query = """
        # CREATE TABLE expression(
            # contig_name TEXT NOT NULL,
            # annotation_source TEXT NOT NULL,
            # description TEXT,
            # metabolic BOOLEAN,
            # notes TEXT,
            # PRIMARY KEY(contig_name, annotation_source)
        # );"""
    # cur.execute(query)
    
    # cur.execute("DROP TABLE IF EXISTS automatic_annotation_reactions")
    # query = """
        # CREATE TABLE expression(
            # contig_name TEXT NOT NULL,
            # annotation_source TEXT NOT NULL,
            # reaction TEXT,
            # direction TEXT,
            # notes TEXT,
            # PRIMARY KEY(, source)
        # );"""
    # cur.execute(query)
    
# def create_manual_annotation_tables(cur):
    # cur.execute("DROP TABLE IF EXISTS manual_annotation")
    # #transcript_id	gene_id	length	effective_length	expected_count	TPM	FPKM	IsoPct
    # query = """
        # CREATE TABLE expression(
            # contig_name TEXT NOT NULL,
            # annotation_source TEXT NOT NULL,
            # description TEXT,
            # metabolic BOOLEAN,
            # notes TEXT,
            # PRIMARY KEY(contig_name, annotation_source)
        # );"""
    # cur.execute(query)
    
    # cur.execute("DROP TABLE IF EXISTS automatic_annotation_reactions")
    # query = """
        # CREATE TABLE expression(
            # contig_name TEXT NOT NULL,
            # annotation_source TEXT NOT NULL,
            # reaction TEXT,
            # direction TEXT,
            # notes TEXT,
            # PRIMARY KEY(, source)
        # );"""
    # cur.execute(query)
    
if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument("sequence_to_expression")
    # parser.add_argument("reaction_to_sequence")
    # parser.add_argument("output")
    # args = parser.parse_args()
    #main(args.sequence_to_expression, args.reaction_to_sequence, args.output)
    main()