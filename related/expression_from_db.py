import sqlite3
import pandas as pd
import pandas.io.sql as sql
import numpy as np
import re

con = sqlite3.connect('test.db')

hits = sql.read_frame("SELECT contig_name, subject FROM blast WHERE experiment='blastx_top5' AND database='TAIR' AND e_value=0 AND hit_order = 1", con)


young_expression = dict()
mature_expression = dict()
log2_change = dict()

young_table = sql.read_frame("SELECT contig_name, TPM FROM expression WHERE condition='young'", con)
mature_table = sql.read_frame("SELECT contig_name, TPM FROM expression WHERE condition='mature'", con)
for (row_num, row) in hits.iterrows():
    if not row['subject'] in young_expression:
        young_expression[row['subject']] = 0
    if not row['subject'] in mature_expression:
        mature_expression[row['subject']] = 0
    #print young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM']
    #print type(young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM'])
    young_expression[row['subject']] += young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM']
    mature_expression[row['subject']] += mature_table[mature_table.contig_name == row['contig_name']].irow(0)['TPM']
    log2_change[row['subject']] = np.log2(mature_expression[row['subject']]/young_expression[row['subject']])

pd.DataFrame({'young_TPM': young_expression, "mature_TPM": mature_expression, "log2_mature_young":log2_change}).to_csv("tair_good_hit_young_mature_expression.tsv", sep="\t")

