import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

e_thresh = 1E-200

def main():

    
    ax = []
    # ax.append(e_val_hist_from_file('estscan_to_nr_top1.txt'))
    # plt.show()
    # plt.close()
    
    # ax.append(e_val_hist_from_file('contigs_to_nr_top1.tsv'))
    # plt.show()
    #plt.close()
    
    ax.append(hist_from_file('contigs_to_nr_top1.tsv'))
    plt.show()
    plt.close()


def hist_from_file(filename):
    
    data = pd.read_table(filename, index_col=0)

    data = e_threshold(data, e_thresh)
    data = data.dropna(subset=['subject_coverage'])

    data.to_csv('out.csv')
    return data['subject_coverage'].hist(bins=50)
    
def e_threshold(data, e_val):
    s = data['e_value']
    s[s > e_val] = float('NaN')
    
    data['e_value'] = s
    
    data = data.dropna(subset=['e_value'])
    return data
    
    
if __name__ == '__main__':
    main()