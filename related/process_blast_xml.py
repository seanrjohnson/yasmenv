from Bio.Blast import NCBIXML
import re
import argparse

def main(infile_name, outfile_name, sbj_col, hits_to_report, e_cutoff):
    
    with open(infile_name, "rb") as blast_handle:
        blast_records = NCBIXML.parse(blast_handle)
        print_best_hits(blast_records, outfile_name, sbj_col, hits_to_report, e_cutoff)

def print_best_hits(blast_records, outfile_name, sbj_col, hits_to_report, e_cutoff):
    """
        prints the following data for the best hit for each query from a BLAST xml file.  If a query has no hits, the query name is printed with nothing after
        
        query	hit	subject	bits	e_value	identities	query_coverage	subject_coverage	align_length	query_start	query_end	subject_start	subject_end
    """
    with open(outfile_name, "w") as outfile:
        outfile.write("query hit subject bits e_value identities query_coverage subject_coverage align_length query_start query_end subject_start subject_end".replace(" ","\t") + "\n")
        print "started writing"
        for rec in blast_records:

            #NOTE: this assumes that rec.alignments[0].hsps[0] is the best matching hsp
            hits_reported = 0
            for i_align in range(len(rec.alignments)):
                if (hits_reported < hits_to_report):
                    if ((e_cutoff is None) or (rec.alignments[i_align].hsps[0].expect <= float(e_cutoff))):
                        hits_reported += 1
                        
                        
                        #query
                        #use name before any space as the name
                        outfile.write(re.split('\s',rec.query)[0])
                        
                        #hit number
                        outfile.write("\t"+str(hits_reported))
                        
                        #subject
                        sbj = ""
                        split_def = re.split('\|',rec.alignments[i_align].hit_def)
                        
                        if (sbj_col <= 0 or sbj_col > len(split_def) ):
                            sbj = rec.alignments[i_align].hit_def.strip()
                        else:
                            sbj = split_def[sbj_col-1].strip()
                            
                        outfile.write("\t"+sbj)
                        
                        #bits
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].bits))
                        
                        #e-value
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].expect))
                        
                        #identities
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].identities))
                        
                        
                        #query_coverage
                        #careful with blastx nt vs protein coordinates
                        outfile.write("\t"+str(coverage(rec.alignments[i_align].hsps[0].query_start, rec.alignments[i_align].hsps[0].query_end, rec.query_length)))
                        
                        #subject_coverage
                        outfile.write("\t"+str(coverage(rec.alignments[i_align].hsps[0].sbjct_start, rec.alignments[i_align].hsps[0].sbjct_end, rec.alignments[0].length)))
                        
                        #align_length
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].align_length))
                        
                        #query_start
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].query_start))
                        
                        #query_end
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].query_end))
                        
                        #subject_start
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].sbjct_start))
                        
                        #subject_end
                        outfile.write("\t"+str(rec.alignments[i_align].hsps[0].sbjct_end))
                        outfile.write("\n")
                else:
                    break #go to the next query sequence
            if (hits_reported == 0):
                #if there are no hits fitting the criteria, print the sequence name anyways
                #use name before any space as the name
                outfile.write(re.split('\s',rec.query)[0])
                outfile.write("\n")
            


def coverage(start, end, total_length):
    return ((abs(start-end) + 1.0)/total_length) * 100

    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--subject_part", type = int, default=0, help="subject sequence names assumed to be divided into parts by pipes '|', this parameter specifies which of these divisions to use as the query sequence name. 0 for the whole thing. Default: 0")
    parser.add_argument("-n", type = int, default=1, help="number of hits to report for each query sequence. Default: 1")
    parser.add_argument("-e", type = float, default=None, help="only report hits with e-values below this number. If not specified, don't filter by e-value")
    parser.add_argument("blast_xml")
    parser.add_argument("output")
    args = parser.parse_args()

    main(args.blast_xml, args.output, args.subject_part, args.n, args.e)

