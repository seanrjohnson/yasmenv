import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
def main():
    ax = []
    # ax.append(e_val_hist_from_file('estscan_to_nr_top1.txt'))
    # plt.show()
    # plt.close()
    
    # ax.append(e_val_hist_from_file('contigs_to_nr_top1.tsv'))
    # plt.show()
    #plt.close()
    
    ax.append(e_val_hist_from_file('contigs_to_swissprot_top1.tsv'))
    plt.show()
    plt.close()


def e_val_hist_from_file(filename):
    
    data = pd.read_table(filename, index_col=0)

    data = data.dropna(subset=['query_coverage'])

    return data['subject_coverage'].hist(bins=50)
    
if __name__ == '__main__':
    main()