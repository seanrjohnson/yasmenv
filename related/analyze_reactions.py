import pandas as pd


def main(experiments, annotations, reaction_mapping_file, out_file_name):

    #experiments = ['AraCyc_GPR.tsv contigs_to_tair_top1.tsv','ec_uniprot.tsv contigs_to_swissprot_top1.tsv','full.tsv contigs_to_swissprot_top1.tsv','full.tsv contigs_to_tair_top1.tsv'] #unique from first column
    #annotations = ['AraCyc','EC','Enzyme','bna572'] #columns other than the first two
    
    #experiments = ['AraCyc_GPR.tsv high_confidence_TAIR_hits.tsv','ec_uniprot.tsv high_quality_swissprot_hits.tsv','full.tsv high_confidence_TAIR_hits.tsv','full.tsv high_quality_swissprot_hits.tsv'] #unique from first column
    #annotations = ['AraCyc','EC','Enzyme','bna572'] #columns other than the first two
    bna_572_ecs = set(['1.3.5.1','1.18.1.2','3.1.3.11','6.3.1.2','3.5.1.2','6.3.5.4','2.6.1.2','4.1.1.31','4.1.1.49','3.5.1.1','2.6.1.1','1.1.1.37','1.1.1.42','4.2.1.3','2.7.4.3','5.1.3.1','2.7.9.1','2.7.1.40','1.2.1.9','5.3.1.1','4.1.2.13','2.7.1.11','2.7.1.90','5.3.1.9','3.2.1.26','2.4.1.13','2.7.1.4','2.7.1.2','1.1.1.39','1.1.1.37','4.2.1.2','2.3.3.1','2.6.1.1','6.3.1.2','2.7.2.3','2.7.1.19','4.1.2.13','3.1.3.37','6.4.1.2','1.4.1.13','1.4.1.2','6.2.1.1','1.1.1.82','1.1.1.40','2.7.4.3','3.6.1.1','2.2.1.1','2.2.1.2','2.2.1.1','5.3.1.6','5.1.3.1','4.1.1.39','2.7.1.40','1.2.1.13','1.2.1.12','5.3.1.1','4.1.2.13','2.7.1.11','3.1.3.11','5.3.1.9','2.3.3.1','1.6.5.3','1.6.5.3','3.6.3.14','3.6.3.14','1.7.7.1','1.7.1.1','3.1.1.3','2.7.1.30','1.3.3.6','4.2.1.17','4.2.1.17','5.3.3.8','1.3.3.6','4.2.1.17','1.3.3.6','4.2.1.17','4.2.1.17','4.2.1.17','4.2.1.17','1.1.1.35','1.1.1.35','1.1.1.35','1.1.1.35','4.2.1.17','2.3.1.16','2.3.1.16','2.3.1.16','2.3.1.16','4.2.1.17','6.2.1.3','1.3.3.6','4.2.1.17','5.3.3.8','4.2.1.17','1.1.1.35','1.1.1.35','2.3.1.16','2.3.1.16','2.3.1.16','4.2.1.17','5.3.3.8','1.3.3.6','4.2.1.17','1.1.1.35','1.1.1.35','2.3.1.16','5.3.3.8','1.3.3.6','4.2.1.17','1.1.1.35','2.3.1.16','6.2.1.3','5.3.3.8','4.2.1.17','5.3.3.8','5.3.3.8','6.2.1.3','5.3.3.8','4.2.1.17','4.2.1.17','5.3.3.8','4.2.1.17','4.2.1.17','1.3.3.6','4.2.1.17','1.1.1.35','2.3.1.16','1.3.3.6','4.2.1.17','1.1.1.35','2.3.1.16','4.2.1.17','6.2.1.3','6.2.1.3','6.2.1.3','2.3.1.180','1.1.1.100','4.2.1.58','1.3.1.9','2.3.1.41','1.1.1.100','4.2.1.59','1.3.1.9','2.3.1.41','1.1.1.100','4.2.1.59','1.3.1.9','2.3.1.41','1.1.1.100','4.2.1.59','1.3.1.9','2.3.1.41','1.1.1.100','4.2.1.59','1.3.1.9','2.3.1.41','1.1.1.100','4.2.1.59','1.3.1.9','4.2.1.61','1.3.1.9','2.3.1.41','1.1.1.100','2.3.1.39','1.3.5.2','1.8.7.1','1.1.1.205','6.3.5.2','2.7.4.3','2.7.4.6','3.5.4.6','1.8.1.9','2.7.4.3','2.7.4.6','3.3.1.1','2.1.1.13','2.7.1.20','1.17.4.2','1.17.4.2','2.1.2.1','2.5.1.6','1.1.1.8','6.3.4.2','1.17.4.2','1.17.4.2','2.7.4.6','2.7.4.9','2.1.1.45','2.7.4.9','2.7.4.6','1.5.1.3','2.1.3.2','3.5.2.3','2.3.1.1','2.7.2.8','1.2.1.38','2.6.1.11','3.5.1.16','2.1.3.3','6.3.4.5','2.1.2.2','6.3.5.3','6.3.3.1','6.3.4.18','5.4.99.18','6.3.2.6','4.3.1.19','2.2.1.6','1.1.1.86','4.2.1.9','2.6.1.42','4.2.1.52','1.3.1.26','2.6.1.83','5.1.1.7','4.1.1.20','2.3.3.13','4.2.1.33','1.1.1.85','2.6.1.42','4.2.1.19','2.6.1.9','3.1.3.15','1.1.1.23','2.4.2.17','3.6.1.31','3.5.4.19','5.3.1.16','2.7.2.11','1.2.1.41','1.5.1.2','2.1.1.10','2.5.1.-','4.4.1.8','2.2.1.6','1.1.1.86','4.2.1.9','4.1.1.23','2.7.4.3','2.7.4.6','6.3.4.4','2.7.4.6','3.5.4.10','4.1.3.27','2.4.2.18','5.3.1.24','1.1.1.25','2.7.1.71','2.5.1.19','5.4.99.5','2.6.1.79','2.4.2.14','6.3.4.13','2.7.4.3','2.7.4.6','4.2.1.91','1.3.1.43','1.1.1.3','2.7.1.39','2.7.2.4','1.2.1.11','2.6.1.42','2.5.1.54','4.2.3.4','3.5.4.9','1.5.1.5','2.4.1.21','2.7.7.27','1.8.4.9','1.8.1.7','2.1.2.3','4.3.2.2','1.8.1.9','2.4.2.10','4.3.2.2','2.3.1.30','2.5.1.47','2.1.2.1','4.2.1.20','4.1.1.48','4.3.2.1','6.3.5.5','2.4.2.-','4.2.3.1','2.7.6.1','4.2.3.5','4.2.1.10','2.6.1.1','2.7.7.4','2.4.1.12','5.3.3.8','1.3.3.6','4.2.1.17','1.3.3.6','4.2.1.17','1.3.3.6','4.2.1.17','1.1.1.35','1.1.1.35','1.1.1.35','2.3.1.16','2.3.1.16','2.3.1.16','1.3.3.6','4.2.1.17','4.2.1.17','1.3.3.6','4.2.1.17','1.1.1.35','1.1.1.35','2.3.1.16','2.3.1.16','1.3.3.6','1.3.3.6','4.2.1.17','1.3.3.6','4.2.1.17','1.1.1.35','1.1.1.35','2.3.1.16','2.3.1.16','1.3.3.6','4.2.1.17','1.1.1.35','1.1.1.35','2.3.1.16','2.3.1.16','4.2.1.17','1.11.1.6','2.7.1.31','2.6.1.45','1.1.1.29','1.1.1.37','1.3.3.6','4.2.1.17','1.1.1.35','2.3.1.16','1.2.4.2','2.3.1.61','1.8.1.4','6.2.1.5','1.4.4.2','2.1.2.10','1.8.1.4','1.10.99.1','1.10.2.2','1.9.3.1','1.3.3.6','1.1.1.35','2.3.1.16','1.1.1.95','2.6.1.52','3.1.3.3','3.1.3.18','4.1.1.39','1.1.3.15','1.1.1.49','3.1.1.31','1.1.1.44','1.2.4.1','2.3.1.12','1.8.1.4','4.1.1.15','1.4.1.2','2.6.1.19','1.2.1.16','1.1.1.36','4.2.1.17','1.3.1.38','1.1.1.36','4.2.1.17','1.3.1.38','2.3.1.119','1.1.1.36','6.2.1.3','6.2.1.3','6.2.1.3','6.4.1.2','2.3.1.119','1.3.1.38','4.2.1.17','1.1.1.36','2.3.1.119','1.3.1.38','4.2.1.17','2.3.1.119','1.14.99.-','1.14.99.-','2.3.3.8','1.1.1.49','3.1.1.31','1.1.1.44','6.2.1.3','6.2.1.3','6.2.1.3','2.6.1.4','4.1.3.1','4.2.1.59','1.3.1.9','2.3.1.41','1.1.1.100','3.1.2.14','3.1.2.14','1.1.1.42','5.4.2.1','4.2.1.11','2.7.1.4','4.2.1.3','1.1.1.41','2.3.1.12','1.8.1.4','1.2.4.1','2.7.1.105','3.1.3.46','2.2.1.2','2.2.1.1','5.3.1.6','2.2.1.1','5.4.2.1','4.2.1.11','2.7.7.9','5.4.2.2','2.7.4.6','2.4.1.14','3.1.3.24','1.14.19.2','3.1.2.14','2.3.3.9','2.7.1.2','2.1.2.1','1.5.1.20','5.4.2.2','2.7.2.3','1.2.1.12','2.2.1.7','1.1.1.267','2.7.7.60','2.7.1.148','4.6.1.12','1.17.7.1','1.17.1.2','1.17.1.2','5.3.3.2','2.5.1.1','2.7.4.14','2.7.4.6','4.2.3.16','1.14.13.47','1.1.1.223','1.3.1.82','1.3.1.81','1.3.1.81','1.14.13.104','1.1.1.207','1.1.1.208'])
    #456 total

    df = pd.read_table(reaction_mapping_file)
    df = df.fillna("")
    seqs = df['seq name'].unique()
    
    seq_to_exp = {x:set() for x in experiments}
    ec_to_exp = {x:set() for x in experiments}
    for seq in seqs:
        recs_table = df[df['seq name'] == seq]
        recs = dict()
        for exp in experiments:
            if not recs_table[recs_table['comparison'] == exp].empty:
                recs[exp] = recs_table[recs_table['comparison'] == exp].irow(0)
            
        
        for exp in experiments:
            found = False
            for annotation in annotations:
                if exp in recs and recs[exp][annotation] != '':
                    #print recs[exp]
                    found = True
            if found == True:
                seq_to_exp[exp].add(seq)
                ec_to_exp[exp] = ec_to_exp[exp].union(set(recs[exp]['EC'].split()))
                
    
    print "CONTIGS"
    for exp in experiments:
        unique = seq_to_exp[exp]
        #print unique
        different = unique
        for exp2 in experiments:
            if exp2 != exp:
                unique = unique.difference(seq_to_exp[exp2])
                difference = different.difference(seq_to_exp[exp2])
                print "IN " + exp +" NOT "+exp2+":"+ str(len(difference))
        print "total in "+exp+": "+str(len(seq_to_exp[exp]))
        print "unique to "+exp+": "+str(len(unique))


    print "ECs"
    experiments.append('bna572')
    ec_to_exp['bna572'] = bna_572_ecs
    unique_ecs = dict()
    different_ecs = dict()
    for exp in experiments:
        unique_ecs[exp] = ec_to_exp[exp]
        
        
        for exp2 in experiments:
            if exp2 != exp:
                comp_name = "IN " + exp +" NOT "+exp2
                different_ecs[comp_name] = ec_to_exp[exp]
                unique_ecs[exp] = unique_ecs[exp].difference(ec_to_exp[exp2])
                different_ecs[comp_name] = different_ecs[comp_name].difference(ec_to_exp[exp2])
                print "IN " + exp +" NOT "+exp2+": "+ str(len(different_ecs[comp_name]))
                #print difference
        print "total in "+exp+": "+str(len(ec_to_exp[exp]))
        print "unique to "+exp+": "+str(len(unique_ecs[exp]))
    print_dict_of_sets(out_file_name, different_ecs)

    
        # bna572_swissprot_represented = set()
        # bna572_ec = set()
        # if recs['ec_uniprot.tsv contigs_to_swissprot_top1.tsv']['bna572'] != '':
            # bna572_represented.add(recs['ec_uniprot.tsv contigs_to_swissprot_top1.tsv']['bna572'])
            # bna572_ec = bna572_ec.join(set(recs['ec_uniprot.tsv contigs_to_swissprot_top1.tsv']['EC'].split(' ')))
        # else:
            


def print_dict_of_sets(outfilename, indict):
    with open(outfilename, 'w') as out:
        for (key, set_) in indict.iteritems():
            for item in set_:
                out.write(str(key) + "\t" + str(item)+"\n")


if __name__ == '__main__':
    main(['metacyc_reactions_with_uniprot_from_Enzyme.tsv contigs_to_swissprot_top1.tsv','metacyc_reactions_with_uniprot_from_Enzyme.tsv high_quality_swissprot_hits.tsv','metacyc_reactions_with_uniprot_from_pathwaytools.tsv contigs_to_metacycUniprot_high_quality.tsv','metacyc_reactions_with_uniprot_from_pathwaytools.tsv contigs_to_metacycUniprot_top1.tsv'],['EC','MetaCyc'],'reactions_metacyc_uniprot.tsv',"ec_lists/metacyc_uniprot_differences.tsv")
    #main(['AraGEM.tsv contigs_to_tair_top1.tsv','AraGEM.tsv high_confidence_TAIR_hits.tsv','AraGEM.tsv contigs_to_swissprot_top1.tsv','AraGEM.tsv high_quality_swissprot_hits.tsv','a_thaliana_iRS1597.tsv contigs_to_tair_top1.tsv','a_thaliana_iRS1597.tsv high_confidence_TAIR_hits.tsv','a_thaliana_iRS1597.tsv contigs_to_swissprot_top1.tsv','a_thaliana_iRS1597.tsv high_quality_swissprot_hits.tsv','metacyc_reactions_with_uniprot_from_Enzyme.tsv contigs_to_swissprot_top1.tsv','metacyc_reactions_with_uniprot_from_Enzyme.tsv high_quality_swissprot_hits.tsv','metacyc_reactions_with_uniprot_from_pathwaytools.tsv contigs_to_swissprot_top1.tsv','metacyc_reactions_with_uniprot_from_pathwaytools.tsv high_quality_swissprot_hits.tsv'],['AraGEM','EC','MetaCyc','iRS1597'],'reactions_AraGEM_metacyc_iRS1597.tsv',"ec_lists/AraGEM_metacyc_iRS1597_EC_differences3.tsv")
    #main(experiments, annotations, reaction_mapping_file, out_file_name):
    #main(['AraCyc_GPR.tsv contigs_to_tair_top1.tsv','ec_uniprot.tsv contigs_to_swissprot_top1.tsv','full.tsv contigs_to_swissprot_top1.tsv','full.tsv contigs_to_tair_top1.tsv'], ['AraCyc','EC','Enzyme','bna572'], "contigs_to_bna572_full_2.tsv", "ec_lists/top1_EC_differences3.tsv")
    #main(['AraGEM.tsv contigs_to_tair_top1.tsv','AraGEM.tsv high_confidence_TAIR_hits.tsv','AraGEM.tsv contigs_to_swissprot_top1.tsv','AraGEM.tsv high_quality_swissprot_hits.tsv','a_thaliana_iRS1597.tsv contigs_to_tair_top1.tsv'], ['AraCyc','EC','Enzyme','bna572'], "contigs_to_bna572_full_2.tsv", "ec_lists/top1_EC_differences3.tsv")
    #main(['AraCyc_GPR.tsv high_confidence_TAIR_hits.tsv','ec_uniprot.tsv high_quality_swissprot_hits.tsv','full.tsv high_confidence_TAIR_hits.tsv','full.tsv high_quality_swissprot_hits.tsv'], ['AraCyc','EC','Enzyme','bna572'], "high_quality_contigs_to_bna572_full_.tsv", "ec_lists/high_quality_EC_differences3.tsv")