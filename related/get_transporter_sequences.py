import sqlite3
import pandas as pd
import pandas.io.sql as sql
import numpy as np
import re

con = sqlite3.connect('test.db')
blast2go = pd.read_table('blast2go_table_20130522_1342.txt')
blast2go = blast2go.dropna(subset=['Seq. Description'])
#print blast2go#['Seq. Name']

subset = blast2go[(blast2go['Seq. Description'].str.lower().str.contains('lipid transfer', na=False)) | (blast2go['Seq. Description'].str.lower().str.contains('abc transporter', na=False))]
with open("putative_abc_transporters_and_lipid_transfer.fasta", "w") as outfile:
    for (index, row) in subset.iterrows():
        name = row['Seq. Name']
        description = row['Seq. Description']
        length = row['Seq. Length']
        seq_hits = sql.read_frame("SELECT contigs.sequence, expression.TPM FROM contigs INNER JOIN expression ON contigs.name=expression.contig_name WHERE contigs.name='"+ name + "' AND expression.condition='young'", con)
        #seq_hits = sql.read_frame("SELECT contigs.sequence, expression.TPM FROM contigs INNER JOIN expression ON contigs.name=expression.contig_name WHERE contigs.name='"+ name + "'", con)
        sequence = ""
        found = 0
        tpm = 0
        for (row_num, seq_row) in seq_hits.iterrows():
            found += 1
            sequence = seq_row['sequence']
            tpm = seq_row['TPM']
        if found != 1:
            print "error on sequence " + name
        else:
            outfile.write(">" + name + " | " + description + " | length "  + str(length) + " | TPM " + str(tpm) +"\n")
            outfile.write(sequence+"\n\n")
subset.to_csv('putative_abc_transporters_and_lipid_transfer.tsv', sep='\t')
# hits = sql.read_frame("SELECT contig_name, subject FROM blast WHERE experiment='blastx_top5' AND database='TAIR' AND e_value=0 AND hit_order = 1", con)




# young_table = sql.read_frame("SELECT contig_name, TPM FROM expression WHERE condition='young'", con)
# mature_table = sql.read_frame("SELECT contig_name, TPM FROM expression WHERE condition='mature'", con)
# for (row_num, row) in hits.iterrows():
    # if not row['subject'] in young_expression:
        # young_expression[row['subject']] = 0
    # if not row['subject'] in mature_expression:
        # mature_expression[row['subject']] = 0
    # #print young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM']
    # #print type(young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM'])
    # young_expression[row['subject']] += young_table[young_table.contig_name == row['contig_name']].irow(0)['TPM']
    # mature_expression[row['subject']] += mature_table[mature_table.contig_name == row['contig_name']].irow(0)['TPM']
    # log2_change[row['subject']] = np.log2(mature_expression[row['subject']]/young_expression[row['subject']])

# pd.DataFrame({'young_TPM': young_expression, "mature_TPM": mature_expression, "log2_mature_young":log2_change}).to_csv("tair_good_hit_young_mature_expression.tsv", sep="\t")

