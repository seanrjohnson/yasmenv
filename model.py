from __future__ import print_function
import modelreaction
import pandas as pd
import numpy
import re
from collections import OrderedDict
from orderedset import OrderedSet
import math
from pprint import pprint as pp
from copy import deepcopy
import util
import pyparsing
import types
import operator


class Model(object):
    
    def __init__(self, reactions=list(), name = '', annotation_fields=OrderedSet(), correct_duplicate_names=False):
        '''
            name is name of model
            reactions is a list of ModelReactions
            
            you can add reactions, but you may not remove them!
            
            #TODO: why not literally use a pandas dataframe as the internal representation of a model? that would work great, I think.
        '''
        
        
        self.name = name
        
        self._reactions = list() #list of ModelReactions
        self._reaction_order = OrderedDict() #keys are reaction names (keys to self._reactions), values are the position of the reaction in self._reactions
        self._metabolites = OrderedDict() #ordered dict of metabolite names (values are the position of the metabolite within the dict)
        self._annotation_fields = OrderedSet(annotation_fields)
        for reaction in reactions:
            self.add_reaction(reaction, correct_duplicate_names=correct_duplicate_names)
        
        
    def __contains__(self, item):
        return item in self._reaction_order
    
    def __getitem__(self, index):
        return self._reactions[self._reaction_order[index]]
    
    def __iter__(self):
        return self._reaction_order.iterkeys()
    
    def copy(self):
        return deepcopy(self)
    
    def reaction_index(self, reaction_name):
        return self._reaction_order[reaction_name]
    
    def make_finite(self, smallest_lb=-1000, largest_ub=1000):
        '''
            returns a copy of the model, where any infinite reaction bounds have been converted into
            finite reaction bounds.
            
            any lower bound smaller than smallest_lb will be set to smallest_lb
            any upper bound larger than largest_ub will be set to largest_ub
        '''
        
        out_reactions = list()
        for rxn_name in self:
            new_rxn = self[rxn_name].copy()
            if new_rxn.lb < smallest_lb:
                new_rxn.lb = smallest_lb
            if new_rxn.ub > largest_ub:
                new_rxn.ub = largest_ub
            out_reactions.append(new_rxn)
        return Model(out_reactions)
    
    def make_infinite(self, lb_threshold=-1000, ub_threshold=1000):
        '''
            returns a copy of the model, where any reaction bounds equal to or beyond the thresholds have been converted into
            infinite reaction bounds.
            
            any lower bound smaller than or equal to smallest_lb will be set to -inf
            any upper bound larger than or equal to largest_ub will be set to +inf
        '''
        
        out_reactions = list()
        for rxn_name in self:
            new_rxn = self[rxn_name].copy()
            if new_rxn.lb <= lb_threshold:
                new_rxn.lb = float('-inf')
            if new_rxn.ub >= ub_threshold:
                new_rxn.ub = float('+inf')
            out_reactions.append(new_rxn)
        return Model(out_reactions)
    
    
    def add_annotation(self, reaction_name, annotation_field, annotation_value):
        '''
            adds an annotation to the given reaction (or changes the annotation of that field if the reaction already has one)
        '''
        if reaction_name in self:
            self._annotation_fields.add(annotation_field)
            self[reaction_name].annotations[annotation_field] = annotation_value
    
    def remove_annotation(self, annotation, inplace=False):
      '''
        removes the annotation field from all reactions in the model
        
        if inplace=True, does this to the reactions in self, otherwise returns a copy of self, with the change
      '''
      m = self
      if  not inplace:
        m = self.copy()
      
      new_annotation_fields = OrderedSet()
      for a in m._annotation_fields:
        if a != annotation:
          new_annotation_fields.add(a)
      m._annotation_fields = new_annotation_fields
    
      for r in m:
        if annotation in m[r].annotations:
          del m[r].annotations[annotation]
    
      return m
    
    def merge_annotations(self, annotation_1, annotation_2, sep=" ", inplace=False):
      '''
        merges the values from annotation_2 into annotation_1 and deletes annotation_2 from all reactions
        
        sep is the string that separates individual entries in the annotation field
        
        if inplace=True, does this to the reactions in self, otherwise returns a copy of self, with the change
        TODO: raise error if annotation_1 or annotation_2 is not in the model
      '''
      m = self
      if  not inplace:
        m = self.copy()
      for r in m:
        an1 = set()
        an2 = set()
        if annotation_1 in m[r].annotations:
          an1 = set(m[r].get_annotations(annotation_1, sep=sep))
        if annotation_2 in m[r].annotations:
          an2 = set(m[r].get_annotations(annotation_2, sep=sep))
        m[r].annotations[annotation_1] = sep.join(an1.union(an2))
      m.remove_annotation(annotation_2, inplace=True)
      
      return(m)
    
    def add_reaction(self, reaction, verbose=False, correct_duplicate_names=False):
        
        if not reaction.name in self._reaction_order:
            self._reaction_order[reaction.name] = len(self._reactions)
            self._reactions.append(reaction)
            self._annotation_fields |= set(reaction.annotations.keys())
            for an in self._annotation_fields:
                if an not in reaction.annotations:
                    reaction.annotations[an] = ""
            for side in (reaction.left, reaction.right):
                for metabolite in side:
                    if not metabolite in self._metabolites:
                        self._metabolites[metabolite] = len(self._metabolites)
        else:
            if correct_duplicate_names:
                #TODO: this will result in potentially unexpected behavior if adding the same reaction to multiple models (which I don't recommend)
                name = reaction.name
                #number = 0
                number = self.len_reactions()
                new_name = name + "_" + str(number)
                while new_name in self:
                    number += 1
                    new_name = name + "_" + str(number)
                    print(new_name)
                
                reaction.name = new_name
                
                self._reaction_order[reaction.name] = len(self._reactions)
                self._reactions.append(reaction)
                self._annotation_fields |= set(reaction.annotations.keys())
                for an in self._annotation_fields:
                    if an not in reaction.annotations:
                        reaction.annotations[an] = ""
                for side in (reaction.left, reaction.right):
                    for metabolite in side:
                        if not metabolite in self._metabolites:
                            self._metabolites[metabolite] = len(self._metabolites)
            elif verbose:
                print("Warning: reaction %s already in model" % reaction.name)
    
    def metabolite_names(self, compartments=None):
        '''
            returns a list of the names of the metabolites in the model
            
            if a list of compartment names is specified, returns all metabolites
            in those compartments
        '''
        out = list()
        
        if compartments is None:
            out = self._metabolites.keys()
        else:
            comps = set(compartments)        
            for met in self._metabolites.keys():
                if get_compartment(met) in comps:
                    out.append(met)
        return out
    
    def len_metabolites(self):
        return len(self._metabolites)
    
    def reaction_names(self):
        '''
            returns a list of the names of the reactions in the model
        '''
        return self._reaction_order.keys()
    
    def len_reactions(self):
        return len(self._reactions)
    
    def add_linked_metabolite(self, side1, side2, new_metabolite, linked_metabolite):
        '''
            adds a new metabolite to reactions if specified other metabolites are already present
            
            side1 and side2 are lists of metabolites.
            
            new_metabolite is the name of a metabolite to be added to the same side of the reaction as 
            the side2 metabolites
            
            linked_metabolite is the metabolite from side2 that the new metabolite should take the stoichiometry of
            
            returns a model where conservation is added to the same side as the side2 metabolites for 
            TODO: side1 and side2 can be dicts specifying stoichiometric ratios
            TODO: add support for executing on all compartments with one call
            
        '''
        
        out_reactions = list()
        side1 = set(side1)
        side2 = set(side2)
        for r in self.reactions_list():
            r = r.copy()
            net = r.net()
            left_mets = set(net.metabolites('left'))
            right_mets = set(net.metabolites('right'))
            
            if side1.intersection(left_mets) == side1 and side2.intersection(right_mets) == side2:
                r.right[new_metabolite] = net.right[linked_metabolite]
            
            elif side1.intersection(right_mets) == side1 and side2.intersection(left_mets) == side2:
                r.left[new_metabolite] = net.left[linked_metabolite]
            
            out_reactions.append(r)
        return Model(out_reactions)
    
    def sparse_matrix(self, style='MRS'):
        '''
            returns a list of 3-tuples of the form with the metabolite_index, reaction_index, and stoichiometric_coefficient
                There are different possible arrangements of these numbers in the tuple. Indicate which you want with the 'style'
                parameter.  Where M is the metabolite position, R is the reaction position, and S is the stoichiometric coefficient
                
                pyglpk expects values to be in the order 'MRS' for the S matrix of an FBA problem.
                cvxopt expects 'SMR'
                
            in terms of the S matrix in an FBA problem,
            (M, R, S) == (row, column, value)
            for left side metabolites, the stoichiometric coefficient is multiplied by -1
            
        '''
        
        m = re.search('[^RMS]', style)
        if m or (len(style) != 3):
            raise "unnacceptable style string in Model.sparse_matrix: %s" % style
        out = list()
        for (rxn_order, rxn) in enumerate(self._reactions):
            mets = dict()
            for (met_name, coeff) in rxn.left.iteritems():
                mets[met_name] = mets.get(met_name, 0.0) + -1. * coeff
            
            for (met_name, coeff) in rxn.right.iteritems():
                mets[met_name] = mets.get(met_name, 0.0) + 1. * coeff
            for (met, coeff) in mets.iteritems():
                data= {'M': self._metabolites[met], 'R': rxn_order, 'S': coeff}
                if coeff != 0.0:
                    entry = list()
                    for x in style:
                        entry.append(data[x])
                    out.append(tuple(entry))
        return out
    
    def sum_reactions(self, reactions, fluxes=None, name=None, lb=float("-inf"), ub=float("inf"), objective=0, annotations=OrderedDict()):
        '''
          returns a reaction that is the sum of the provided reactions.
          input: 
              reactions: a list of reaction names
              fluxes: a list of fluxes of the given reactions. If not provided, then each reaction is expected to have a flux of 1.
                      if fluxes is provided, but has a different length than reactions, an exception is thrown.
        '''
        net_dict = dict()
        model = self
        if fluxes is not None and len(fluxes) != len(reactions):
          raise ValueError("reactions and fluxes are not the same size")
        for (i, r) in enumerate(reactions):
            
            coefficient = 1.0
            if fluxes is not None:
                coefficient = fluxes[i]
            for met, stoich in model[r].stoich_dict().iteritems():
                net_dict[met] = net_dict.get(met, 0.0) + coefficient * stoich
        left = {x:abs(y) for x,y in net_dict.iteritems() if y < 0}
        right = {x:y for x,y in net_dict.iteritems() if y > 0}
        rxn = modelreaction.ModelReaction(name, left, right, lb, ub, objective, annotations)
        return rxn
            
    def compartment_net(self, compartment, flux_field=None):
        '''
            returns as a reaction the sum of the reactions in a given compartment
        '''
        m_rxns = list()
        m_fluxes = list()
        for r in self.reactions_list():
            if compartment in r.compartments():
                m_rxns.append(r.name)
                if flux_field is not None:
                    m_fluxes.append(float(r.annotations[flux_field]))
        #print(m_rxns)
        #print(m_fluxes)
        if flux_field is None:
            m_fluxes = None
        compartment_sum = self.sum_reactions(m_rxns, m_fluxes)
        #print(compartment_sum.as_str())
        return compartment_sum
      
    def reaction_column_coefficients(self):
        '''
            return a dict of dicts of floats
            
            first key is reaction name
            second key is metabolite name
            value is stoichiometry of the metabolite in the reaction
            positive if on the right, negative if on the left
        '''
        out = dict()
        for (rxn_order, rxn) in enumerate(self._reactions):
            mets = dict()
            net_dict = dict()
            for (met_name, coeff) in rxn.left.iteritems():
                mets[met_name] = mets.get(met_name, 0.0) + -1. * coeff
            
            for (met_name, coeff) in rxn.right.iteritems():
                mets[met_name] = mets.get(met_name, 0.0) + 1. * coeff
            for (met, coeff) in mets.iteritems():
                if coeff != 0.0:
                    net_dict[met] = coeff
                    
            out[rxn.name] = net_dict
        return out
    
    
    def reactions_list(self):
        return self._reactions
    
    def param_list(self, param):
        '''
          returns an ordered list of the given parameter in the model
        '''
        out = list()
        for r in self:
          out.append(getattr(self[r], param))
        return out
        
        
    def find_singlets(self):
        '''
            returns a list of metabolites that are involved in exactly 1 reaction in the model
        '''
        out = list()
        for (metab, count) in self.metabolite_connectivity().iteritems():
            if count == 1:
                out.append(metab)
        return out
    
    def metabolite_connectivity(self):
        '''
            returns an orderedDict of metabolites where the key is the name and the value is the number of reactions it is associated with
        '''
        out = OrderedDict()
        for metab in self._metabolites:
            count = 0
            for rxn in self._reactions:
                if metab in rxn.left:
                    count += 1
                elif metab in rxn.right:
                    count += 1
            out[metab] = count
        return out
        
    # def to_sbml(self,filename):
        # pass
    
    # def to_excel(self, filename):
        # pass
    
    def merge_in(self, other):
        '''
            adds all of the reactions from the model other, to this model.
        '''
        for rxn in other:
            self.add_reaction(other[rxn])
    
    def make_irreversible(self):
        '''
            converts a reversible model to a model where all bounds are 0 or positive
            returns the new model and a dict mapping the new reaction names to reaction names from the old model, 
                and the relative direction of the new reaction, -1 if the new reaction is in the opposite direction, 
                1 if the same.
        '''
        rxn_map = OrderedDict() # keys are reaction names in the new model, values are the corresponding reaction from the old model
        irrev = list() # a list of irreversible reactions
        for reaction in self.reactions_list():
            #__init__(self, name, left_side, right_side, lb, ub, objective, expression=None):
            (forward_bounds, reverse_bounds) = reaction.split_bounds()
            
            f = modelreaction.ModelReaction(reaction.name + "_L_to_R", reaction.left, reaction.right, forward_bounds[0], forward_bounds[1], reaction.objective, reaction.annotations)
            r = modelreaction.ModelReaction(reaction.name + "_R_to_L", reaction.right, reaction.left, reverse_bounds[0], reverse_bounds[1], -1*reaction.objective, reaction.annotations)
            
            rxn_map[reaction.name + "_L_to_R"] = dict()
            rxn_map[reaction.name + "_L_to_R"]['name'] = reaction.name
            rxn_map[reaction.name + "_L_to_R"]['direction'] = 1
            rxn_map[reaction.name + "_R_to_L"] = dict()
            rxn_map[reaction.name + "_R_to_L"]['name'] = reaction.name
            rxn_map[reaction.name + "_R_to_L"]['direction'] = -1
            
            irrev.append(f)
            irrev.append(r)
        # for rxn in irrev:
            # print "%s %f %f" % (rxn.name, rxn.lb, rxn.ub)
        return (Model(irrev), rxn_map)
    
    def decompartmentalize(self, preserve=list(), decompartmentalize_names=False):
        '''
            returns a copy of the model, with compartment designations removed from metabolites
            
            if preserve is supplied, then compartments are not removed from metabolites whose compartment is in preserve
            
            Precondition: Assumes that compartments are designated by a series of characters in brackets at the end of a metabolite name
        '''
        
        decomp = list()
        preserve = set(preserve)
        
        for rxn in self.reactions_list():
            decomp.append(decompartmentalize_reaction(rxn, preserve, decompartmentalize_names))
            
        return Model(decomp)

    
    def merge_metabolites(self, met_dict, all_compartments=False):
        '''
            returns a model with metabolites exchanged according to met_dict
            in most cases this model should then be cleaned with the clean method
            
            This method is similar to rename_metabolites but depending on the situation, one or the other may be more convenient.
            
            met_dict: dict where keys are new metabolite names, and values are lists of old metabolite names.
            from the model, any metabolite with a name in one of the values will be replaced the corresponding key name.
            Any given metabolite should appear under at most one key
        '''
        
        met_key = dict()
        out_model = Model()
        
        for (new_met, old_mets) in met_dict.iteritems():
            for old_met in old_mets:
                met_key[old_met] = new_met
        
        compartment = ""
        for reaction in self.reactions_list():
            new_rxn = reaction.copy()
            for side in ['left','right']:
                side_members = getattr(new_rxn, side)
                for met in side_members.keys():
                    if (all_compartments):
                        compartment = get_compartment(met, default = "")
                        processed_met = remove_compartment(met)
                        if compartment != "":
                            compartment = "[" + compartment + "]"
                    else:
                        processed_met = met
                    if processed_met in met_key:
                        stoich = side_members[met]
                        del side_members[met]
                        side_members[met_key[processed_met] + compartment] = side_members.get(met_key[processed_met] + compartment, 0) + stoich
            out_model.add_reaction(new_rxn)
        return out_model
        
    
    def clean(self, merge=True, merge_args={'metabolites_merge':max}):
        '''
            returns a copy of the model with various inconsistencies removed
                also a map of reaction names from the original model to the corresponding reactions in the new model
            
            cleaning includes (in this order):
                convert reactions to net reactions
                    if a metabolite is on both sides of a reaction, the side with the greater stoichiometry will keep it with stoichiometry reduced by the other side.  
                    if stoichiometry is equal on both sides, the metabolite will be removed from both sides
                    if a metabolite has a stoichiometry of zero, it will be removed
                
                remove reactions with no metabolites
                
                if merge=True, merge reactions with identical metabolites (if their bounds overlap),
                    mergs_args is a dict of args passed to the merge function when merging reactions
        '''

        start_list = list()
        end_list = list()
        
        #log = list()
        
        reaction_map = dict() #keys are reaction names from input, values are the corresponding reaction names in the output (i.e. the reaction that the original reaction was merged into)

        start_list = self.reactions_list()
        
        # convert reactions to net reactions
        # if a metabolite is on both sides of a reaction, the side with the greater stoichiometry will keep it with stoichiometry reduced by the other side.  
        # if stoichiometry is equal on both sides, the metabolite will be removed from both sides
        for rxn in start_list:
             end_list.append(rxn.net())
        start_list = end_list
        end_list = list()
        
        #remove reactions with no metabolites
        for rxn in start_list:
            if ((len(rxn.left) > 0) or (len(rxn.right) > 0)):
                end_list.append(rxn)
            else:
                reaction_map[rxn.name] = None
        start_list = end_list
        end_list = list()
        
        #if merge=True, merge reactions with identical metabolites (if their bounds overlap)
        #assumes no duplicate reaction names
        if merge == True:
            #compare every reaction to every other reaction to generate sets of reactions that are the same or reverse
            grouped = set() # a set of reaction names of reactions that have been added to a group
            groups = dict() # a dict of sets of dicts of reactions and directions. Members of the same set have the same stoichiometry (or exactly reversed stoichiometry)
            for i in range(len(start_list)):
                if start_list[i].name in grouped:
                    continue
                #else:
                grouped.add(start_list[i].name)
                groups[start_list[i].name] = list([{'rxn': start_list[i], 'dir': 1.}])
                for i2 in range(i+1, len(start_list)):
                    cmp = start_list[i].compare(start_list[i2])
                    if 'forward' in cmp or 'reverse' in cmp:
                        if ('overlap' in cmp) and ('surrounded' not in cmp):
                            # the idea here is that we merge overlapping reactions without destroying fixed fluxes
                            grouped.add(start_list[i2].name)
                            if 'forward' in cmp:
                                groups[start_list[i].name].append({'rxn': start_list[i2], 'dir': 1.})
                            else:
                                groups[start_list[i].name].append({'rxn': start_list[i2].reversed(), 'dir': -1.})

            #for every set, merge reactions with overlapping bounds, keep track of which reaction from the original model was merged into which in the output
            #pp(groups)
            for g in groups.itervalues():
                (l, mapped) = merge_reactions(g, net=False, **merge_args)
                end_list.extend(l)
                reaction_map.update(mapped)
            
        #return {'model': Model(end_list), 'map': reaction_map}
        return (Model(end_list), reaction_map)
       
    def remove_metabolites(self, to_remove, all_compartments=False):
        '''
            returns a copy of the model with all metabolites in the to_remove list absent
            
            If all_compartments = True, then metabolite names in the model will have any 
            terminal bracketed string removed before being checked against to_remove
            
            It is sometimes desirable to run the 'clean' method afterwards to give a model without empty reactions
        '''
        removed = list()
        to_remove = set(to_remove)
        
        for rxn in self.reactions_list():
            l = dict() #new left side
            r = dict() #new right side
            for (met, stoich) in rxn.left.iteritems():
                if (all_compartments):
                    processed_met = remove_compartment(met)
                else:
                    processed_met = met
                if processed_met not in to_remove:
                    l[met] = l.get(met, 0.) + stoich
            for (met, stoich) in rxn.right.iteritems():
                if (all_compartments):
                    processed_met = remove_compartment(met)
                else:
                    processed_met = met
                if processed_met not in to_remove:
                    r[met] = r.get(met, 0.) + stoich
            #def __init__(self, name, left_side, right_side, lb, ub, objective, expression=None):
            removed.append(modelreaction.ModelReaction(rxn.name, l, r, rxn.lb, rxn.ub, rxn.objective, rxn.annotations))
        return Model(removed)
    

    def remove_reactions_with_metabolites(self, to_remove, all_compartments=False):
        '''
            returns a copy of the model with reactions removed that have at least one 
            product or reactant in the the to_remove list
            
            If all_compartments = True, then metabolite names in the model will have any 
            terminal bracketed string removed before being checked against to_remove
        '''
        removed = list()
        to_remove = set(to_remove)
        for rxn in self.reactions_list():
            l = dict() #new left side
            r = dict() #new right side
            keep = True
            for (met, stoich) in rxn.left.iteritems():
                if (all_compartments):
                    met = remove_compartment(met)
                if met in to_remove:
                    keep = False
            for (met, stoich) in rxn.right.iteritems():
                if (all_compartments):
                    met = remove_compartment(met)
                if met in to_remove:
                    keep = False
            if keep:
                removed.append(rxn.copy())
        return Model(removed)
    
    def to_tsv(self, filename, sep='\t', name_col='Rxn name', lb_col='LB', ub_col='UB', objective_col='Objective', formula_col='Formula', output_annotations=True, equals_sign='<=>'):
        '''
          TODO: add column order for some or all fields
        '''
    
        out = list() # a list of dicts.  Each list item is a row (a reaction) and each entry in the dict is a column
        annotation_cols = [x for x in self._annotation_fields]
        for rxn in self.reactions_list():
            d = dict()
            d[name_col] = rxn.name
            
            if (rxn.lb == float('-inf')):
                d[lb_col] = '(-)inf'
            else:
                d[lb_col] = rxn.lb
            
            if (rxn.ub == float('inf')):
                d[ub_col] = '(+)inf'
            else:
                d[ub_col] = rxn.ub
            
            d[objective_col] = rxn.objective
            d[formula_col] = rxn.as_str(equals_sign=equals_sign)
            if output_annotations:
                for col_name in annotation_cols:
                    if col_name in rxn.annotations:
                        d[col_name] = rxn.annotations[col_name]
                    else:
                        d[col_name] = ""
            out.append(d)

                    
        
        df = pd.DataFrame(out)
        cols = [name_col, formula_col, lb_col, ub_col, objective_col]
        cols += sorted(annotation_cols)
        
        df = df.reindex_axis(cols, axis=1)
        #df.to_csv(filename, sep=sep, cols=cols, index=False)
        df.to_csv(filename, sep=sep, index=False, columns=cols)
    
    def to_sif(self, filename, ignore=list(), fluxes=[], ignore_compartmentalization=False):
        '''
            writes model to a sif file.
            skips any reaction or metabolite if its name is in the ignore list
            fluxes is a list of 2-tuples of (reaction_name, flux) if supplied, then
              for reactions with positive flux, the reactants are listed before the 
                reaction name, and the products after. Vice versa for reactions with
                negative flux
            if ignore_compartmentalization=True, then the metabolites in ignore list are ignored in all compartments
        '''
        ignore = set(ignore)
        flux_dict = {}
        for t in fluxes:
            if len(t) > 1:
                flux_dict[t[0]] = t[1]
        with open(filename,'w') as outfile:
            for rxn in self.reactions_list():
                if rxn.name not in ignore:
                    lhs = "left"
                    rhs = "right"
                    if rxn.name in flux_dict and flux_dict[rxn.name] < 0:
                        lhs = "right"
                        rhs = "left"
                    for (met, stoich) in getattr(rxn,lhs).iteritems():
                        igmet = met
                        if ignore_compartmentalization:
                            igmet = remove_compartment(met)
                        if igmet not in ignore:
                            outfile.write(met + "\t" + "cr" + "\t" + rxn.name + "\n")
                    for (met, stoich) in getattr(rxn,rhs).iteritems():
                        igmet = met
                        if ignore_compartmentalization:
                            igmet = remove_compartment(met)
                        if igmet not in ignore:
                            outfile.write(rxn.name + "\t" + "rc" + "\t" + met + "\n")
    def to_COBRA(self):
        '''
            converts the model into a COBRA for Python model
            
            requires COBRA for Python to be installed
            
            This is pretty minimalistic at the moment. 
            It could potentially be expanded to handle more details,
            such as compartmentation, molecular formulas, gene associations etc.
        '''
        import cobra
        from cobra import Model as CobraModel
        from cobra import Reaction as CobraReaction
        from cobra import Metabolite as CobraMetabolite
        
        
        
        cobra_mets = OrderedDict()
        for met in self.metabolite_names():
            #print met
            cobra_mets[met] = CobraMetabolite(util.sbmlify_id(met), compartment='c')
        cobra_model = CobraModel(self.name)
        #print cobra_model
        for rn in self:
            rxn = CobraReaction(util.sbmlify_id(rn))
            rxn.name = rn #in COBRA, this field is more for a description, so some other yasmenv field may be more appropriate here
            #rxn.subsystem = 
            rxn.lower_bound = self[rn].lb #TODO: how to handle inf?
            rxn.upper_bound = self[rn].ub 
            if self[rn].lb == 0 or self[rn].ub == 0:
                rxn.reversibility = 0
            else:
                rxn.reversibility = 1
            objective_coefficient = self[rn].objective
            
            stoich_dict = {cobra_mets[met]:stoich for (met, stoich) in self[rn].stoich_dict().iteritems()}
            #pp(stoich_dict)
            
            rxn.add_metabolites(stoich_dict)
            cobra_model.add_reaction(rxn)
            #print rxn.reaction
        #print cobra_model
        return cobra_model
        
class GraParser(object):
    def __init__(self, or_strings=('OR','or','|'), and_strings=('AND','and','&'), multiply_strings=('*')):
        self.or_strings = or_strings
        self.and_strings = and_strings
        self.multiply_strings = multiply_strings
        self.gr_parse = self.GeneReactionAssociationExpressionSyntax()
        
    @staticmethod
    def divide(denom, num):
        '''
            input: two numbers
            output: if denom is 0 output nan, if denom is nan output num, otherwise output num/denom
        '''
        if math.isnan(denom):
            out = num
        elif denom == 0:
            out = float('nan')
        else:
            out = float(num)/float(denom)
        return out
    
    @staticmethod
    def expression_and(left, right):
        '''
            input: two numbers
            output: if one is nan, output the other one. If neither is nan, output the smaller one
        '''
        if math.isnan(left):
            out = float(right)
        elif math.isnan(right):
            out = float(left)
        else:
            out = float(min(left, right))
        return out

    @staticmethod
    def expression_or(left, right):
        '''
            input: two numbers
            output: if one is nan, output the other one. If neither is nan, output the sum
        '''
        if math.isnan(left):
            out = float(right)
        elif math.isnan(right):
            out = float(left)
        else:
            out = float(operator.add(left, right))
        return out


    def expr_val(self, gra_stack, expression_dict, ignore_multipliers=True, return_gene_names=False):
        '''
          input:
            gra_stack: a list of lists, such as the kind produced by the parser generated by GeneReactionAssociationExpressionSyntax
              for example: [[['4', '*', 'AT3G22960'],  'AND',  ['4', '*', ['AT5G52920', 'OR', 'AT1G32440']]]]
            
            expression_dict: keys are gene names, values are numbers representing expression level
              for example: {'AT3G22960': 10, 'AT5G52920': 0.2, 'AT1G32440': 1}
            
            ignore_multipliers: if True, bare numbers and '*' signs will be ignored and not used in the reaction expression level calculation.
              if False, expression levels will be divided by the multipliers, representing the fact that more units are needed for activity
            
            
            NOTE: if multipliers follow the genes they are modifying, and ignore_multipliers is False, expression will be calculated incorrectly
              because the multiplier will be divided by gene expression instead of vice versa.
              
            NOTE2: if a gene identifier is not recognized, it will be ignored, such that when it is part of an "AND" operation, the other one will be used.
            
            
          output:
            a number representing the expression of the reaction
            
        '''
        op = None #operator, a string: 'OR', 'AND', or '*'
        lhs = None #a number, the left-hand-side of the operator
        ops = dict() #dict where keys are the valid operators and values are the functions they represent
        
        for s in self.or_strings:
            ops[s] = self.expression_or
        for s in self.and_strings:
            ops[s] = self.expression_and
        for s in self.multiply_strings:
            ops[s] = self.divide
        
        if isinstance(gra_stack, types.StringTypes): #gra_stack is a string and not a list. This is our base case. gra_stack should never be just an operator
            if self.isnum(gra_stack): #it's a number string
                lhs = float(gra_stack) #convert the string to a number
            elif gra_stack in expression_dict: #it's a gene name that we have an expression value for
                lhs = expression_dict[gra_stack] #get the expression level from expression_dict
            else: #it's something we don't recognize
                lhs = float('nan')  #TODO: it may be a good idea to have a set list of unknowns, and then if an identifier isn't in that list, to throw an error
        else: #gra_stack is a list
            for x in xrange(len(gra_stack)): #iterate through the stack
                if isinstance(gra_stack[x], types.StringTypes) and gra_stack[x] in ops: #the next item in the stack is an operator
                    if op is None and lhs is not None: #we've seen an expression already for lhs, and we haven't seen an operator yet
                        op = gra_stack[x] #remember the operator
                    else: #the input was malformed either two operators in a row, or an operator without anything to the left of it
                        print(op + " " + lhs)
                        print("error parsing boolean expression at line %s" % repr(gra_stack[x]), file=sys.stderr)
                else: #the next item in the stack is a number, a gene, or a list
                    if lhs is None:
                        lhs = self.expr_val(gra_stack[x], expression_dict, ignore_multipliers) #recursively evaluate the expression and remember the result
                    elif op is None: #if we've seen an lhs, but have not seen an operator and now we're seeing another expression, it's an error.
                        print("error parsing boolean expression %s" % repr(gra_stack[x]), file=sys.stderr)
                    else:
                        if op in self.multiply_strings and ignore_multipliers: #ignore_multipliers is active, and we encounter a multiplication
                            lhs = self.expr_val(gra_stack[x], expression_dict, ignore_multipliers) # ignore the the left hand side (presumably the bare number) and evaluate the right hand side
                        else: #we've seen a valid operator and have a number in the lhs
                            lhs = ops[op](lhs, self.expr_val(gra_stack[x], expression_dict, ignore_multipliers)) # apply the operator, using lhs
                        op = None
        out = lhs
        return out
    
    def gra_genes(self, gra_stack, gene_names_set):
        '''
          input:
            gra_stack: a list of lists, such as the kind produced by the parser generated by GeneReactionAssociationExpressionSyntax
              for example: [[['4', '*', 'AT3G22960'],  'AND',  ['4', '*', ['AT5G52920', 'OR', 'AT1G32440']]]]
            
            gene_names_set: a set of gene names already associated with the gra
            
            
          output:
            gene_names_set
          side_effect: gene_names_set is modified to contain the genes from the GRA
              (this function essentially returns the same thing in two different ways)
        '''
        op = None #operator, a string: 'OR', 'AND', or '*'
        lhs = None #a number, the left-hand-side of the operator
        ops = dict() #dict where keys are the valid operators and values are the functions they represent
        gene_names_stack = list()
        
        for s in self.or_strings:
            ops[s] = self.expression_or
        for s in self.and_strings:
            ops[s] = self.expression_and
        for s in self.multiply_strings:
            ops[s] = self.divide
        
        
        if isinstance(gra_stack, types.StringTypes): #gra_stack is a string and not a list. This is our base case. gra_stack should never be just an operator
            if self.isnum(gra_stack) or gra_stack in ops:
                pass #we don't care, it's not a gene
            else: #it's a gene
                gene_names_set.add(gra_stack)
        else: #gra_stack is a list
            for x in xrange(len(gra_stack)): #iterate through the stack
                self.gra_genes(gra_stack[x], gene_names_set)

        return gene_names_set

    @staticmethod
    def isnum(s):
      '''
          checks if a string is a number of the form:
          123 or 134.451 or .4134 etc.
          returns True if it's a number, else False
      '''
      out = False
      if re.match(r'^[0-9]*(\.[0-9]*)?$', s):
          out = True
      return out
    
    def GeneReactionAssociationExpressionSyntax(self):
        gene = pyparsing.Word(pyparsing.alphas, pyparsing.alphanums+"_.-")
        number = pyparsing.Word(pyparsing.nums) | pyparsing.Combine(pyparsing.Word(pyparsing.nums), pyparsing.Literal("."), pyparsing.Word(pyparsing.nums)) | pyparsing.Combine(pyparsing.Literal("."), pyparsing.Word(pyparsing.nums)) # 123 or 134.451 or .4134 etc.
        
        for (i, s) in enumerate(self.or_strings):
            if i == 0:
                or_str = pyparsing.Literal(s)
            else:
                or_str = or_str | pyparsing.Literal(s)
        
        for (i, s) in enumerate(self.and_strings):
            if i == 0:
                and_str = pyparsing.Literal(s)
            else:
                and_str = and_str | pyparsing.Literal(s)
        
        for (i, s) in enumerate(self.multiply_strings):
            if i == 0:
                multiply_str = pyparsing.Literal(s)
            else:
                multiply_str = multiply_str | pyparsing.Literal(s)
        
        atom = gene | number
        expr = pyparsing.infixNotation(atom, [(multiply_str, 2, pyparsing.opAssoc.RIGHT), (and_str, 2, pyparsing.opAssoc.LEFT), (or_str, 2, pyparsing.opAssoc.LEFT)])
        return expr
    
    
    def get_reaction_expression(self, gra_string, expression_dict):
        out = 0
        if gra_string != "-":
            gra = self.gr_parse.parseString(gra_string).asList()
            out = self.expr_val(gra, expression_dict)
        return out
    
    def get_genes_from_bool(self, gra_string):
        out = set()
        mt = dict()
        if gra_string != "-":
            gra = self.gr_parse.parseString(gra_string).asList()
            #print(gra)
            new_set = set()
            #print(self.expr_val(gra, mt, return_gene_names=True))
            new_genes = self.gra_genes(gra, new_set)
            out |= new_set
        return out

def get_compartment(name, default=None):
    '''
        Given a string of the form .+\[[^\[]+]$
        (in other words, an arbitrary sequence of characters followed by '[' followed by an arbitrary sequence of characters not including '[', followed by ']' and then immediately by the end of the string
        
        default is what to return if no compartment is found
        
        return the compartment (without the brackets) (None if there is no compartment)
    '''
    compartment_regex = re.compile('^(.+)\[([^\[]+)\]$')
    m = compartment_regex.search(name)
    out = default
    if m:
        out = m.group(2)
    return out

def remove_compartment(name):
    '''
        Given a string of the form .+\[[^\[]+]$
        (in other words, an arbitrary sequence of characters followed by '[' followed by an arbitrary sequence of characters not including '[', followed by ']' and then immediately by the end of the string
        
        return the metabolite name without the compartment
    '''
    compartment_regex = re.compile('^(.+)\[([^\[]+)\]$')
    m = compartment_regex.search(name)
    out = name
    if m:
        out = m.group(1)
    return out

def add_compartment(name,compartment):
    '''
        given a metabolite name and a compartment
        returns the name[compartment]
    '''
    if '[' in compartment:
        print('warning: illegal character "]" in compartment name %s. The offending character has been removed' % compartment)
    compartment = compartment.replace('[', '')
    out = name + '[' + compartment + ']'
    return out

def from_sbml(filename, correct_duplicate_names=False, annotations_sep = " ", convert_compartments_to_bracket_style=True):
    '''
        reads an sbml level 2 version 1 file and outputs a yasmenv model
        
        returns a Model object
    
    '''
    import sbml
    
    (sbml_reactions, out_metabolites, out_compartments) = sbml.read_sbml(filename, convert_compartments_to_bracket_style=convert_compartments_to_bracket_style)
    reactions = list()
    for (name, rxn) in sbml_reactions.iteritems():
      annotations = OrderedDict()
      annotations['Description'] = rxn['name']
      left = rxn['left']
      right = rxn['right']
      annotations['reversible'] = rxn['reversible']
      lb = 0
      ub = 0
      obj = 0
      expression = 0
      #print(rxn[u'properties'])
      for (property, value) in rxn[u'properties']:
        if property == "LOWER_BOUND":
          lb = value
        elif property == "UPPER_BOUND":
          ub = value
        elif property == "OBJECTIVE_COEFFICIENT":
          obj = value
        else:
          if property not in annotations:
            annotations[property] = value
          else:
            annotations[property] += annotations_sep + value
      reactions.append(modelreaction.ModelReaction(name, left, right, lb, ub, obj, expression, annotations=annotations))
    return Model(reactions, correct_duplicate_names=correct_duplicate_names)
    
def from_tsv(filename, sep='\t', name_col='Rxn name', lb_col='LB', ub_col='UB', objective_col='Objective', formula_col='Formula', expression_col=None, equals_signs=['<=>','<==>', '->'], correct_duplicate_names=False):
    '''
        reads a delimited text file defining a stoichiometric model
        expression_col is deprecated. Expression is now just treated like any other annoation
        
        returns a Model object
    
    '''
    file  = pd.read_csv(filename, sep=sep, index_col=False)  #TODO: maybe replace this with the csv reader from the standard library eventually
    
    named_cols = OrderedSet([name_col, lb_col, ub_col, objective_col, formula_col])
    
    reactions = list()
    
    annotation_columns = OrderedSet(file.columns.tolist()) - named_cols
    
    for (index, row) in file.iterrows():
        
        try:
            lb = float(row[lb_col])
        except:
            if (row[lb_col] == '(-)inf'):
                lb = float('-inf')
            else:
                raise Exception("can't parse lower bound: %s" % row[lb_col])
        
        try:
            ub = float(row[ub_col])
        except:
            if (row[ub_col] == '(+)inf'):
                ub = float('inf')
            else:
                raise Exception("can't parse upper bound: %s" % row[ub_col])
        
        
        obj = float(row[objective_col])
        #TODO: another way to prevent errors from invalid objective values in the input may be to use df.fillna
        if math.isnan(obj):
            obj = 0.0
        
        
        name = str(row[name_col])
        (left, right) = (None, None)
        try:
            (left, right) = _parse_formula(str(row[formula_col]), equals_signs)
        except:
            print("Error line " + str(index))
            print(str(row[formula_col]))
        
        annotations = OrderedDict()
        for column_name in annotation_columns:
            if pd.isnull(row[column_name]):
                annotations[column_name] = ""
            else:
                annotations[column_name] = str(row[column_name])
        
        
        reactions.append(modelreaction.ModelReaction(name, left, right, lb, ub, obj, annotations=annotations))
    return Model(reactions, annotation_fields = annotation_columns, correct_duplicate_names=correct_duplicate_names)

def from_multiple(filename, **args):
    '''
        reads a file that lists other filenames, one per line.
        Then reads reactions from the files listed and combines them into a model
    '''
    file_names = list_from_file_from_multiple(filename)
    combined_model = Model()
    for name in file_names:
        #print name
        tmp_model = from_tsv(name, **args)
        for rxn in tmp_model:
            combined_model.add_reaction(tmp_model[rxn])
    return combined_model

def list_from_file_from_multiple(filename, comment_symbol=None):
    '''
        opens a file returns each non-blank line as a member in a list
        
        if a comment_symbol is specified, ignore lines where the first non-whitespace characters are comment_symbol
    '''
    out = list()
    combined_model = Model()
    with open(filename, 'r') as listfile:
        for line in listfile:
            line = line.strip()
            if line != "" and (comment_symbol is None or not line.startswith(comment_symbol)):
                out.append(line)
    return out

def decompartmentalize_reaction(rxn, preserve=list(), decompartmentalize_name=False):
    l = dict() #new left side
    r = dict() #new right side
    for (met, stoich) in rxn.left.iteritems():
        if get_compartment(met) not in preserve:
            met = remove_compartment(met)
        l[met] = l.get(met, 0.) + stoich
    for (met, stoich) in rxn.right.iteritems():
        if get_compartment(met) not in preserve:
            met = remove_compartment(met)
        r[met] = r.get(met, 0.) + stoich
    #def __init__(self, name, left_side, right_side, lb, ub, objective, expression=None):
    name = rxn.name
    if decompartmentalize_name:
        name = remove_compartment(rxn.name)
    return modelreaction.ModelReaction(name, l, r, rxn.lb, rxn.ub, rxn.objective, rxn.annotations)

def compartmentalize_reaction(rxn, compartment):
    l = dict() #new left side
    r = dict() #new right side

    for (met, stoich) in rxn.left.iteritems():
        new_met = add_compartment(met, compartment)
        l[new_met] = l.get(new_met, 0.) + stoich
    for (met, stoich) in rxn.right.iteritems():
        new_met = add_compartment(met, compartment)
        r[new_met] = r.get(new_met, 0.) + stoich
    #def __init__(self, name, left_side, right_side, lb, ub, objective, expression=None):
    return modelreaction.ModelReaction(rxn.name, l, r, rxn.lb, rxn.ub, rxn.objective, deepcopy(rxn.annotations))

    
def _parse_formula(formula, equals_signs):
    '''
    
    
    '''
    lr = halve_reaction(formula, equals_signs)
    (left, right) = (parse_reaction_half(x) for x in lr)
    
    for met in left.copy():
        if met in right:
            #print "warning: metabolite %s on both sides of reaction %s <===> %s" % (met, lr[0], lr[1])
            diff = right[met] - left[met]
            
            if diff == 0:
                del right[met]
                del left[met]
            elif diff < 0:
                left[met] = -diff
                del right[met]
            else:
                right[met] = diff
                del left[met]
    
    return (left, right)

def parse_reaction_half(string):
    '''
        
    '''
    string = string.strip()
    terms = string.split(' + ')
    terms = [x.strip() for x in terms]
    out = dict()
    if string != "": #return an empty dict if it's a blank string
        for term in terms:
            term = term.strip()
            
            stoich = 1.
            reactant = None
            #see if the term begins with a number
            match = re.search('^([0-9]*(\.[0-9]+)? )(.+)', term)
            if match:
                stoich = float(match.group(1))
                reactant = match.group(3)
            else:
                if term != "" and not re.search('^[0-9]*(\.[0-9]+)?$', term):
                    reactant = term
                else:
                    raise Exception('cannot parse reaction half: ' + string)
            if (not reactant in out):
                out[reactant] = 0.
            out[reactant] += stoich

    return out
    
def halve_reaction(string, original_splitters):
    '''
        string: a string
        splitters: a list of strings
        splits the string in half based at any one of the strings in splitters
        if there are more than one possible breakpoints, raises an exception
    '''
    string = string.strip()
    splits = 0
    halves = list()
    splitters = list()
    for i in range(len(original_splitters)):
         splitter = re.escape(original_splitters[i])
         splitters.append(" "+splitter+" ")
         splitters.append("^"+splitter+" ")
         splitters.append(" "+splitter+"$")
         splitters.append("^"+splitter+"$")
    for splitter in splitters:
        tmp_halves = re.split(splitter, string)
        if (len(tmp_halves) == 2):
            halves = tmp_halves
            #print tmp_halves
            splits += 1
        elif (len(tmp_halves) == 1):
            pass
            #no problem here, just means that a particular equals sign was not found
            #if no equals signs at all are found, it will be caught below
        else:
            
            raise Exception("reaction equation has multiple equals signs: " + string)
        
    if splits > 1:
        raise Exception("reaction equation has multiple equals signs: " + string)
    elif splits == 0:
        raise Exception("reaction equation has no equals signs: " + string)

    return halves

def merge_reactions(reaction_set, **kwargs):
    '''
        given a list of dicts of reactions and direction, merge reactions with overlapping bounds into a single reaction.
        input:
            a list of dicts with the following keys:
                rxn: a reaction object
                dir: 1 or -1, this value will be preserved in the mapping returned.
                    it indicates whether the reaction in the original model was in the same direction (1)
                    or the opposite direction (-1) as the one in the merged reaction
            additional parameters will be passed to the modelreaction.merge method on every call
        returns: the reactions merged into a single reaction
        
        precondition: this function doesn't check if the formulas are the same, so it might give unexpected
        results if reaction_set isn't all the same formula. The advantage to this is that, given the right parameters,
        it can be used for various kinds of merges, such as "adding" two reactions and so forth.
    '''
    #sort the reactions by name (which should be unique), then lower bound
    rl = sorted(reaction_set, key=lambda x: getattr(x['rxn'], 'name'))
    rl = sorted(rl, key=lambda x: getattr(x['rxn'], 'lb'))

    stack = list()
    mapping = dict()# a dict of dicts where key1 is the group name, key2 is 'name' or 'direction'.  the value at 'direction' is 1 or -1
    for d in rl:
        r = d['rxn']
        direct = d['dir']
        if len(stack) == 0:
            stack.append(r)
            mapping[r.name] = {'name': r.name, 'dir': direct}
        else:
            if stack[-1].ub >= r.lb:
                stack[-1] = stack[-1].merge(r, **kwargs)
                mapping[r.name] = {'name': stack[-1].name, 'dir': direct}
            else:
                stack.append(r)
                mapping[r.name] = {'name': r.name, 'dir': direct} 
    return (stack, mapping)
    