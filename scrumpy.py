from __future__ import division #so the eval in scrumpy_to_model works correctly, we don't use integer division at all in this module so it shouldn't cause any unexpected behavior
import model
import re
import modelreaction
import constants
import util

#note: scrumpy notation is to mark compartments with underscore notation. To switch this to bracket notation, use the following regular expression:
# replace _([^_\s]+)(\s)
# with [\1]\2
# this finds the last underscore in a word and everything that comes after it, and replaces it with brackets and whatever comes after it.

# another idiosyncratic scrumpy notation is to identify some external metabolites with a x_ or X_ prefix, we handle this in scrumpy_to_model

def new_reaction(name, coeffs, metabolites, reversible):
    sides = [dict(), dict()]
    for x in range(len(sides)):
        for (i, metab) in enumerate(metabolites[x]):
            sides[x][metab] = coeffs[x][i]
    ub = constants.inf
    lb = constants.ninf
    if not reversible:
        lb = 0
    return modelreaction.ModelReaction(name, sides[0], sides[1], lb, ub, 0)

def scrumpy_to_model(scrumpy_file, round_to_digits = 3, remove_metabolites_with_x_prefix = True, convert_compartment_style = False):
    '''
        Turns a scrumpy model file into a yasmenv model object
        Specifically written for the model by Cheung et al.
        Plant J. 2013 Sep;75(6):1050-61
        It may work for other scrumpy models, but be careful.
        scrumpy files express stoichiometry as fractions. Yasmenv uses decimals.
            So we need to round the fractions.
        
        round_to_digits specifies how many digits after the decimal point to round to.
        
        remove_metabolites_with_x_prefix: if True (default), metabolites whose names begin with x_ or X_ will not be added to the model
        in Scrumpy, x_ generally indicates an external metabolite.
        
        This function does not account for Scrumpy directives such as AutoPolymer(). This isn't a problem for the model referenced above, but it
        may be for other models.
    '''
    #
    
    new_model = model.Model()
    with open(scrumpy_file, 'r') as infile:
        state = "new"
        rxn_name = None
        for line in infile:
            line = line.strip() #trim
            line  = line.replace('"','') #remove quotes
            rxn_name_match = re.search("(\S+):", line)
            if len(line) == 0:
                pass
            elif line[0] == '#':
                pass
                #comment, so ignore
            elif line == '~':
                pass
            elif rxn_name_match:
                if state == "new":
                    rxn_name = rxn_name_match.group(1)
                    state = 'rxn'
            elif re.search('^(\S)+\(.+\)',line):
                pass
                #starts with a function (or something that looks kind of like one, that re will catch various other things as well...), so ignore
            elif " -> " in line or " <> " in line:
                if state == 'rxn':
                    state = "new"
                    if " -> " in line:
                        reversible = False
                        sides = line.split(" -> ")
                    if " <> " in line:
                        reversible = True
                        sides = line.split(" <> ")
                    coeffs = list()
                    metabolites = list()
                    for side in sides:
                        terms = side.split(" + ")
                        side_coeffs = list()
                        side_metabolites = list()
                        met_name = None
                        met_stoich = None
                        for term in terms:
                            term = term.strip()
                            splitted = term.split()
                            if len(splitted) == 1:
                                met_name = splitted[0]
                                met_stoich = 1.0
                            elif len(splitted) == 2:
                                met_name = splitted[1]
                                met_stoich = round(eval(splitted[0], {}, {}), round_to_digits)

                            if met_name is not None and met_stoich is not None:
                                ext_prefix = re.search('^[xX]_', met_name)
                                if not (bool(ext_prefix) and remove_metabolites_with_x_prefix):
                                    if convert_compartment_style:
                                        met_name = util.underscore_compartment_to_bracket_compartment(met_name)
                                    side_metabolites.append(met_name)
                                    side_coeffs.append(met_stoich)
                            else:
                                print "error parsing line in file " + scrumpy_file + ": " + line
                                

                        coeffs.append(side_coeffs)
                        metabolites.append(side_metabolites)
                    if convert_compartment_style:
                        rxn_name = util.underscore_compartment_to_bracket_compartment(rxn_name)
                    new_model.add_reaction(new_reaction(rxn_name, coeffs, metabolites, reversible))
                else:
                    print "error parsing line in file " + scrumpy_file + ": " + line
    return new_model

def scrumpy_compartment_to_yasmenv_compartment(name):
    '''
        given a name that ends with a scrumpy style compartment indicator (_x), convert it into a name
        with a yasmenv/COBRA style compartment indicator ([x])
    '''
    out = name
    match = re.match("^(.+)_([^_]+)$", name)
    if match:
        out = match.group(1) + "[" + match.group(2) + "]"
    return out