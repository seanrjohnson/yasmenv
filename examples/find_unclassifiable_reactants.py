'''
    prints the name of anything listed as a reactant in reactions.dat
    but not present as a unique-id in classes.dat or compounds.dat,
    and the number of reactions they appear in
'''

import yasmenv.biocyc as bc
from pprint import pprint as pp

compounds = bc.Dat("compounds.dat", rec_class=bc.Compound)
reactions = bc.Dat("reactions.dat", rec_class=bc.Reaction)
classes = bc.Dat("classes.dat", rec_class=bc.Class)

reactants = dict()
for r in reactions:
    f = reactions[r].unidentifiable_reactants(compounds, classes)
    if f is not None:
        for k in f:
            reactants[k] = reactants.get(k, 0) + 1
pp(reactants)