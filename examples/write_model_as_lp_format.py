import sys
sys.path.append("../")
import model
import linearprogramming as lp


#bna = model.from_tsv('../test/data/'+'bna572_mint_full.tsv')
bna = model.from_tsv('../test/data/'+'AraGEM.tsv')
solver = lp.Solver()

solution = solver.fba(bna, return_lp=True)

#solution['lp'].write(cpxlp='bna572_mint.cpxlp')
solution['lp'].write(cpxlp='AraGEM.cpxlp')