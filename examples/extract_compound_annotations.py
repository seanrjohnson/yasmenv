#
# generates a tsv file of compounds from a biocyc database
#
#

import yasmenv.biocyc as bc
import yasmenv.util as util

output_name = "compounds.tsv"

out_header = "ID\tcommon_name\tformula\tcharge\ttypes\tCHEBI\tpubchem\tINCHI\tmolecular_weight\tmonoisotopic_weight\tSMILES"


def compound_string(name, compounds):
    common_name = compounds[name].common_name()
    formula = util.molecular_formula_dict_to_string(compounds[name].formula(), True)
    charge = str(compounds[name].charge())
    types = " ".join(compounds[name].types())
    chebi = compounds[name].chebi()
    pubchem = compounds[name].pubchem()
    inchi = compounds[name].inchi()
    mw = compounds[name].molecular_weight()
    monoisotopic = compounds[name].monoisotopic_weight()
    smiles = compounds[name].smiles()
    
    return "\t".join([name, common_name, formula, charge, types, chebi, pubchem, inchi, mw, monoisotopic, smiles])


def extract_compounds():
    compounds = bc.Dat("compounds.dat", rec_class=bc.Compound) #change
    
    with open(output_name, 'w') as out:
        out.write(out_header)
        out.write("\n")
        for cpd_name in compounds.all_records().keys():
            out.write(compound_string(cpd_name, compounds))
            out.write("\n")
    
    
    
    
if __name__ == "__main__":
    extract_compounds()