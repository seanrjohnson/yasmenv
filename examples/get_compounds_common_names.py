'''
    prints the names and common names of all the compounds in the database,
'''

import yasmenv.biocyc as bc
from pprint import pprint as pp

compounds = bc.Dat("E:\\databases\\metacyc\\17.1\\data\\compounds.dat", rec_class=bc.Compound)

names = dict()
for c in compounds:
    name = compounds[c].first('COMMON-NAME')
    if name is not None:
        print c + "\t" + name
    else:
        print c + "\t"