#
#   generates instantiated reactions from a biocyc database
#
#   sorts instantiated reactions into two categories
#   "good": reactions and instances that are balanced and unambiguous
#   "bad": reactions that have no balanced instantiations
#           ambiguous instantiations
#           reactions that are not generic, but are not balanced
#

import yasmenv.biocyc as bc
from pprint import pprint as pp

goodoutfilename = "instantiated_good_test.tsv" #change
badoutfilename = "instantiated_bad_test.tsv" #change
data_directory = "E:/databases/metacyc/17.1/data/" #change

out_header = "Rxn name\tInstance\tDescription\tFormula\tProteins\tSubsystems\tLB\tUB\tObjective\tEC\tSpontaneous\tNotes"

def instantiated_reaction_string(name, instance, instance_num, reactions, enzrxns, proteins):
    bool_dict = {True:'1', False:'0'}
    description = reactions[name].description()
    formula = bc.reaction_as_str(instance['reaction'])
    proteins = " ".join(reactions[name].uniprot(enzrxns, proteins))
    subsystems = " ".join(reactions[name].subsystems())
    (LB, UB) = reactions[name].bounds()
    objective = '0'
    ecs = " ".join(reactions[name].ecs())
    spontaneous = bool_dict[reactions[name].spontaneous()]
    notes = instance['tag']
    
    return "\t".join([name, str(instance_num), description, formula, proteins, subsystems, LB, UB, objective, ecs, spontaneous, notes])

def instantiate():
    classes = bc.Dat(data_directory + "classes.dat", rec_class=bc.Class)
    compounds = bc.Dat(data_directory + "compounds.dat", rec_class=bc.Compound)
    reactions = bc.Dat(data_directory + "reactions.dat", rec_class=bc.Reaction)
    proteins = bc.Dat(data_directory + "proteins.dat", rec_class=bc.Protein)
    enzrxns = bc.Dat(data_directory + "enzrxns.dat", rec_class=bc.Enzrxn)
    
    
    
    with open(goodoutfilename, 'w') as goodout:
        goodout.write(out_header)
        goodout.write("\n")
        with open(badoutfilename, 'w') as badout:
            badout.write(out_header)
            badout.write("\n")
            for rxn_name in reactions.all_records().keys():
                #(balanced_include, unbalanced, balanced_not_include)
                (balanced_include, unbalanced, balanced_not_include) = reactions[rxn_name].instances(compounds, classes)
                for (num, rxn) in enumerate(balanced_include):
                    goodout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    goodout.write("\n")
                for (num, rxn) in enumerate(unbalanced):
                    badout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    badout.write("\n")
                for (num, rxn) in enumerate(balanced_not_include):
                    badout.write(instantiated_reaction_string(rxn_name, rxn, num, reactions, enzrxns, proteins))
                    badout.write("\n")


if __name__ == "__main__":
    instantiate()