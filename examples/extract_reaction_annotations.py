#
# generates a yasmenv tsv file from reactions from a biocyc database
#
#

import yasmenv.biocyc as bc

output_name = "reactions.tsv"

out_header = "Rxn name\tDescription\tFormula\tProteins\tSubsystems\tLB\tUB\tObjective\tEC\tparents\tancestors\tSpontaneous"

def reaction_string(name, reactions, enzrxns, proteins):
    bool_dict = {True:'1', False:'0'}
    description = reactions[name].description()
    formula = bc.reaction_as_str(reactions[name].formula())
    proteins = " ".join(reactions[name].uniprot(enzrxns, proteins))
    subsystems = " ".join(reactions[name].subsystems())
    (LB, UB) = reactions[name].bounds()
    objective = '0'
    ecs = " ".join(reactions[name].ecs())
    parent_reactions = " ".join(reactions[name].parent_reactions(reactions))
    ancestor_reactions = " ".join(reactions[name].ancestor_reactions(reactions))
    spontaneous = bool_dict[reactions[name].spontaneous()]
    
    return "\t".join([name, description, formula, proteins, subsystems, LB, UB, objective, ecs, parent_reactions, ancestor_reactions, spontaneous])


def extract_reactions():
    reactions = bc.Dat("reactions.dat", rec_class=bc.Reaction) #change
    proteins = bc.Dat("proteins.dat", rec_class=bc.Protein) #change
    enzrxns = bc.Dat("enzrxns.dat", rec_class=bc.Enzrxn) #change
    with open(output_name, 'w') as out:
        out.write(out_header)
        out.write("\n")
        for rxn_name in reactions.all_records().keys():
            out.write(reaction_string(rxn_name, reactions, enzrxns, proteins))
            out.write("\n")
    
    
    
    
if __name__ == "__main__":
    extract_reactions()