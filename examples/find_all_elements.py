'''
    prints the symbols of all elements in the database,
    and the number of compounds they appear in
'''

import yasmenv.biocyc as bc
from pprint import pprint as pp

compounds = bc.Dat("compounds.dat", rec_class=bc.Compound)

elements = dict()
for c in compounds:
    f = compounds[c].formula()
    if f is not None:
        for (k, v) in f.iteritems():
            elements[k] = elements.get(k, 0) + 1
pp(elements)