'''
    gets gene associations for all reactions in a database that have associated genes

'''

import yasmenv.biocyc as bc

def bool_string(l):
    anded = list()
    for s in l:
        anded.append("(" + " and ".join(s) + ")")
    ord = " or ".join(anded)
    return ord

data_directory = "E:/databases/aracyc/11.0/data/" #change

#classes = bc.Dat(data_directory + "classes.dat", rec_class=bc.Class)
#compounds = bc.Dat(data_directory + "compounds.dat", rec_class=bc.Compound)
#reactions = bc.Dat(data_directory + "reactions.dat", rec_class=bc.Reaction)
proteins = bc.Dat(data_directory + "proteins.dat", rec_class=bc.Protein)
enzrxns = bc.Dat(data_directory + "enzrxns.dat", rec_class=bc.Enzrxn)
genes = bc.Dat(data_directory + "genes.dat", rec_class=bc.Gene)

out = dict() # a dict of lists of lists. [reaction_name][enzrxn][genes_associated_with_enzrxn]

for enzrxn in enzrxns:
    reactions = enzrxns[enzrxn]['REACTION']
    
    if len(reactions) > 0:
        #don't bother finding the genes if there's no reaction to associate it with
        enzymes = enzrxns[enzrxn]['ENZYME']
        for enzyme in enzymes:
            tmp_genes = proteins[enzyme].genes(proteins, genes)
            for reaction in reactions:
                out[reaction] = out.get(reaction, list()) + [tmp_genes]
                #print reaction + "\t" + bool_string(out[reaction])

for reaction in out:
    print reaction + "\t" + bool_string(out[reaction])
    
