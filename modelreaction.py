import operator
from copy import deepcopy
import re
from collections import OrderedDict

class ModelReaction(object):
    
    def __init__(self, name, left_side, right_side, lb, ub, objective, annotations = OrderedDict()):
        self.name = name
        
        if (ub < lb):
            print "warning, upper bound less than lower bound for reaction %s" % name
        self.ub = ub
        self.lb = lb
        self.objective = objective
        self.left = left_side #dict keys are metabolite names, values are stoichiometry
        self.right = right_side #dict
        self.annotations = annotations #dict of strings
    # def __eq__(self, other):
       # return self.compare(other) == 'same'
    def __repr__(self):
        return self.name
    
    def copy(self):
        return deepcopy(self)
        
    def as_str(self, equals_sign = "<=>", net=False):
        '''
            returns a string representation of the reaction
        '''
        rxn = self
        if net:
            rxn = self.net()
        left = list()
        right = list()
        for (met, stoich) in rxn.left.iteritems():
            left.append(str(stoich) + " " + met)
        
        for (met, stoich) in rxn.right.iteritems():
            right.append(str(stoich) + " " + met)
        
        left = sorted(left)
        right = sorted(right)
        
        
        eq = " " + equals_sign + " "
        return eq.join((" + ".join(left), " + ".join(right))).strip()
    def metabolites(self, side="both"):
        '''
            returns a set of the metabolites in the reaction
            side can be "both", "left", or "right", and fails silently if it's something else
        '''
        out = set()
        include_side = list()
        if side == "left" or side == "both":
            include_side.append("left")
        if side == "right" or side == "both":
            include_side.append("right")
        
        for side in include_side:
            out.update(getattr(self, side))
        
        return out
        
    def compartments(self, side="both"):
        '''
            returns a set of the compartments represented by the metabolites in the reaction
            side can be "both", "left", or "right", and fails silently if it's something else
        '''
        out = set()
        mets = self.metabolites(side)
        compartment_regex = re.compile('^(.+)\[([^\[]+)\]$')
        
        for met in mets:
            match = compartment_regex.search(met)
            if match:
                out.add(match.group(2))
        return out
    
    def stoich_dict(self):
        out = dict()
        for (met, stoich) in self.left.iteritems():
            out[met] = -1 * stoich
        for (met, stoich) in self.right.iteritems():
            out[met] = stoich
        return out
    
    def get_annotations(self, annotation, sep="\s+"):
        '''
          retrieve as a list the annotations with a given heading.
          input:
            annotation: the name of the annotation to retrieve
            sep: regex to split the annotation on
          annotations are stored internally as strings. It is up to the user to choose a separator 
          if multiple annotations are necessary under a given title.
        '''
        out = re.split(sep, self.annotations[annotation].strip())
          
        return out
    
    def compare(self, other, net=False, annotations=list()):
        '''
            compares the reaction to another reaction
            
            annotations is a list of annotation fields to compare
            
            returns a set of strings describing the relation of the two reactions:
                'same' if all attributes are the same
                'forward' if the stoichiometries are the same
                'reverse' if the stoichiometries are the same, except left and right are swapped
                'overlap' if the bounds overlap (or if 'reverse', if the reverse of the bounds overlap)
                'surrounded' if overlap and bounds of one reaction are completely contained in the other
                'objective' if the objective function is different (or if reversed if they are not opposite)
                'annotation_x' where x is the name of an annotation field, if that annotation field is different
        '''
        diffs = set() #differences
        out = set()
        
        if net:
            self = self.net()
            other = other.net()
        
        if self.name != other.name:
            diffs.add('name')
        
        if not dicts_equal(self.left, other.left):
            diffs.add('left')
        
        if not dicts_equal(self.right, other.right):
            diffs.add('right')
        
        if self.lb != other.lb:
            diffs.add('lb')
        
        if self.ub != other.ub:
            diffs.add('ub')
            
        if self.objective != other.objective:
            diffs.add('objective')
            out.add('objective')
        
        for annotation in annotations:
            if self.annotations.get(annotation, "") != other.annotations.get(annotation, ""):
                diffs.add('annotation_'+annotation)
                out.add('annotation_'+annotation)
        
        if (('left' not in diffs) and ('right' not in diffs)):
            out.add('forward')
            over = overlap(self.lb, self.ub, other.lb, other.ub)
            if overlap(self.lb, self.ub, other.lb, other.ub) != 'separate':
                out.add('overlap')
                if over == "surrounded":
                    out.add("surrounded")
        else:
            if (dicts_equal(self.left, other.right) and dicts_equal(self.right, other.left)):
                out.add('reverse')
                over = overlap(self.lb, self.ub, *other.reversed_bounds())
                if overlap(self.lb, self.ub, *other.reversed_bounds()) != 'separate':
                    out.add('overlap')
                    if over == "surrounded":
                        out.add("surrounded")
        
        return out
    
    
    def net(self):
        '''
            returns o copy of the reaction converted into a net reaction
                if a metabolite is on both sides of the reaction, the side with the greater stoichiometry will keep it with stoichiometry reduced by the other side.  
                if stoichiometry is equal on both sides, the metabolite will be removed from both sides
                if a metabolite has a stoichiometry of zero, it will be removed
        '''
        net = dict()
        lr = {'left':-1., 'right':1.}
        for (direction, factor) in lr.iteritems():
            for (met, coeff) in getattr(self,direction).iteritems():
                net[met] = net.get(met, 0.) + factor*coeff
        l, r = dict(), dict()
        for (met, coeff) in net.iteritems():
            if coeff > 0:
                r[met] = coeff
            elif coeff < 0:
                l[met] = -1.*coeff
            else:
                #coeff == 0, so don't put it on either side
                pass
        return (ModelReaction(self.name, l, r, self.lb, self.ub, self.objective, self.annotations))
    
    def split_bounds(self):
        '''
            Precondition: lb <= ub
        
            returns the lower and upper bounds for the corresponding irreversible reactions
            a list of two 2-tuples
            the first tuple is the lb and ub for the forward reaction
            the second tuple is the lb and ub for the reverse reaction
        '''
        lb = self.lb
        ub = self.ub
        
        (flb,fub,rlb,rub) = [0.0] * 4
        
        
        if lb >= 0.:
            #lower bound is positive, so the reaction can only go in the forward direction, so the bounds for the reverse direction are both zero
            #and the bounds for the forward reaction are the original bounds
            flb = lb
            fub = ub
        elif ub <= 0.:
            #upper bound is negative, so the reaction can only go in the reverse direction, so the bounds for the forward direction are both zero
            #and the bounds for the reverse direction are derived from the original bounds (by mirroring them accross the zero)
            
            rub = -1.*lb
            rlb = -1.*ub
        else:
            #the reaction is reversible
            # flb = 0, rlb = 0
            fub = ub
            rub = -1*lb

        return [(flb,fub),(rlb,rub)]
    
    def reversed_bounds(self):
        '''
            returns the reflection of the bounds over 0
            Precondition: lb <= ub
        '''
        lb = self.lb
        ub = self.ub
        
        
        nlb = -1.*ub
        nub = -1.*lb
        
        return (nlb,nub)
    
    def merge(self, other, net=True, name=None, objective_merge=operator.add, metabolites_merge=operator.add, keep_annotations=True, annotations_sep= " ", annotations_sep_default=" "):
        '''
            Merges two reactions
            returns a new ModelReaction that is the merger of self and other
            
            uses the lowest lower bound, and the highest upper bound
            combines the objective coefficients according to function supplied at 'objective' (default add)
            combines metabolite coefficients based on the function supplied at 'metabolites' (default add)
                in many cases it will make more sense to supply an averaging function here, or max
            if net=True (default), derives the net reaction after combining.
            
            annotations_sep can be a dict or a string, if it's a dict, then the keys are annotation field names, and the values are the separator for that field.
                in this case, annotations_sep_default is a string used for any field not in the dict
                if it's a string, then the same separator is used for all annotation fields
            
            if name is supplied, uses the supplied name, else uses the name of self
        '''
        
        (lb,ub) = extreme_bounds(self, other)
        
        obj = objective_merge(self.objective, other.objective)
        
        # Calculate Expression
        # There isn't an obvious way to handle expression of None
        # because None means no expression information supplied
        # which is not the same thing as expression = 0
        # expression = None #declare
        # if self.expression is None and other.expression is None:
            # expression = None
        # elif self.expression is None:
            # expression = other.expression
        # elif other.expression is None:
            # expression = self.expression
        # else:
            # expression = expression_merge(self.expression, other.expression)
        
        sides = {'left':dict(), 'right':dict()}
        for (side_name, out_side) in sides.iteritems():
            for met in set(getattr(self, side_name).keys() + getattr(other, side_name).keys()):
                self_coeff = 0.
                other_coeff = 0.
                if met in getattr(self, side_name):
                    self_coeff = getattr(self, side_name)[met]
                if met in getattr(other, side_name):
                    other_coeff = getattr(other, side_name)[met]
                out_side[met] = metabolites_merge(self_coeff, other_coeff)
        
        if name is None:
            name = self.name
        else:
            #name remains whatever was passed in
            pass

        annotations = OrderedDict()
        if keep_annotations:
            all_annotations = set(self.annotations.keys()) | set(other.annotations.keys())
            for annotation in all_annotations:
                sep = annotations_sep
                if isinstance(annotations_sep, dict):
                  sep = annotations_sep.get(annotation, annotations_sep_default)
                annotations_text = set(self.annotations.get(annotation, "").split(sep)) | set(other.annotations.get(annotation, "").split(sep))
                annotations_text = {x for x in annotations_text if x.strip() != ""}
                annotations[annotation] = sep.join(annotations_text)
        
        out = ModelReaction(name, sides['left'], sides['right'], lb, ub, obj, annotations)
        if net:
            out = out.net()
        
        return out
    
    def is_boundary(self):
        '''
            returns True if the reaction represents a boundary flux, else False.
            
            a boundary flux is defined as a reaction with the following form: 
            metabolite <=> 
            or
            <=> metabolite
            that is: there should be only one metabolite on one side, and none on the other.
            
            Alternative formulations of boundary fluxes (such as active transport)
            will not be recognized by this function. It is recommended to rewrite these
            reactions as two separate reactions, for example:
            metabolite[in] + ATP[in] <=> ADP[in] + Pi[in]
            could be rewritten as 
            metabolite[in] + ATP[in] <=> metabolite[ext] + ADP[in] + Pi[in]
            metabolite[ext] <=> 
        '''
        
        if len(self.left_side) + len(self.right_side) == 1:
            return True
        else:
            return False
        
        
    def reversed(self):
        '''
            reverses a reaction
            
            returns a new ModelReaction that is the reverse of the current reaction
            
            all aspects are reversed (except name and annotations).
            left coefficients become right coefficients
            bounds are swapped then reflected over 0
            the objective function is multiplied by -1
        '''
        
        l = self.right
        r = self.left
        (lb,ub) = self.reversed_bounds()
        obj = -1. * self.objective
        name = self.name
        #(self, name, left_side, right_side, lb, ub, objective)
        return ModelReaction(name, l, r, lb, ub, obj, deepcopy(self.annotations))
        
        

def overlap(start_1, end_1, start_2, end_2):
    '''
        returns a description of the overlap of the two segments:
            "contained": one segment is completely contained in the other, or if the segments are identical
            "surrounded": contained except that borders do not touch
            "overlap": exactly one end of one segment touches or is inside the other segment
            "separate": the two segments do not touch or overlap
            
            precondition: start_1 <= end_1, start_2 <= end_2
    '''
    
    #convert bounds to finite, we're not going to bother converting them back, so we only need the first two return values
    ((start_1, start_2), (end_1, end_2)) = convert_bounds_to_finite((start_1, start_2), (end_1, end_2))[0:2] 
    
    a = start_1 - start_2 ## (a)
    b = end_1 - start_2 ## (b)
    c = start_1 - end_2 ## (c)
    d = end_1 - end_2 ## (d)
    
    out_cat = ""
    
    ##contained if a * d <= 0
    if (a*d <= 0):
        if a*d < 0: #ADDED
            out_cat = "surrounded"
        else:
            out_cat = "contained"
    
    ##overlap if a*d > 0 and c*b <= 0
    elif (c*b <= 0):
        out_cat = "overlap"
    
    ##separate if a*d > 0 and c*b > 0
    else:
        out_cat = "separate"
        
    
    return out_cat

def convert_bounds_to_finite(lower, upper):
    '''
        converts None bounds to finite numbers guaranteed to be (larger for upper bounds, or smaller for lower bounds) than any non-Nones in the input
        returns (list_of_converted_lower_bounds, list_of_converted_upper_bounds, lowest, highest)
            where lowest and highest are the values substituted in for the Nones
    '''
    inf = float('inf')
    ninf = -inf
    
    #get all non-inf bounds
    bounds = [x for x in upper + lower if (x != ninf and x != inf)]
    #append 0 just in case they're all inf
    bounds.append(0.)
    
    lowest = min(bounds) - 1. #find a number smaller than any in the list
    highest = max(bounds) + 1. #find a number larger than any in the list

    #out_lower = list()
    
    out = {'lower':list(), 'upper':list()}
    
    for (direction, out_list) in out.iteritems():
        for bound in locals()[direction]:
            if bound == ninf:
                #print direction
                out_list.append(lowest)
            elif bound == inf:
                out_list.append(highest)
            else:
                out_list.append(bound)

    return (out['lower'], out['upper'], lowest, highest)
    
def convert_bounds_to_infinite(lower, upper, low_inf, high_inf):
    '''
        the inverse operation of convert_bounds_to_finite
    '''
    inf = float('inf')
    ninf = -inf
    
    out = {'lower':list(), 'upper':list()}
    
    for (direction, out_list) in out.iteritems():
        for bound in locals()[direction]:
            if bound == low_inf:
                out[direction].append(ninf)
            elif bound == high_inf:
                out[direction].append(inf)
            else:
                out[direction].append(bound)
    
            
    return(out['lower'], out['upper'])
    
def extreme_bounds(*args):
    '''
        input any number of ModelReaction objects
        returns (lower_bound, upper_bound)
        where lower_bound is the lowest of the lower bounds of the inputs
        and upper_bound is the highest of the uppers
    '''
    
    #convert bounds to finite, so we can find the smallest and largest
    (lower, upper, lowest, uppest) = convert_bounds_to_finite([x.lb for x in args], [x.ub for x in args])
    
    #find the smallest lower_bound and largest upper bound and then cinvert them to infinite bounds if necessary
    ([out_lower], [out_upper]) = convert_bounds_to_infinite([min(lower)], [max(upper)], lowest, uppest)
    
    return (out_lower, out_upper)

def dicts_equal(d1, d2):
    #TODO: turns out this function really isn't necessary...
    for (k, v) in d1.iteritems():
        if (k not in d2) or (d2[k] != v):
            return False
    
    for (k, v) in d2.iteritems():
        if (k not in d1) or (d1[k] != v):
            return False
    return True