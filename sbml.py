import xml.etree.ElementTree as ET
from collections import OrderedDict
import util

def read_sbml(filename, convert_compartments_to_bracket_style=False):

  sbml_tag = '{http://www.sbml.org/sbml/level2}'
  html_tag = '{http://www.w3.org/1999/xhtml}'
  list_of_species_tag = sbml_tag+'listOfSpecies'
  list_of_reactions_tag = sbml_tag+'listOfReactions'
  list_of_compartments_tag = sbml_tag+'listOfCompartments'
  list_of_left_side_tag = sbml_tag+'listOfReactants'
  list_of_right_side_tag = sbml_tag+'listOfProducts'
  list_of_notes_tag = sbml_tag+'notes'
  list_of_kinetic_law_parameters_tag = sbml_tag + 'listOfParameters'
  
  species_tag = sbml_tag+'species'
  compartment_tag = sbml_tag+'compartment'
  reaction_tag = sbml_tag+'reaction'
  reactant_tag = sbml_tag+'speciesReference'
  notes_tag = html_tag+'p'
  kinetic_law_tag = sbml_tag + 'kineticLaw'
  kinetic_law_parameter_tag = sbml_tag + 'parameter'
  
  out_reactions = OrderedDict()
  out_compartments = OrderedDict()
  out_metabolites = OrderedDict()
  
  tree = ET.parse(filename)
  root = tree.getroot()
  
  reactions = root.findall('.//' + reaction_tag)
  
  for reaction in reactions:
    i = reaction.attrib['id']
    if convert_compartments_to_bracket_style:
      i = util.underscore_compartment_to_bracket_compartment(i)
    reaction_name = reaction.attrib['name']
    out_reactions[i] = {}
    out_reactions[i][u'name'] = reaction.attrib['name']
    out_reactions[i][u'id'] = i
    out_reactions[i][u'reversible'] = reaction.attrib['reversible']
    out_reactions[i][u'left'] = {}
    out_reactions[i][u'right'] = {}
    out_reactions[i][u'properties'] = list() #list of tuples, [0] = property name, [1] = property value
    for side in ((list_of_left_side_tag, u'left'), (list_of_right_side_tag, u'right')):
      lists_of_sides = reaction.findall(side[0])
      for side_list in lists_of_sides:
        for reactant in side_list.findall(reactant_tag):
          metabolite_name = reactant.attrib['species']
          if convert_compartments_to_bracket_style:
            metabolite_name = util.underscore_compartment_to_bracket_compartment(metabolite_name)
          stoich = float(reactant.attrib['stoichiometry'])
          if metabolite_name not in out_reactions[i][side[1]]:
            out_reactions[i][side[1]][metabolite_name] = 0.0
          out_reactions[i][side[1]][metabolite_name] += stoich

    for comment_list in reaction.findall(list_of_notes_tag):
      # print('x1')
      # print(comment_lists)
      # for comment_list in comment_lists:
        # print('x2')
        # print(comment_list)
      #for comment in comment_list.findall(notes_tag):
      for comment in comment_list.findall(notes_tag):
        #print('x3')
        splitted = comment.text.split(":")
        out_reactions[i][u'properties'].append((splitted[0], splitted[1]))
    
    for kinet in reaction.findall(kinetic_law_tag):
      for param in kinet.find(list_of_kinetic_law_parameters_tag).findall(kinetic_law_parameter_tag):
        out_reactions[i][u'properties'].append((param.attrib['id'], param.attrib['value']))
  
  for compart_list in root.findall('.//' + list_of_compartments_tag):
    for compart in compart_list.findall(compartment_tag):
      out_compartments[compart.attrib['id']] = compart.attrib['name']
  
  for spec_list in root.findall('.//' + list_of_species_tag):
    for spec in spec_list.findall(species_tag):
      met_name = spec.attrib['id']
      if convert_compartments_to_bracket_style:
        met_name = util.underscore_compartment_to_bracket_compartment(met_name)
        spec.attrib['id'] = met_name
      out_metabolites[met_name] = spec.attrib
      if 'name' in out_metabolites[met_name] and convert_compartments_to_bracket_style:
        out_metabolites[met_name]['name'] = util.underscore_compartment_to_bracket_compartment(out_metabolites[met_name]['name'])
  
  return (out_reactions, out_metabolites, out_compartments)
  
def write_sbml(model, filename):
  pass
  