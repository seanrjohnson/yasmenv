import re
import util

class Metabolite(object):
    
    def __init__(self, name, formula, mass, charge, structure, synonyms, annotations=dict()):
        self.name = name #unique name, can be arbitrary
        self.formula = formula #a string of the formula TODO:maybe make a distinct formula class
        self.mass = mass 
        self.charge = charge 
        self.structure = structure #string, a molfile
        self.synonyms = synonyms #dict key is database or format (like KEGG or ChEBI or IUPAC, InChI) or some other descriptor, value is a set of ids
        self.annotations = annotations #dict, key is annotation name, value is annotation value
    

    
    @classmethod
    def remove_duplicates(cls,list):
        pass
    
    @classmethod
    def combine_lists(cls, list1, list2):
        pass
    
    @classmethod
    def update(cls, into, outof, compare = None): #into and outof are dicts of Metabolites, compare is a list of strings
        '''
            fills in missing information in one dict of Metabolites with information from another
            
            compares based on the 'synonyms' key specified by 'compare', if no keys are specified, any key-value pair will count as a match
            a match is counted two metabolites, one from each list, have the same word for a key of 'synonyms', and the two sets pointed to
            by that key intersect with at least one member.
            
            any field that has a value of 'None' in the molecule in into will take its value from outof.
            and all synonyms listed in outof will be added to into.
            
            if there is a descrepancy in charge, mass, or formula, a warning will be printed and the metabolites will not merge
            
        '''
        
        outof_index = {} # a hash of hashes of lists of strings.  First key is equivalent to out.of synonyms key, second key is a value from the set, value is a list of keys for outof that conforms to the preceding two keys.
        
        # first create an index of synonyms from outof with a key from compare
            # for out_of_metabolite in outof:
                # for synonym_key in out_of_metabolite.synonyms.keys()
                    
                
        
        pass
        
        
    
    @classmethod
    def from_rhea_biopax2(cls, biopax_metabolite_dict, biopax_xref_dict):
        out = {}
        
        for metabolite in biopax_metabolite_dict.keys():
            name = metabolite
            formula = None
            if 'formula' in biopax_metabolite_dict[metabolite]:
                formula = biopax_metabolite_dict[metabolite]['formula']
            else:
                print "no formula for " + metabolite

            mass = None
            charge = None
            structure = None
            synonyms = {}
            
            synonyms['Common_Name'] = set()
            synonyms['Common_Name'].add(biopax_metabolite_dict[metabolite]['name'])
            
            for xref in biopax_metabolite_dict[metabolite]['xref']:
                if (xref in biopax_xref_dict):
                    db = biopax_xref_dict[xref]['db']
                    id = biopax_xref_dict[xref]['id']
                    if (not db in synonyms):
                        synonyms[db] = set()
                    synonyms[db].add(id)
                else:
                    print "warning metabolite xref " + xref + " not found"
                    
            if (metabolite in out):
                #this will probably never happen... but if it does, it means trouble
                print "warning metabolite " + metabolite + " found twice in list of reactions from biopax"
            
            out[metabolite] = Metabolite(name, formula, mass, charge, structure, synonyms)
        
        return out
    
    @classmethod
    def from_sbml(cls, filename, convert_compartments_to_bracket_style=True):
        import sbml
        (sbml_reactions, out_metabolites, out_compartments) = sbml.read_sbml(filename, convert_compartments_to_bracket_style=convert_compartments_to_bracket_style)
        out = {}
        

        for (met, attribs) in out_metabolites.iteritems():
            name = ""
            formula = "Unknown"
            mass = float("nan")
            charge = 0
            structure = ""
            synonyms = dict()
            annotations = dict()
            #print(met, attribs)
            for attrib, value in attribs.iteritems():
                if attrib == 'id':
                    name = value
                elif attrib == "name":
                    synonyms['Common_Name'] = set()
                    synonyms['Common_Name'].add(value)
                elif attrib == 'charge':
                    charge = float(value)
                annotations[attrib] = value
            out[name] = Metabolite(name, formula, mass, charge, structure, synonyms, annotations)
          
        return out


    @classmethod
    def from_sdf(cls, filename, type = "ChEBI"):
        '''
            input: the name of an sdf file, the type of file.
                SDF is not a well standardized format, so the fields may be named differently depending on the source
            output: a dict of metabolites where the keys are (arbitrarily) met[number] (e.g. met1, met2, etc), and values are Metabolite objects
        '''
        
        out = {}
        
        field_names = {} #key is the sdf field name, value is the Metabolite field name
        synonym_names = {} #key is the sdf field name, value is what to use as the key
        
        if (type == "ChEBI"):
            field_names = {'Formulae': 'formula', 'Mass': 'mass', 'Charge': 'charge'}
            synonym_names = {'ChEBI ID': 'ChEBI_ID', 'ChEBI Name': 'ChEBI_Name', "Secondary ChEBI ID": 'ChEBI_ID', 'SMILES': 'SMILES', 'InChIKey': 'InChIKey', 'InChI': 'InChI', 'IUPAC Names': 'IUPAC', 'Synonyms': 'Common_Name', 'PubChem Database Links': 'PubChem_SID'}
            #main_name = 'ChEBI ID'
        elif (type == "Pubchem"):
            field_names = {'PUBCHEM_MOLECULAR_FORMULA': 'formula', 'PUBCHEM_MOLECULAR_WEIGHT': 'mass', 'PUBCHEM_TOTAL_CHARGE': 'charge'}
            synonym_names = {'PUBCHEM_OPENEYE_CAN_SMILES': 'SMILES', 'PUBCHEM_OPENEYE_ISO_SMILES': 'SMILES', 'PUBCHEM_IUPAC_INCHIKEY': 'InChIKey', 'PUBCHEM_IUPAC_INCHI': 'InChI', 'PUBCHEM_IUPAC_NAME': 'IUPAC', 'PUBCHEM_IUPAC_SYSTEMATIC_NAME': 'IUPAC', 'PUBCHEM_IUPAC_TRADITIONAL_NAME': 'IUPAC', 'PUBCHEM_SUBSTANCE_SYNONYM': 'Common_Name', 'PUBCHEM_COMPOUND_CID': 'PubChem_CID', 'PUBCHEM_SUBSTANCE_ID': 'PubChem_SID'}
            #main_name = 'ChEBI ID'
        elif (type == 'Rhea'):
            field_names = {'Formula': 'formula', 'Mass': 'mass', 'Charge': 'charge'} #note that there doesn't seem to actually be a 'Mass' field in the Rhea sdf format
            synonym_names = {'ChEBI ID': 'ChEBI_ID', 'UniProt.name': 'UniProt_Name', 'UniProt.name.ascii': 'UniProt_Name_Ascii'}
        
        sdf_file = cls._parse_sdf(cls, filename)
        met_num = 0
        for record in sdf_file:
            met_num += 1
            name = "met" + met_num
            
            metabolite_data = {}
            synonyms = {}
            if ("Molfile" in record):
                metabolite_data['structure'] = record["Molfile"][0]
            else:
                metabolite_data['structure'] = ''
            for field_name in field_names.keys():
                if (field_name in record):
                    metabolite_data[field_names[field_name]] = record[field_name]
                else:
                    metabolite_data[field_names[field_name]] = ""
                    print "warning: no field " + field_name + " for substance " + met_num
            for syn_name in synonym_names.keys():
                if (syn_name in record):
                    if (synonym_names[syn_name] not in synonyms):
                        synonyms[synonym_names[syn_name]] = set()
                    synonyms[synonym_names[syn_name]].update(set(record[syn_name]))
            #(self, name, formula, mass, charge, structure, synonyms)
            tmp_metab = Metabolite(metabolite_data['name'], metabolite_data['formula'], metabolite_data['charge'],metabolite_data['structure'],synonyms)
            out[name] = tmp_metab
        return out

    @classmethod
    def _parse_sdf(cls, filename):
        """
        open a molfile/SDF file (such as a .sdf from ChEBI or pubchem) and extract data from it into a data structure
        Input: filepath
        output: a list of dicts of lists, the first index is arbitrary, the second index is the field name, the third index is the value is a data line from the file
            
        """
        
        #loosely based on a parser by Ian Daniher at 
        # https://github.com/itdaniher/PyChEBI/blob/master/ChEBI_JSON_Builder.py
        
        out = []
        rec = -1
        
        with open(filename) as infile:
            file_text = infile.read()
            
            # split string by "\n$$$$\n", the SD file segmenter
            file_text = file_text.split("\n$$$$\n")

            # normalize Molfile data to match formatting for the rest of the file
            file_text = ["\n> <Molfile>\n"+item for item in file_text]
            
            record_delimeter = re.compile('^> <(.+)>$')
            for record in file_text:
                lines = record.split("\n")
                field = None
                rec += 1
                out[rec] = {}
                for line in lines:
                    delimeter_match = record_delimeter.match(line)
                    if (delimeter_match):
                        field = delimeter_match.group(1)
                    else:
                        if (field not in out[rec]):
                            out[rec][field] = []
                        if (field == "Molfile"):
                            out[rec][field][0] += line
                        else:
                            out[rec][field].append(line)
        return out