import glpk
import math

class Solver:
    def __init__(self):
        pass
        
    def fba(self, model, maximize=True, return_lp=False, minimize_flux=False):
        '''
            Flux balance analysis on the input model.
            
            maximize: True if the objective function should be maximized, false if it should be minimized
            return_lp: True if the returned data structure should include the glpk.LPX() object used to solve the FBA
            minimize_flux: If True, a secondary LP will be solved using the objective value for the first LP as a constraint.
                            In the new LP, the objective function will be set to minimize the total flux (and thus avoid huge fluxes from loops)
            
            if an optimal solution is found:
                returns a dict with the following members:
                    obj: the value of the 
                    fluxes: list of 2-tuples where the first value is the reaction name, and the second is the flux
                    lp: (optional) the lp structure generated for the problem
            else:
                prints an error message and returns None
        '''
        out = dict()
        lp = glpk.LPX()
        lp.obj.maximize = maximize
        
        model_in = model #save the original model
        rxn_map = None #declare
        if minimize_flux:
            (model, rxn_map) = model.make_irreversible() #recalculate the model as irreversible if we're going to minimize flux
        
        lp.cols.add(model.len_reactions())
        lp.rows.add(model.len_metabolites())
        
        lp.matrix = model.sparse_matrix()
        
        for (i, rxn) in enumerate(model.reactions_list()):
            lp.obj[i] = rxn.objective
            #print rxn.lb, rxn.ub
            lp.cols[i].bounds = bound_to_glpk_bound(rxn.lb, True), bound_to_glpk_bound(rxn.ub, False)
        
        for row in lp.rows:
            row.bounds = 0
        
        success = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution.  If there isn't a solution, None is returned.
        if not success:
            return None
        elif (not minimize_flux):
            out['obj'] = lp.obj.value
            out['fluxes'] = [(model.reactions_list()[i].name, lp.cols[i].primal) for i in range(len(lp.cols))]
        else:
            #print lp.obj.value
            out['obj'] = lp.obj.value #save the originial objective value for the output
            
            #fix the objective to the first solution
            objective_row = lp.rows.add(1) #add one more row to the model to hold the previous objective solution
            lp.rows[objective_row].bounds = lp.obj.value #fba_sol['obj'], fba_sol['obj'] #force the new row to have the optimal value calculated by the fba problem
            #add coefficients to the objective row (make it the same as the objective function)
            mat = lp.matrix
            for i in range(len(lp.obj)):
                if lp.obj[i] != 0.:
                    mat.append((objective_row, i, lp.obj[i]))
            lp.matrix = mat
            
            #reset the objective function to ones
            for i in range(len(lp.obj)):
                lp.obj[i] = 1.
            
            #set the LP to be a minimization
            lp.obj.maximize = False
            
            success2 = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution.  If there isn't a solution, None is returned.
            if success2:
                out_fluxes = dict()
                for (i, rxn) in enumerate(model.reactions_list()):
                    out_fluxes[rxn_map[rxn.name]['name']] = out_fluxes.get(rxn_map[rxn.name]['name'], 0) + rxn_map[rxn.name]['direction'] * lp.cols[i].primal
                
                out['fluxes'] = [(i.name, out_fluxes[i.name]) for i in model_in.reactions_list()]
            else:
                print "Warning: failure in secondary LP optimization"
                return None
        
        if return_lp:
            out['lp'] = lp
        # print lp.status
        # print lp.obj.value
        if lp.status == 'opt' and math.isnan(out['obj']):
            # TODO: this may not be the best place to deal with this (see the comment in solve_simplex)
            out['obj'] = 0.0
        
        return out

    def fva(self, model, maximize=True, verbose=True):
        '''
            Flux variability analysis on the input model
            
            TODO: change this so it doesn't require there to be a FBA solution (see the GUROBI implementation)
                    that way you can use it to check every reactions flux carrying ability
            
            if the model has an optimal fba solution (and therefore fva can be perfromred):
                returns a dict with the following members:
                    obj: the optimal objective value of the fba solution
                    fluxes: a list of 3-tuples where the first member is the reaction name, the second is the minimum flux, and the third is the maximum.
                        (-)inf means unbounded below
                        (+)inf means unbounded above
                    
            else:
                returns None
        '''
        out = dict()
        out['fluxes'] = list()
        
        fba_sol = self.fba(model, maximize=maximize, return_lp=True)
        if not fba_sol:
            return None
        else:
            lp = fba_sol['lp'] # get the lp for the solved fba problem
            out['obj'] = fba_sol['obj'] #save the fba solution objective value
            
            objective_row = lp.rows.add(1) #add one more row to the model to hold the previous objective solution
            lp.rows[objective_row].bounds = fba_sol['obj'] #fba_sol['obj'], fba_sol['obj'] #force the new row to have the optimal value calculated by the fba problem
            
            #print "obj value %f" % fba_sol['obj']
            
            #add coefficients to the objective row (make it the same as the objective function)
            mat = lp.matrix
            for i in range(len(lp.obj)):
                if lp.obj[i] != 0.:
                    mat.append((objective_row, i, lp.obj[i]))
            lp.matrix = mat
            
            #print lp.matrix
            
            #reset the objective function to zeros
            for i in range(len(lp.obj)):
                lp.obj[i] = 0.
            
            #find maxima and minima for each reaction
            #continually reuse the same lp object, thereby "warmstarting", using the previous solution as an initial basis for the 
            #next optimization.  This results in fast execution times
            for i in range(len(lp.cols)):
                # set the objective function to the current reaction
                lp.obj[i] = 1
                min = None
                max = None
                
                #solve the minimization
                lp.obj.maximize = False
                success = solve_simplex(lp, accept_unbound=True, verbose=False)
                if success:
                    if (lp.status == 'unbnd'):
                        min = '(-)inf'
                    else:
                        min = lp.obj.value
                
                #solve the maximization
                lp.obj.maximize = True
                success = solve_simplex(lp, accept_unbound=True, verbose=False)
                if success:
                    if (lp.status == 'unbnd'):
                        max = '(+)inf'
                    else:
                        max = lp.obj.value
                
                out['fluxes'].append((model.reactions_list()[i].name, min, max))
                
                #reset the objective coefficient back to zero
                lp.obj[i] = 0
        return out
        
        
    def single_knockout(self, model, maximize=True):
        '''
            Determines the essentiality of individual reactions for optimizing the objective function.
            
            First solves FBA for the full model.
            
            Then, for one reaction at a time, sets the upper and lower bound to zero and resolves the FBA. (A "simulated knockout")
            
            The value of the objective function for each of the knockouts is reported. If the knockout causes the
            model to be unsolvable, 0.0 is reported
            
            returns a dict with the following members:
                obj: the optimal objective value of the fba solution
                kos: a list of 2-tuples where the first value is the reaction name, and the second value is the objective function for the knockout 
            
            If the initial FBA is infeasible, returns None
        '''
        
        out = dict()
        out['kos'] = list()
        
        fba_sol = self.fba(model, maximize=maximize, return_lp=True)
        obj = fba_sol
        
        if not fba_sol:
            return None
        else:
            obj = float(fba_sol['obj']) #save the fba solution objective value
            out['obj'] = obj
            #model.reactions_list()
            lp = fba_sol['lp'] # get the lp for the solved fba problem
            for (i, reaction) in enumerate(model.reactions_list()):
                
                #set the bounds
                old_bounds = lp.cols[i].bounds
                lp.cols[i].bounds = 0.0
                
                #solve the FBA problem
                success = solve_simplex(lp, accept_unbound=True, verbose=False)
                if success:
                    #print reaction.name + str(float(lp.obj.value))
                    out['kos'].append(reaction.name, float(lp.obj.value))
                else:
                    out['kos'].append((reaction.name, 0.0))
                
                #reset the bounds
                lp.cols[i].bounds = old_bounds
                
        return out
        
        
    
    def double_knockout(self, model, maximize=True):
        '''
            Determines the essentiality of individual reactions for optimizing the objective function.
            
            First solves FBA for the full model.
            
            Then, for one reaction at a time, sets the upper and lower bound to zero and resolves the FBA. (A "simulated knockout")
            
            The ratio of the optimized objective function for the knockout / the original 
            is reported.  If the knockout causes the model to become unsolvable, 0.0 is reported
            
            returns a dict with the following members:
                obj: the optimal objective value of the fba solution
                kos: a list of 3-tuples where the first value is the reaction name of the first knockout, the second is the name of the second, and the third value is the KO/Full ratio as described above
            
            If the initial FBA is infeasible, returns None
        '''
        
        out = dict()
        out['kos'] = list()
        
        fba_sol = self.fba(model, maximize=maximize, return_lp=True)
        obj = fba_sol
        
        if not fba_sol:
            return None
        else:
            obj = fba_sol['obj'] #save the fba solution objective value
            out['obj'] = float(obj)
            model.reactions_list()
            lp = fba_sol['lp'] # get the lp for the solved fba problem
            for i1 in range(len(model.reactions_list())):
                # if (i1 % 50 == 0):
                    # print i1
                for i2 in range(i1, len(model.reactions_list())):
                    #set the bounds
                    old_bounds1 = lp.cols[i1].bounds
                    old_bounds2 = lp.cols[i2].bounds
                    lp.cols[i1].bounds = 0.0
                    lp.cols[i2].bounds = 0.0
                    
                    
                    #solve the FBA problem
                    success = solve_simplex(lp, accept_unbound=True, verbose=False)
                    if success:
                        #print lp.obj.value
                        out['kos'].append((model.reactions_list()[i1].name, model.reactions_list()[i2].name, float(lp.obj.value)/obj))
                    else:
                        out['kos'].append((model.reactions_list()[i1].name, model.reactions_list()[i2].name, 0.0))
                    
                    #reset the bounds
                    lp.cols[i1].bounds = old_bounds1
                    lp.cols[i2].bounds = old_bounds2
        return out
    

def solve_simplex(lp, accept_unbound=False, verbose=True):
    '''
        wrapper for lp.simplex(), catches and reports errors
        
        returns None and prints an error to stdout if there was an error or no solution
        else returns True
        in either case, lp is modified by the function
    '''
    _solver_errors = {
        'fault': 'There are no rows or columns, or the initial basis is invalid, or the initial basis matrix is singular or ill-conditioned.',
        'objll': 'The objective reached its lower limit.',
        'objul': 'The objective reached its upper limit.',
        'itlim': 'Iteration limited exceeded.',
        'tmlim': 'Time limit exceeded.',
        'sing': 'The basis matrix became singular or ill-conditioned.',
        'nopfs': 'No primal feasible solution. (Presolver only.)',
        'nodfs': 'No dual feasible solution. (Presolver only.)'
    }

    _solver_status = {
        'opt':'The solution is optimal.',
        'undef':'The solution is undefined.',
        'feas':'The solution is feasible, but not necessarily optimal.',
        'infeas':'The solution is infeasible.',
        'nofeas':'The problem has no feasible solution.',
        'unbnd':'The problem has an unbounded solution.'
    }
    
    err = lp.simplex()
    #print err
    #print lp.status
    #print lp.obj.value
    if err:
        if verbose: print _solver_errors[err]
        return None
    elif lp.status != 'opt' and ((not accept_unbound) or (accept_unbound and lp.status != 'unbnd')):
        if verbose: print _solver_status[lp.status]
        return None
    
    if lp.status == 'opt' and math.isnan(lp.obj.value):
        #print lp.obj.value
        # TODO:
        # There is a bit of an issue in that an lp where the objective function cannot carry any flux
        # for example if it contains a singlet
        # then lp.status == 'opt', but lp.obj.value != 0.0
        # instead, lp.obj.value is nan
        # this is generally not a problem, but it may be in some specific instances
        # I can't think of a good way to deal with this right now,
        # but it may become an issue (hopefully not)
        
        #print type(lp.obj.value)
        #lp.obj.value = 0.0 #this doesn't work... lp.obj.value is read-only
        pass
    
    return True
    
def bound_to_glpk_bound(bound, lower):
    '''
        converts infinite bounds to None
        lower whould be True if it is a lower bound, else False
    '''
    
    out = bound
    if bound == float('inf'):
            out = None
            if lower:
                print "Warning +inf lower bound in problem, it will be treated as -inf"
    if bound == float('-inf'):
            out = None
            if not lower:
                print "Warning -inf upper bound in problem, it will be treated as +inf"
    return out