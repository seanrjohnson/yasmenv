import gurobipy as gp
import math
from collections import OrderedDict, defaultdict
import pprint
import constants
import util
import scipy.sparse
import numpy as np
import exceptions

pp = pprint.pprint
gp.setParam("LogToConsole", 0)
# gp.setParam("ConcurrentMIP", 3)
# gp.setParam("MIPFocus", 3)

class Solver:
    def __init__(self):
        pass
    
    def spot(self, model, expression_field="Expression", allow_antifluxes=True, return_lp=False):
        '''
          The SPOT algorithms is described in:
          Kim M K, Lane A, Kelley J J, Lun D S (2016)
          PLoS ONE 11(6):e0157101
          
          This implementation allows a modification to the original algorithm.
                In SPOT, the first step is to convert reversible reactions into two irreversible reactions. The objective function is to maximize flux_vector*gene expression.
                irreversible reactions occur only once in the objective function because they are not split into two subreactions. But reversible reactions occur twice. Once
                for the forward reaction, and once for the reverse reaction. There is no mechanism in SPOT for preventing flux from occuring at the same time through the both
                the forward and reverse 
                
                To resterict flux to only one of the two irreversible reaction derived from a reversible reaction, set the option allow_antifluxes=False
          
          First converts the model into an irreversible model
          
          Then
          Solves the problem:
            Maximize: v*G
            
            Subject to:
              S*v = 0
              v >= 0
              magnitude(v)^2 <= 1
              
              optionally: at least one of each pair (vf[n], vr[n]) must be 0
            
            Where:
              S is the stoichiometric matrix
              v is the flux vector for the irreversible model
              v = [vf vr]
              vf is the forward reactions of the irreversible model
              vr is the reverse reactions of the irreversible model
              G is the vector of gene expression
              
            Precondition:
                In the input model, the upper bound must be >= 0, and the lower bound must be <= 0.
        '''
        
        model_in = model #save the original model
        m = model.copy()
        out = dict()
        
        rxn_map = None #declare
        (m, rxn_map) = m.make_irreversible() #recalculate the model as irreversible
        
        reverse_map = defaultdict(list)
        for (new_rxn,old_rxn) in rxn_map.items():
            #pp((new_rxn,old_rxn))
            reverse_map[old_rxn["name"]].append(new_rxn)
        
        (lp, rxn_vars) = self.fba_lp(m, maximize=True, model_name="SPOT2") #actually a quadratic program... but I'll keep the variable name lp
        
        expression = dict() #keys are reaction name, values are expression levels
        for r in m:
            exp = m[r].annotations.get(expression_field, "0.0")
            if exp.strip() == "":
                exp = 0.0
            try:
                exp = float(exp)
            except:
                raise ValueError("invalid value for expression " + str(exp))
            
            if exp < 0:
                raise ValueError("invalid value for expression " + str(exp))
                
            expression[r] = exp
        
        
        lp.addConstr(gp.quicksum([rxn * rxn for rxn in rxn_vars.values()]),gp.GRB.LESS_EQUAL, 1.0)
        lp.update()
        
        #### Restrict flux to just one of the new reactions in an irreversible pair
        if not allow_antifluxes:
            for (old_rxn, new_rxns) in reverse_map.items():
                component_rxn_vars = [rxn_vars[x] for x in new_rxns]
                lp.addSOS(gp.GRB.SOS_TYPE1, component_rxn_vars , (1,1))
            lp.update()
        
        obj = gp.quicksum([expression[rxn.name] * rxn_vars[rxn.name] for rxn in m.reactions_list()])
        
        lp.setObjective(obj)
        
        success = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution.  If there isn't a solution, None is returned.
        if not success:
            return None
        else:
            out['obj'] = lp.objVal
            out_fluxes = dict()
            for rxn in m.reactions_list():
                out_fluxes[rxn_map[rxn.name]['name']] = out_fluxes.get(rxn_map[rxn.name]['name'], 0.0) + rxn_map[rxn.name]['direction'] * rxn_vars[rxn.name].getAttr('X')
            
            out['fluxes'] = [(i.name, out_fluxes[i.name]) for i in model_in.reactions_list()]
            
            #pp(out['fluxes'])
        #(lp, rxn_vars) = self.fba_lp(model, maximize=maximize, model_name="fba")
        
        return out
        
    # def spot(self, model, expression_field="Expression", genes_field="Genes", bounds_threshold=1000, return_lp=False):
        # '''
          # The SPOT algorithms as described in:
          # Kim M K, Lane A, Kelley J J, Lun D S (2016)
          # PLoS ONE 11(6):e0157101
          
          # This implementation includes a modification to the original algorithm, where reactions that don't have any associated transcripts are not used in the objective function.
          
          # converts the model into an irreversible model, then
          
          # Solves the problem:
            # Maximize: v*G
            
            # Subject to:
              # S*v = 0
              # v >= 0
              # magnitude(v)^2 <= 1
            
            # Where:
              # S is the stoichiometric matrix
              # v is the flux vector for the irreversible model
              # G is the vector of gene expression
              
            # TODO: are additional constraints necessary to prevent forward and reverse reactions being used at the same time?
        # '''
        # model_in = model #save the original model
        # m = model.copy()
        # out = dict()
        
        # rxn_map = None #declare
        # (m, rxn_map) = m.make_irreversible() #recalculate the model as irreversible
        
        
        # (lp, rxn_vars) = self.fba_lp(model, maximize=maximize, model_name="fba")
        
        
        
        pass
    
    def e_flux(self, model, expression_field="Expression", genes_field="Genes", bounds_threshold=1000, return_lp=False, minimize_flux=False):
        '''
          The E-flux and E-flux2 algorithms as described in:
          Kim M K, Lane A, Kelley J J, Lun D S (2016)
          PLoS ONE 11(6):e0157101
          
          This implementation includes a modification to the original algorithm, where reactions that don't have any associated transcripts are left unbound.
          
          If minimize_flux=False, then the algorithm will be E-flux
          If minimize_flux=True, then the algorithm will be E-flux2
          
          predicts flux distributions from gene expression
          
          Solves the problem:
            Maximize: f'*v
            
            subject to:
              S*R = 0
              Aej <= Rj <= Bej
              
              where, for all j
              Aej = -Gj, if Aj < 0
              Aej = 0, if Aj >= 0
              Bej = Gj, if Bj > 0
              Bej = 0, if Bj <= 0
              
          Where:
            S is the stoichiometric matrix
            Gj is the expression level of reaction j
            Aj is the lower flux-bound of reaction j
            Bj us the upper flux-bound of reaction j
        '''
        
        m = model.copy()
        out = dict()
        m = m.make_infinite(-abs(bounds_threshold), abs(bounds_threshold))
        
        expression = dict() #keys are reaction name, values are expression levels
        for r in m:
            exp = m[r].annotations.get(expression_field, "0.0")
            if exp.strip() == "":
                exp = 0.0
            try:
                exp = float(exp)
            except:
                raise ValueError("invalid value for expression " + str(exp))
            
            if exp < 0:
                raise ValueError("invalid value for expression " + str(exp))
            
            expression[r] = exp
        
        #m = m.make_finite(smallest_lb=-1*max(expression.values()), largest_ub=max(expression.values()))
        
        for r in m:
            if m[r].annotations.get(genes_field, "").strip() != "":
                if m[r].ub == constants.inf:
                    m[r].ub = expression[r]
                if m[r].lb == constants.ninf:
                    m[r].lb = -1*expression[r]
        
        
        out = self.fba(m, maximize=True, return_lp=return_lp, minimize_flux=minimize_flux, flux_minimization_style="sum_of_squares")
        #out = self.fba(m, maximize=True, return_lp=return_lp, minimize_flux=minimize_flux, flux_minimization_style="sum")
        
        return out
      
      
      
      
    
    def e_fmin(self, model, biomass_reaction, expression_field="Expression", e=1, return_lp=False):
        '''
          An implementation of the E-Fmin algorithm
          Song H-S, Reifman J, Wallqvist A (2014) 
          Prediction of Metabolic Flux Distribution from Gene Expression Data Based on the Flux Minimization Principle. 
          PLoS ONE 9(11): e112524. doi:10.1371/journal.pone.0112524
          
          predicts flux distributions from gene expression
          
          Solves the problem:
            Minimize: sum( Wi*abs(Ri) )
            
            Subject to: S*R = 0
            Rl <= R <= Ru
            Rb >= e
            
            where:
              Rl and Ru are the flux lower and upper bounds.
              Rb is the flux through the biomass reaction
              e is an arbitrarily small number (it ensures that there is flux into the known metabolic products)
              Wi is a weight specific to a reaction, and calculated by: 
                gi = (gene expression for reaction i) / (the largest gene expression for any reaction)
                Wi = -gi + 1
          
            
          Considerations:
            Reactions without any associated genes are considered to have an expression of zero. The result is that pathways with more complete annotation will tend to be preferred.
            If any fluxes are known experimentally, their bounds can be fixed, to force flux through those reactions.
            
          input:
            model: a yasmenv model
            biomass_reaction: The reaction that will be given a flux bound, e. 
            expression_field: the annotation field to use as the basis for generating 
            e: the bound for the biomass reaction. If e is positive, it will be a lower bound, and the upper bound will be set to inf. If e is negative, it will be an upper bound, and the lower bound will be set to -inf.

          output:
            
          
          TODO: implement a FVA version of this.
        '''
        
        m = model.copy()
        out = dict()
        
        if biomass_reaction in m:
          if e < 0:
            m[biomass_reaction].lb = constants.ninf
            m[biomass_reaction].ub = e
          else:
            m[biomass_reaction].lb = e
            m[biomass_reaction].ub = constants.inf
        else:
          raise ValueError("biomass_reaction reaction name passed to e_fmin is not contained in the model")
        
        (m_irrev, rxn_map) = m.make_irreversible()
        
        expression = dict() #keys are reaction name, values are expression levels
        for r in m:
          exp = m[r].annotations.get(expression_field, "0.0")
          if exp.strip() == "":
            exp = 0.0
          try:
            exp = float(exp)
          except:
            raise ValueError("invalid value for expression " + str(exp))
          
          if exp < 0:
            raise ValueError("invalid value for expression " + str(exp))
            
          expression[r] = exp
          
        max_exp = max(expression.values())
        
        (lp, rxn_vars) = self.fba_lp(m_irrev, model_name="e_fmin")
        
        
        ### set the e-fmin objective function
        obj = gp.quicksum([(((-1.0 * expression[rxn_map[rxn.name]['name']] / max_exp) + 1) * rxn_vars[rxn.name]) for (i, rxn) in enumerate(m_irrev.reactions_list())])
        
        
        #this will overwrite whatever objective function it had before
        lp.setAttr('ModelSense',gp.GRB.MINIMIZE) #e-fmin is a minimization
        #print obj
        lp.setObjective(obj)
        
        success = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution. If there isn't a solution, None is returned.
        if success:
          out['obj'] = lp.objVal
          out_fluxes = dict()
          for (i, rxn) in enumerate(m_irrev.reactions_list()):
            out_fluxes[rxn_map[rxn.name]['name']] = out_fluxes.get(rxn_map[rxn.name]['name'], 0) + rxn_map[rxn.name]['direction'] * rxn_vars[rxn.name].getAttr('X')
            
          out['fluxes'] = [(i.name, out_fluxes[i.name]) for i in m.reactions_list()]
        else:
            print "Warning: failure in e-fmin LP optimization"
            return None
        
        if return_lp:
            out['lp'] = lp
            out['rxn_vars'] = rxn_vars
       
        return out
        
    #@profile
    def frank(self, model, ignored_reactions=list(), bound_magnitude=1000, linear_objective=False, minimize_flux=False, find_all=False, expression_field="Expression"):
        '''
            Flux-rank optimization. Given a set of expression values, orders the reactions by gene expression value,
            and finds a flux vector such that the euclidean distance between the ranked fluxes and the ranked
            gene expression is minimized.
            if linear_objective=True: find the minimal taxicab distance between ranks rather than the minimal
                euclidean distance
            
            The problem is implemented as a branch and bound algorithm with LP subproblems, and a linear objective function.
            
            minimize_flux not yet implemented
        
        
            TODO: bound_magnitude might not be necessary, absolute values could be implemented as SOS1 instead
                of absolute values.  Also, I'm not sure if it would ever be useful, but there's no reason rank weights have to be limited
                to the 1 or 2 norm. They could really be any function of 2 variables.
                            
            TODO: decide if the weight matrix handles rank ties like it should. Right now, does not handle ties in any special way, so a reaction in a tied expression
            always has a positive weight, the other way to do this would be to make reactions with tied expression have no weight at any of the tied positions
        '''
        out = dict()
        ignored_reactions = set(ignored_reactions)
        bound_magnitude = abs(bound_magnitude)
        

        
        if bound_magnitude == constants.inf:
            bound_magnitude = 1000
        
        # # #first make the model finite so we can implement absolute values
        model = model.make_finite(smallest_lb=-bound_magnitude, largest_ub=bound_magnitude)
    
        # # #get the fba problem formulation, we'll overwrite the objective function later.
        (qp, rxn_vars) = self.fba_lp(model, model_name="frank")
        
        # # #rank reactions according to expression
        expression_levels = dict() #keys are expression values, values are how many reactions have that expression value
        for r in model:
            if r not in ignored_reactions:
                lv = model[r].annotations.get(expression_field, None)
                if lv is None:
                    lv = 0.0
                expression_levels[lv] = expression_levels.get(lv, 0) + 1
        expression_hierarchy = sorted(expression_levels.keys())
        expression_hierarchy.reverse() #a greatest to least sorted list of expression levels
        expression_to_rank = dict() #keys are expression, values are the rank of any reaction with that expression level
        so_far = 0
        ranks_combined = dict() #a dict of sets. Keys are expression levels, values are sets of possible flux rankings that a reaction can take with no cost
        # that is: the ranking space that the reaction and all reactions with the same expression level occupy.
        for (position, lv) in enumerate(expression_hierarchy):
            expression_to_rank[lv] = so_far + 0.5 * (expression_levels[lv]-1)
            ranks_combined[lv] = {so_far + x for x in xrange(0, expression_levels[lv])}
            so_far += expression_levels[lv]
        expression_rank = list() #list of tuples: (name, rank)
        tied_rank_positions = dict() #keys are reaction names, values are sets of ranks that the reaction could take with no cost
        absolute_expression = OrderedDict()
        for r in model:
            if r not in ignored_reactions:
                lv = model[r].annotations.get(expression_field, None)
                if lv is None:
                    lv = 0.0
                expression_rank.append((r, expression_to_rank[lv]))
                absolute_expression[r] = lv
                tied_rank_positions[r] = ranks_combined[lv]
        #ranks are from 0 to number_of_ranked_reactions - 1 inclusive
        
        # # # sort by name, then by rank, each low to high
        expression_rank = sorted(expression_rank, key=lambda x: x[0]) #name
        expression_rank = sorted(expression_rank, key=lambda x: x[1]) #rank
        ranked_len = len(expression_rank)
        
        list_of_indexes = np.arange(0, ranked_len)
        
        # # # Define possible rank difference weighting schemes
        def euclidean_ranking(expression_ranking, flux_ranking):
            return (expression_ranking - flux_ranking)**2
        def taxicab_ranking(expression_ranking, flux_ranking):
            return abs(expression_ranking - flux_ranking)
        
        # # # Select the appropriate ranking function
        if linear_objective:
            rank_func = taxicab_ranking
        else:
            rank_func = euclidean_ranking
        
        # # # generate the weight matrix
        weights = np.matrix(np.zeros((ranked_len, ranked_len)))
        for i in xrange(ranked_len):
            for j in xrange(ranked_len):
                weights[i,j] = rank_func(expression_rank[i][1], j)
        
        # # # add flux magnitude variables to lp
        abs_flux = list()
        x_plus = list()
        x_minus = list()
        bin_var = list()
        for r in xrange(ranked_len):
            x_plus.append(None)
            x_minus.append(None)
            bin_var.append(qp.addVar(lb=0, ub=1, obj=0, vtype=gp.GRB.BINARY))
            if model[expression_rank[r][0]].ub <= 0:
                x_plus[r] = qp.addVar(lb=0.0, ub=0.0, obj=0, vtype=gp.GRB.CONTINUOUS)
            else:
                x_plus[r] = qp.addVar(lb=0.0, ub=model[expression_rank[r][0]].ub, obj=0, vtype=gp.GRB.CONTINUOUS)
            if model[expression_rank[r][0]].lb >= 0:
                x_minus[r] = qp.addVar(lb=0.0, ub=0.0, obj=0, vtype=gp.GRB.CONTINUOUS)
            else:
                x_minus[r] = qp.addVar(lb=0.0, ub=-model[expression_rank[r][0]].lb, obj=0, vtype=gp.GRB.CONTINUOUS)
            abs_flux.append(qp.addVar(lb=0.0, ub=max(abs(model[expression_rank[r][0]].lb), abs(model[expression_rank[r][0]].ub)), obj=0, vtype=gp.GRB.CONTINUOUS))
        qp.update() #updating is slow, so do it outside of a loop
        for r in xrange(ranked_len):
            qp.addConstr(x_plus[r], gp.GRB.LESS_EQUAL, model[expression_rank[r][0]].ub*bin_var[r])
            qp.addConstr(x_minus[r], gp.GRB.LESS_EQUAL, -model[expression_rank[r][0]].lb*(1-bin_var[r]))
            qp.addConstr(abs_flux[r], gp.GRB.EQUAL, x_minus[r] + x_plus[r])
            qp.addConstr(rxn_vars[expression_rank[r][0]], gp.GRB.EQUAL, x_plus[r] - x_minus[r])
        
        # # # add order constraints (all zero for now, but will be filled in according to each node)
        #turns out you can't actually modify the variable coefficients of a constraint with the Gurobi api, so we'll have to create and destroy them over and over. Hope it doesn't impact performance too badly.
        # order_constraints = list()
        # for r in xrange(ranked_len-1):
            # order_constraints.append(qp.addConstr(0, gp.GRB.GREATER_EQUAL, 0))
        
        # # # set objective function to zero. We don't have a particular objective 
        qp.setObjective(0)
        
        # # # initialize branch and bound search
        queue = list() #list of tuples (node, next_position)
        queue.append((np.ones(ranked_len, dtype=np.int)*-1, 0))
        solution = None
        upper_bound = ranked_len * weights.max() + 1 #some number larger than the objective function of any possible solution
        while len(queue) > 0:
            (node, next) = queue.pop()
            print ""
            print node
            node_lb = self._frank_weight_lb(node, next, weights)
            if node_lb < upper_bound and self._frank_feasible(node, qp, abs_flux) != 'infeasible':
                #possible solution
                print str(upper_bound)
                print str(node_lb)

                if (next == ranked_len):
                    # a complete path through the tree
                    solution = node
                    upper_bound = node_lb
                else:
                    rows = np.ones(node.shape[0]) == 1
                    rows[node[0:next]] = False
                    next_next = next+1
                    for r in list_of_indexes[rows][::-1]: #go through all remaining 
                        new_node = np.array(node, copy=True)
                        new_node[next] = r
                        queue.append((new_node, next_next))
        
        
        return solution
    #@profile
    def _frank_feasible(self, node, lp, fluxes, delta=0.001): #delta is for avoiding numerical difficulties over tiny fluxes
        '''
            
        '''
        constraints = list()
        if node[0] == -1:
            # there are no rank constraints
            pass
            #do nothing
        else:
            flux_indexes = set(xrange(len(fluxes)))
            #next_constr = 0
            for i in xrange(node.shape[0]-1): #the last flux can't have any constraints, so only iterate to it
                flux_indexes.remove(node[i])
                if node[i+1] != -1: #it's not the last flux with a fixed rank, so make it 
                    constraints.append(lp.addConstr(fluxes[node[i]] + delta, gp.GRB.GREATER_EQUAL, fluxes[node[i+1]]))
                    #print str(node[i]) + " >= " + str(node[i+1])
                else:
                    for j in flux_indexes:
                        constraints.append(lp.addConstr(fluxes[node[i]] + delta, gp.GRB.GREATER_EQUAL, fluxes[j]))
                        #print str(node[i]) + " >= " + str(j)
                    break
        
        sol = solve_simplex(lp, accept_infeasible=True)
        
        
        #remove the constraints that were made
        for c in constraints:
            lp.remove(c)
        
        return sol
        
    
    def _frank_weight_lb(self, node, next_pos, weights):
        '''
            given a node from a frank problem, calculate the lower_bound for the objective function for the tree starting with that node.
        '''
        sum = 0
        sum += weights[node[0:next_pos], np.arange(0, next_pos)].sum() #get the weights of the reactions with rigidly placed flux ranks
        
        #subset the matrix to remove rows and columns that have already been accounted for
        rows = np.ones(node.shape[0]) == 1
        rows[node[0:next_pos]] = False
        matrix_subset = weights[rows,:][:,next_pos:]
        sum += matrix_subset.trace().sum()

        
        return sum
        
    
    def bit_scan(self, model, ignored_reactions=list(), maximize=True, annotation_to_scan='bits'):
        '''
            run FBA on a series of subsets of a model. Report the criterion used and the resulting value of the objective function.
            
            maximize: True if the objective function should be maximized, else False
            annotation_to_scan: the name of the annotation field to scan accross. The annotations should be numbers. Scanning will be from low to high
            ignored_reactions: reactions to include in every subset regardless of their annotations.
            
            Algorithm:
                For every value of the selected annotation in the model:
                    set the bounds of every reaction with a lower value of the annotation to zero, disabling the reaction
                    run FBA
                    store the result
                    (note that since we are moving from low to high for the annotation values, bounds do not need to be reset after
                    the FBA)
        '''
        ignored_reactions = set(ignored_reactions)
        (lp, rxn_vars) = self.fba_lp(model, maximize=maximize, model_name="fba")
        
        annotation_values = list()
        for r in model:
            if (r not in ignored_reactions):
                annotation_values.append(float(model[r].annotations[annotation_to_scan]))
        
        annotation_values = list(set(annotation_values)) #remove duplicates
        annotation_values = sorted(annotation_values) # sort low to high
        annotation_values.append(annotation_values[-1] + 1) # add one higher than the highest entry so that there will be a trial with all reactions set to zero
        
        out = {'value':list(), 'objective':list()}
        for av in annotation_values:
            #print
            #print av
            for r in model:
                if r not in ignored_reactions:
                    if float(model[r].annotations[annotation_to_scan]) < av:
                        #print r
                        rxn_vars[r].lb = 0.0
                        rxn_vars[r].ub = 0.0
            sol = solve_simplex(lp, accept_unbound=True)
            out['value'].append(av)
            if sol == 'unbound':
                out['objective'].append('unbound')
            elif sol is None:
                out['objective'].append('infeasible')
            else:
                out['objective'].append(lp.objVal)
            if (sol is None) or (lp.objVal == 0.0):
                break #if this doesn't work, none of the rest will either
        
        return zip(out['value'], out['objective'])
    
    def fba(self, model, maximize=True, return_lp=False, minimize_flux=False, flux_minimization_style="sum"):
        '''
            Flux balance analysis on the input model.
            
            maximize: True if the objective function should be maximized, false if it should be minimized
            return_lp: True if the returned data structure should include the gurobipy.Model() object used to solve the FBA
            minimize_flux: If True, a secondary LP will be solved using the objective value for the first LP as a constraint.
                            In the new LP, the objective function will be set to minimize the total flux (and thus avoid huge fluxes from loops)
            flux_minimization_style: Can equal "sum" or "sum_of_squares". If "sum" is selected
            
            if an optimal solution is found:
                returns a dict with the following members:
                    obj: the value of the 
                    fluxes: list of 2-tuples where the first value is the reaction name, and the second is the flux
                    lp: (optional) the lp structure generated for the problem
            else:
                prints an error message and returns None
            
            NOTE: if sum_of_squares minimization fails, try increasing the tolerance in the variable: lp.params.BarConvTol
        ''' 
        out = dict()
        
        model_in = model #save the original model
        rxn_map = None #declare
        if minimize_flux:
            if flux_minimization_style != "sum" and flux_minimization_style != "sum_of_squares":
                raise exception.ValueError("flux_minimization style must be either sum or sum_of_squares")
            if flux_minimization_style == "sum":
                (model, rxn_map) = model.make_irreversible() #recalculate the model as irreversible if we're going to minimize flux
        (lp, rxn_vars) = self.fba_lp(model, maximize=maximize, model_name="fba")
        
        
        success = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution.  If there isn't a solution, None is returned.
        if not success:
            return None
        elif (not minimize_flux):
            out['obj'] = lp.objVal
            out['fluxes'] = [(x, rxn_vars[x].getAttr('X')) for x in rxn_vars] #'X' is the value of the variable in the current solution, presumably an optimal solution if it's got this far.
            #pp(out['fluxes'])
        else:
            out['obj'] = lp.objVal
            
            #fix the objective to the first solution as a constraint
            obj_constr = gp.quicksum([rxn.objective*rxn_vars[rxn.name] for rxn in model.reactions_list()])
            lp.addConstr(obj_constr, gp.GRB.EQUAL, out['obj'])
            
            #reset the objective function to ones
            if flux_minimization_style == "sum":
                for (name, x) in rxn_vars.iteritems():
                    x.obj = 1.0
            elif flux_minimization_style == "sum_of_squares":
                obj = gp.quicksum([rxn_vars[rxn.name] * rxn_vars[rxn.name] for (i, rxn) in enumerate(model.reactions_list())])
                lp.setObjective(obj)
                #lp.params.ObjScale = -0.05
                lp.params.BarConvTol  = 0.000001 #Adjust if solution fails

            
            #set the LP to be a minimization
            lp.setAttr('ModelSense',gp.GRB.MINIMIZE)
            
            success2 = solve_simplex(lp) #lp is modified by solve_simplex to contain the solution.  If there isn't a solution, None is returned.
            if success2:
                out_fluxes = dict()
                if flux_minimization_style == "sum":
                    for (i, rxn) in enumerate(model.reactions_list()):
                        out_fluxes[rxn_map[rxn.name]['name']] = out_fluxes.get(rxn_map[rxn.name]['name'], 0) + rxn_map[rxn.name]['direction'] * rxn_vars[rxn.name].getAttr('X')
                
                    out['fluxes'] = [(i.name, out_fluxes[i.name]) for i in model_in.reactions_list()]
                elif flux_minimization_style == "sum_of_squares":
                    out['fluxes'] = [(x, rxn_vars[x].getAttr('X')) for x in model_in]
            else:
                print "Warning: failure in secondary LP optimization"
                return None
        
        if return_lp:
            out['lp'] = lp
            out['rxn_vars'] = rxn_vars
        #print lp.status
        #print lp.obj.value
        
        return out

    def fva(self, model, objective_value=None, loop_law=False):
        '''
            Flux variability analysis on the input model
            
            if objective_value is supplied, the objective function in the model is fixed to that value as a constraint,
            otherwise the objective (biomass) coefficients are ignored and fva calculates the flux carrying capacities of the
            reactions in the model regardless of biomass objective
            
            TODO: implement loop law
            
            returns a dict with the following members:
                fluxes: a list of 3-tuples where the first member is the reaction name, the second is the minimum flux, and the third is the maximum.
                    (-)inf means unbounded below
                    (+)inf means unbounded above
                    
            in case of error:
                returns None
        '''
        out = dict()
        out['fluxes'] = list()
        (lp, rxn_vars) = self.fba_lp(model, maximize=False, model_name="fva")
        
        #if there was an objective value specified, make a constraint for it
        if objective_value is not None:
            obj_constr = gp.quicksum([model[rxn].objective*rxn_vars[rxn] for rxn in model if model[rxn].objective != 0])
            lp.addConstr(obj_constr, gp.GRB.EQUAL, objective_value)
        
        
        #reset the objective function to zeros
        #qp.setObjective(0)
        
        #find maxima and minima for each reaction
        #continually reuse the same lp object, thereby "warmstarting", using the previous solution as an initial basis for the 
        #next optimization.  This results in fast execution times
        for r in model:
            # set the objective function to the current reaction
            lp.setObjective(rxn_vars[r])
            min = None
            max = None
            
            #solve the minimization
            lp.setAttr('ModelSense',gp.GRB.MINIMIZE)
            success = solve_simplex(lp, accept_unbound=True, verbose=False)
            if success:
                if (success == 'unbound'):
                    min = '(-)inf'
                else:
                    min = lp.objVal

            #solve the maximization
            lp.setAttr('ModelSense',gp.GRB.MAXIMIZE)
            success = solve_simplex(lp, accept_unbound=True, verbose=False)
            if success:
                if (success == 'unbound'):
                    max = '(+)inf'
                else:
                    max = lp.objVal
            
            out['fluxes'].append((r, min, max))
            #print (r, min, max)
        return out

    def single_knockout(self, model, maximize=True):
        '''
            Determines the essentiality of individual reactions for optimizing the objective function.
            
            First solves FBA for the full model.
            
            Then, for one reaction at a time, sets the upper and lower bound to zero and resolves the FBA. (A "simulated knockout")
            
            The value of the objective function for each of the knockouts is reported.
            
            
            returns a dict with the following members:
                obj: the optimal objective value of the fba solution
                kos: a list of 2-tuples where the first value is the reaction name, and the second value is the objective function for the knockout 
            
            If the initial FBA is infeasible, returns None
        '''
        
        out = dict()
        out['kos'] = list()
        
        fba_sol = self.fba(model, maximize=maximize, return_lp=True)
        obj = fba_sol
        
        if not fba_sol:
            return None
        else:
            obj = float(fba_sol['obj']) #save the fba solution objective value
            out['obj'] = obj
            lp = fba_sol['lp'] # get the lp for the solved fba problem
            rxn_vars = fba_sol['rxn_vars']
            for reaction in model.reactions_list():
                
                #set the bounds
                old_bounds = (rxn_vars[reaction.name].lb , rxn_vars[reaction.name].ub)
                (rxn_vars[reaction.name].lb , rxn_vars[reaction.name].ub) = (0.0, 0.0)
                lp.update()
                #solve the FBA problem
                success = solve_simplex(lp, accept_unbound=True, verbose=False)
                if success:
                    #print reaction.name + str(float(lp.obj.value))
                    out['kos'].append((reaction.name, float(lp.objVal)))
                else:
                    out['kos'].append((reaction.name, 0.0))
                
                #reset the bounds
                (rxn_vars[reaction.name].lb , rxn_vars[reaction.name].ub) = old_bounds
                lp.update()
        return out
    
    def double_knockout(self, model, maximize=True):
        '''
            Determines the essentiality of individual reactions for optimizing the objective function.
            
            First solves FBA for the full model.
            
            Then, for one reaction at a time, sets the upper and lower bound to zero and resolves the FBA. (A "simulated knockout")
            
            The ratio of the optimized objective function for the knockout / the original 
            is reported.  If the knockout causes the model to become unsolvable, 0.0 is reported
            
            returns a dict with the following members:
                obj: the optimal objective value of the fba solution
                kos: a list of 4-tuples where the first value is the reaction name of the first knockout, the second is the name of the second, the third value is the KO objective value, and the fourth is the KO/Full ratio as described above
                NOTE: When interpreting the results, pay attention to whether you are doing a maximization or a minimzation. 
            If the initial FBA is infeasible, returns None
        '''
        out = dict()
        out['kos'] = list()
        
        fba_sol = self.fba(model, maximize=maximize, return_lp=True)
        obj = fba_sol
        
        if not fba_sol:
            return None
        else:
            obj = float(fba_sol['obj']) #save the fba solution objective value
            out['obj'] = obj
            lp = fba_sol['lp'] # get the lp for the solved fba problem
            rxn_vars = fba_sol['rxn_vars']
            for r1 in model.reactions_list():
                #set the bounds
                old_bounds1 = (rxn_vars[r1.name].lb , rxn_vars[r1.name].ub)
                (rxn_vars[r1.name].lb , rxn_vars[r1.name].ub) = (0.0, 0.0)
                lp.update()
                for r2 in model.reactions_list():
                    #set the bounds
                    old_bounds2 = (rxn_vars[r2.name].lb , rxn_vars[r2.name].ub)
                    (rxn_vars[r2.name].lb , rxn_vars[r2.name].ub) = (0.0, 0.0)
                    lp.update()
                    #solve the FBA problem
                    success = solve_simplex(lp, accept_unbound=True, verbose=False)
                    if success:
                        #print r1.name + str(float(lp.objVal))
                        out['kos'].append((r1.name, r2.name, float(lp.objVal), float(lp.objVal)/float(obj)))
                        #out['kos'].append((r1.name, float(lp.objVal)))
                    else:
                        out['kos'].append((r1.name, r2.name, 0.0, 0.0))
                    
                    #reset the bounds
                    (rxn_vars[r2.name].lb , rxn_vars[r2.name].ub) = old_bounds2
                    lp.update()
                #reset the bounds
                (rxn_vars[r1.name].lb, rxn_vars[r1.name].ub) = old_bounds1
                lp.update()
        return out
    # def add_loop_law_constraints(self, lp, rxn_vars, model, ):
        # '''
            # add loop law constraints to an FBA model such that it is turned into a MILP, where internal flux loops are not allowed.
            
            # Preconditions:
                # lp should be an fba style model object, such as one output by fba_lp.
                # model should be a yasmenv.model.Model object, and should be the model that lp was derived from
                # in model, all boundary fluxes should be of the form: 
                # metabolite <=> 
                # or
                # <=> metabolite
                # that is: there should be only one metabolite on one side, and none on the other.
            
            
            # Reference: Biophys J. 2011 Feb 2;100(3):544-53. doi: 10.1016/j.bpj.2010.12.3707.
            # Loosely based on Schellenberger's implementation in the COBRA MATLAB toolbox
        # '''
        
        # #find all internal reactions
        # S = scipy.sparse.lil_matrix()
        # for (met, rxn, stoich) in model.sparse_matrix():
            # S[met, rxn] = stoich
        # S = S.todense() #TODO: this might not be necessary
        
        # rxn_names = np.array(model.reaction_names())
        
        # internal_indexes = (S != 0).sum(0) > 1 #boolean numpy array
        # internal = S[:,internal_indexes] #internal should be all of the reactions with more than one substrate
        
        # internal_rxn_names = rxn_names[internal_indexes]
        
        # internal_null = util.null(internal)
        
        # loop_bools = dict() #a sub i in the paper
        # rxn_gs = dict() #G sub i in the paper
        # for r in internal_rxn_names:
            # loop_vars[r] = lp.addVar(lb=0, ub=1, obj=0, vtype=gp.GRB.BINARY)
            # rxn_gs[r] = lp.addVar(lb=-GMAX, ub=GMAX, obj=0, vtype=gp.GRB.CONTINUOUS)
        # lp.update()
        
        # for r in internal_rxn_names:
            # lp.addConstr(rxn_gs[r], gp.GRB.LESS_EQUAL, loop_bools[r], name)
        
        ### ON HOLD FOR NOW. BASED ON THE MATLAB IMPLEMENTATION AND THE PAPER, IT LOOKS LIKE THIS WILL TAKE QUITE A BIT OF EFFORT TO DO CORRECTLY ###
        
        
    def fba_lp(self, model, maximize=True, model_name="fba"):
        '''
            return a Gurobi problem object representing the fba problem for the model
        '''
        lp = gp.Model(model_name)
        #lp.setParam(gp.GRB.param.Presolve, 0)
        
        # lp.obj.maximize = maximize

        rxn_vars = OrderedDict()
        
        for rxn in model.reactions_list():
            rxn_vars[rxn.name] = lp.addVar(lb=bound_to_gurobi_bound(rxn.lb, True), ub=bound_to_gurobi_bound(rxn.ub, False), obj=rxn.objective, vtype=gp.GRB.CONTINUOUS, name=rxn.name)
        lp.update()
        
        
        stoichiometries = model.reaction_column_coefficients()        
        #pp(stoichiometries)
        lhs = dict() #keys are metabolite names, values are gurobi linear expressions
        constraints = list()
        for rxn in model.reactions_list():
            for met in stoichiometries[rxn.name].keys():
                lhs[met] = lhs.get(met, 0.0) + stoichiometries[rxn.name][met]*rxn_vars[rxn.name]
            #pp([stoichiometries[rxn.name][x]*rxn_vars[rxn.name] for x in stoichiometries[rxn.name].keys()])
        for (name, c) in lhs.iteritems():
            #print c
            constraints.append(lp.addConstr(c,gp.GRB.EQUAL, 0.0, name))
        lp.update()
        #pp(constraints)
        
        sense = gp.GRB.MINIMIZE
        if maximize:
            sense = gp.GRB.MAXIMIZE
        lp.setAttr('ModelSense',sense)
        
        return lp, rxn_vars
        
        
    def moma(self, model, target_flux, fba_optimize_constraint=True, fba_obj_maximize=True, scale=None):
        '''
            the MOMA problem is:
                The objective is to minimize the sum of of the squares of the 
                distances between the solution vector and 'target_flux'.
                In other words, to minimizing the euclidean distance between the vectors.
            
            if fba_optimize_constraint is True (default),
                an initial FBA problem will first be run on the model,
                and a constraint added requiring the Moma flux to also
                optimize the models objective function
            
            The solution vector should conform to the same constraints as it would for a 
            flux balance analysis problem
            
            minimize    euclidean_norm(x-target_flux)
            subject to  x >= lb
                        x <= ub
                        A*x = 0
                
            A*x = b
            
            
            #TODO: Add options for scaling.  Two possibilities are to scale by target flux or do two iterations of the problem,
                and for the second iteration scale by the results from the first.
        '''
        out = dict()

        (qp, rxn_vars) = self.fba_lp(model, model_name="moma")
        
        if fba_optimize_constraint:
            fba_sol = self.fba(model, maximize=fba_obj_maximize)
            out['fba_obj'] = fba_sol['obj']
            #fix the objective to the first solution as a constraint
            obj_constr = gp.quicksum([rxn.objective*rxn_vars[rxn.name] for rxn in model.reactions_list()])
            qp.addConstr(obj_constr, gp.GRB.EQUAL, fba_sol['obj'])
        
        ### set the moma objective function
        obj = gp.quicksum([(target_flux[i] - rxn_vars[rxn.name]) * (target_flux[i] - rxn_vars[rxn.name]) for (i, rxn) in enumerate(model.reactions_list())])   #SUM OF SQUARES
        # note this formulation of the objective function should be preferred over the formulation from the original paper, because Gurobi (5.5) seems to handle it better and give
        # better values for the objective and fluxes closer to the targets.  I believe this is due somehow to implementation details internal to Gurobi.  Changing various Gurobi parameters
        # may give even tighter solutions.
        
        #this will overwrite whatever objective function it had before
        qp.setAttr('ModelSense',gp.GRB.MINIMIZE) #MOMA is a minimization
        #print obj
        qp.setObjective(obj)
        
        qp.optimize()
        
        residual_sum = 0.0
        for (i, v) in enumerate(qp.getVars()):
           residual = (target_flux[i] - v.x)**2
           residual_sum += residual
           #print v.varName + "\t" + str(v.x) + "\t" + str(residual)
        out['fluxes'] = [(x, rxn_vars[x].getAttr('X')) for x in rxn_vars]
        out['obj'] = math.sqrt(residual_sum)
        
        return out

def solve_simplex(lp, accept_unbound=False, accept_infeasible=False, verbose=True):
    '''
        wrapper for lp.optimize(), catches and reports errors
        
        returns None and prints an error to stdout if there was an error or no solution
        else returns True
        in either case, lp is modified by the function
    '''
    
    ## TODO: switch these to Gurobi messages
    # _solver_errors = {
        # 'fault': 'There are no rows or columns, or the initial basis is invalid, or the initial basis matrix is singular or ill-conditioned.',
        # 'objll': 'The objective reached its lower limit.',
        # 'objul': 'The objective reached its upper limit.',
        # 'itlim': 'Iteration limited exceeded.',
        # 'tmlim': 'Time limit exceeded.',
        # 'sing': 'The basis matrix became singular or ill-conditioned.',
        # 'nopfs': 'No primal feasible solution. (Presolver only.)',
        # 'nodfs': 'No dual feasible solution. (Presolver only.)'
    # }

    # _solver_status = {
        # 'opt':'The solution is optimal.',
        # 'undef':'The solution is undefined.',
        # 'feas':'The solution is feasible, but not necessarily optimal.',
        # 'infeas':'The solution is infeasible.',
        # 'nofeas':'The problem has no feasible solution.',
        # 'unbnd':'The problem has an unbounded solution.'
    # }
    
    lp.optimize()
    out = "bound"
    #lp.write("Test_model.mps")
    #from the gurobi example file lp.py
    if lp.status == gp.GRB.status.INF_OR_UNBD:
        # Turn presolve off to determine whether model is infeasible
        # or unbounded
        lp.setParam(gp.GRB.param.Presolve, 0)
        lp.optimize()
        lp.setParam(gp.GRB.param.Presolve, 1) #turn the presolver back on
    if lp.status == gp.GRB.status.INFEASIBLE:
        if not accept_infeasible:
            if verbose: print "Infeasible" #TODO: looks like Gurobi reports this on it's own, figure out how to shut it up
            return None
        else:
            out = 'infeasible'
    elif lp.status == gp.GRB.status.UNBOUNDED:
        if not accept_unbound:
            if verbose: print "Unbounded"
            return None
        else:
            out = "unbound"
    elif lp.status != gp.GRB.status.OPTIMAL:
        if verbose: print "Gurobi error"
        return None

    return out


    
def bound_to_gurobi_bound(bound, lower):
    '''
        converts infinite bounds to None
        lower whould be True if it is a lower bound, else False
    '''
    
    out = bound
    if bound == float('inf'):
            out = float('inf')
            if lower:
                print "Warning +inf lower bound in problem, it will be treated as -inf"
    if bound == float('-inf'):
            out = float('-inf')
            if not lower:
                print "Warning -inf upper bound in problem, it will be treated as +inf"
    return out