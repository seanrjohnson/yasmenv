YASMEnv
=======
yet another stoichiometric modeling environment

A set of tools for creating and using stoichiometric models of cellular metabolism

By Sean R Johnson
***

The goal of this project is to provide an easy interface to common stoichiometric modeling tasks.

The focus of YASMEnv is semi-automated model creation, using tab separated model files as intermediates. YASMEnv can also perform simulation tasks such as Flux Balance Analysis, Flux Variability Analysis, and others.

A disadvantage of YASMEnv is that it relies heavily on GUROBI as the solver, and doesn't have modular support for different solvers.

Lots of examples of code using YASMEnv can be found [at another repository](https://bitbucket.org/seanrjohnson/menpigt_2015).

Current stable version is 0.2, I recommend using that one. You can find it [Here](https://bitbucket.org/seanrjohnson/yasmenv/src/639dd6f). And download it [Here](https://bitbucket.org/seanrjohnson/yasmenv/get/639dd6f.zip).
 
***
Dependencies:
YASMEnv is modularized, so if you don't use a particular functionality, you don't need to
have the dependency library.

* rdflib (reading biopax databases)
* pandas (reading delimited text files, and munging data)
* Gurobi (simulation algorithms, FBA, FVA, MOMA, knockouts, e-fmin, etc.)
* pyglpk (an alternative to Gurobi for some linear programming tasks)
* COBRA for Python (for sbml export, YASMEnv models can be converted into COBRA models)
* optGpSampler (ACHR stochastic sampling)

***
The model file format for YASMEnv is descended from the format used by the COBRA Toolbox for Excel files.
The file is a delimited text file where the first row contains column titles.
There are 5 mandatory columns (additional columns will be read and stored as 'annotations'. Be sure each column has a discrete name though)


mandatory columns(default title):

* name(Rxn name)
* lower bound(LB)
* upper bound(UB)
* objective function coefficient(Objective)
* reaction formula(Formula)

reactions bounds can be unbounded.  For unbounded lower bounds use (-)inf in the lower bound column
for unbounded upper bounds, use (+)inf in the upper bound column.

