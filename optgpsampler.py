from optGpSampler.cbModel import CbModel
from optGpSampler.cbModelReducer import CbModelReducer
from optGpSampler.cbModelSampler import CbModelSampler
import numpy as np
from scipy import sparse
from scipy import stats
from yasmenv.model import Model
from yasmenv.modelreaction import ModelReaction
'''
  The citation for optgpsampler is:
  Megchelenbrink W, Huynen M, Marchiori E (2014) 
  optGpSampler: An Improved Tool for Uniformly Sampling the Solution-Space of Genome-Scale Metabolic Networks. 
  PLoS ONE 9(2): e86587. doi:10.1371/journal.pone.0086587
'''


class OptGpSampler:
  def __init__(self, solver='gurobi'):
    self.solver = solver
  
  def sample(self, m, nsamples=1000, nsteps=25, nthreads=2, verbose=False, reduce_parameters=dict()):
    '''
      input: 
        m: a model in yasmenv format
        nsamples: number of samples to take
        nsteps: number of steps to take between samples on the same chain
        nthreads: number of CPU threads to use
        verbose: flag to pass to optGpSampler
    '''
    m2 = model_to_optgp_model(m)
    
    sampler = CbModelSampler(m2)
    
    sampler.setSolverName(self.solver)
    sampler.setNrSamples(nsamples)
    sampler.setNrSteps(nsteps)
    sampler.setNrThreads(nthreads)
    sampler.setVerbose(verbose)
    
    out = sampler.sample()
    
    return out
    
  def reduce(self, m, tolerance=1e-6, sTol=0, verbose=False, return_optgp=False):
    '''
      Reduces the model so that it can be more easily sampled. 
      According to the optGpSampler documentation:
      Remove all reactions that can not carry flux, making the actual sampling more efficient (since all samples will be zero for these reactions).
      Increase the numerical stability by "repairing" (very) small stoichiometric coefficients or flux bounds.
      
      
      input: 
        m: a model in yasmenv format
        tolerance: bounds tolerance. if the magnitude of a bound is less than this value, round it to zero. If an FVA soultion for a reaction is less than this value, round it to zero.
        sTol: stoichiometric tolerance. Stoichiometries below this will be rounded down to zero
        verbose: flag to pass to optGpSampler
        return_optgp: if True, then don't convert the model back to yasmenv format. Return it as an optGp model
        
      output: a reduced model of either yasmenv or optgp format
      
    '''
    m2 = model_to_optgp_model(m)
    reducer = CbModelReducer(m2)
    
    reducer.fixStoichiometricMatrix(sTol)
    reducer.setSolverName(self.solver)
    reducer.setTolerance(tolerance)
    reducer.setVerbose(verbose)

    reduced = reducer.reduce()
    
    if return_optgp:
      out = reduced
    else:
      out = Model()
      m3 = m.copy()
      for (i, r) in enumerate(reduced.reactions):
        out.add_reaction(m3[r])
        out[r].lb = reduced.lower_bounds[i]
        out[r].ub = reduced.upper_bounds[i]
        out[r].objective = reduced.objective_coefficients[i]
      #out = optgp_model_to_model(reduced)
    
    return out
    
  def sampled_frank(self, m, samples, rank_field='Expression'):
    '''
      given an array of flux solutions, finds the one that has the highest spearman correlation to the given field of the input model
      input: 
        m: a yasmenv model
        samples: a list of flux solutions for the yasmenv model
        rank_field: the annotation field to compare to the flux solutions
      out: the flux solution vector of the sampled point with the best spearman correlation coefficient to the rank field
    '''
    rank_vector = list()
    for x in m:
      s = m[x].annotations.get(rank_field,"nan").strip()
      if s == "":
        s = "nan"
      rank_vector.append(float(s))
    rank_vector = np.array(rank_vector)
    valid_expression_bool = ~np.isnan(rank_vector)
    valid_expression = np.array([rank_vector[valid_expression_bool]]).T
    
    pts_to_correlate = np.abs(samples[valid_expression_bool,:])
    
    correlation_array = np.concatenate((valid_expression,pts_to_correlate),1)
    (cor, p) = stats.spearmanr(correlation_array)
    first_row = cor[0,1:cor.shape[1]-1]
    max_index = first_row.argmax()
    out = samples[:,max_index]
    #rank_vector = [float(m[x].annotations.get(rank_field,"0").strip()) for x in m]
    #combined = np.
    return out

def model_to_optgp_model(m):
  '''
    convert a yasmenv model to a model that works with the optGpSampler for nullspace sampling
    
    S*flux = b
    lb <= flux <= ub
    
    input: a yasmenv model
    output: an optgp model
  '''
  m = m.make_finite()
  description = m.name
  S = sparse.lil_matrix((m.len_metabolites(),m.len_reactions()))
  for (met, rxn, stoich) in m.sparse_matrix():
    S[met, rxn] = stoich
  
  lower_bounds = np.array(m.param_list('lb'))
  upper_bounds = np.array(m.param_list('ub'))
  objective_coefficients = np.array(m.param_list('objective'))
  
  metabolites = m.metabolite_names()
  reactions = m.reaction_names()
  b = np.zeros(len(metabolites)) #I think this should be zeros, so that the solutions are mass balanced. yasmenv handles import and export with S and flux bounds, not with b.

  return CbModel(description, S, lower_bounds, upper_bounds, objective_coefficients, b, metabolites, reactions)
  
def optgp_model_to_model(m):
  '''
    convert a model in optgp form to a yasmenv model
    
    S*flux = b
    lb <= flux <= ub
    
    input: an optgp model
    output: a yasmenv model
  '''
  
  name = m.description
  out = Model(name=name)
  
  csc = m.S.tocsc()
  
  for i in range(len(m.reactions)):
    rxn_name = m.reactions[i]
    lb = m.lower_bounds[i]
    ub = m.upper_bounds[i]
    objective = m.objective_coefficients[i]
    row = csc[:,i]
    
    left = dict()
    right = dict()
    
    rxn_stoich = dict()
    for (i,v) in enumerate(row.indices):
      stoich = row.data[i]
      met_name = m.metabolites[v]
      rxn_stoich[met_name] = rxn_stoich.get(met_name, 0.0) + stoich
    
    for (met,stoich) in rxn_stoich.iteritems():
      if stoich > 0:
        right[met] = stoich
      else:
        left[met] = -1*stoich
    
    out.add_reaction(ModelReaction(rxn_name, left, right, lb, ub, objective))
    
  return out